# OML3

Outils Mathématiques et Logiciels (OML3).

[BUT GEII](https://www.iutsf.u-pec.fr/departements/genie-electrique-et-informatique-industrielle), [IUT-SF](https://www.iutsf.u-pec.fr/), [UPEC](https://www.u-pec.fr).




Building html book
-----

See the [doc](https://jupyterbook.org/). 
And an example in [github](https://github.com/executablebooks/jupyter-book)

* install
   * ```pip install jupyter-book```
   * proof extension: [www](https://jupyterbook.org/en/stable/content/proof.html)

* configure: 
   * sphinx proof extension: [www](https://jupyterbook.org/en/stable/content/proof.html#adding-extension-through-config-yml)
   * sphinx+latex: add extra packages: ???

Build PDF book
----

* dependencies: see https://jupyterbook.org/en/stable/advanced/pdf.html
* pdfhtml builder: will download chromium
  > jupyter-book build book/ --builder pdfhtml

* pdflatex: needs latexmk ubunutu package; will stop on latex error
  > jupyter-book build book/ --builder pdflatex


Make figures
----

* make them in a notebook then invoke glue
* [circuitikz](https://texample.net/tikz/examples/induction-machine/), compile pdf from latex with `\documentclass{standalone}`.
   
   
Misc
----

qpdf --empty --pages TD1.pdf TD2.pdf -- merged.pdf
   
ou bien :

cat book/TD1-revisions.md bookb/TD2-DSF.md > TD1+2.md

puis print avec typora   
