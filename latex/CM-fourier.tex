\chapter{Décomposition en série de Fourier}

\begin{framed}\noindent Prérequis~\\
OML2, 1ere année, lien direct \href{https://upecnumerique.sharepoint.com/sites/OML/Supports\%20de\%20cours/Forms/AllItems.aspx}{sharepoint}
Chapitres: Développement en Série de Fourier.
\end{framed}

\begin{framed}\noindent Note~\\
Cette partie du cours est adaptée du cours de Guillaume Bourlet à l'IUT SF.
\end{framed}

\begin{framed}\noindent Important~\\
La DSF est au centre de la théorie de Fourier. Elle signifie qu'on peut reconstruire un signal continu et périodique
(sous-entendu non sinusoïdal) comme somme infinie de signaux sinusoïdaux (ses harmoniques).
\end{framed}

%%% ICI illustration de ce principe générale: H1, H1+H2,H1+H2+H3

\begin{definition}[Coefficients de Fourier réels]
\label{def.DSF}
Les coefficients de Fourier d'une fonction T-périodique sont définis par les formules :
\begin{eqnarray}
a_0 &=&  \frac{1}{T} \int_{[T]} f(t) dt  \\
a_n &=& \frac{2}{T} \int_{[T]} f(t) \cos(n \omega t) dt \\
b_n &=& \frac{2}{T} \int_{[T]} f(t) \sin(n \omega t) dt 
\end{eqnarray}
\end{definition}

\begin{framed}
\textbf{Notation}: $\int_{[T]}$ signifie que l'intégrale est calculée sur une période quelconque, par exemple $[t_0; t_0+T]$.
\end{framed}

\begin{example}
\label{DSF-example_creneau}
Le créneau impair $C(t)$, 2-périodique valant $1$ sur $]0;1[$
\begin{center}
\includegraphics[width=0.5\linewidth]{fig/creneau}%![creneau](fig/creneau.svg)
\end{center}
On a $T=2$ et $\omega=\frac{2\pi}{T}=\pi$
\paragraph{Calcul de $a_0$}
Le créneau est impair donc sa valeur moyenne sur une période est nulle $a_0=0$

\paragraph{Calcul de $a_n, n \geq 1$}
\begin{eqnarray}
a_n &=& \frac{2}{T} \int_{[T]} C(t) \cos(n \omega t) dt 
\end{eqnarray}
Comme le créneau $C(t)$ est impair et que la fonction 
$\cos(n \omega t)$ est paire, le produit $C(t)\cos(n\omega t)$ est
impair donc son intégrale est nulle sur toute période: $\forall n \geq 1, a_n=0$

\paragraph{Calcul de $b_n, n \geq 1$}
\begin{eqnarray}
b_n &=& \frac{2}{T} \int_{[T]} C(t) \sin(n \omega t) dt 
\end{eqnarray}
Comme le créneau $C(t)$ est impair et que la fonction $\sin(n\omega t)$ est impaire,
le produit $C(t)\sin(n\omega t)$ est pair donc :
\begin{eqnarray}
\int_{-T/2}^{T/2} C(t) \sin(n \omega t) dt  &=& 2 \int_{0}^{T/2} C(t) \sin(n \omega t) dt 
\end{eqnarray}
On calcule donc :
\begin{eqnarray}
b_n &=& 2 \frac{2}{2} \int_{0}^1 C(t) \sin(n \omega t) dt \\
&=& 2  \int_{0}^1 \sin(n \omega t) dt \\
&=& 2  \Bigg[ \frac{ -\cos(n \omega t) }{n \omega} \Bigg]_{0}^1 \\
&=& -\frac{ 2}{n \omega}  \Bigg(  \cos(n \omega )  -\cos(0) \Bigg) \\
&=& -\frac{ 2}{n \pi}  \Bigg(  \cos(n \pi )  -\cos(0) \Bigg) \\
&=& -\frac{ 2}{n \pi}  \Bigg( (-1)^n -1 \Bigg) \\
&=& \frac{ 2}{n \pi}  \Bigg( 1 - (-1)^n  \Bigg) 
\end{eqnarray}
\paragraph{Bilan}
\begin{eqnarray}
a_0 &=& 0 \\
\forall n \geq 1, a_n &=& 0 \\
\forall n \geq 1, b_n &=& \frac{ 2}{n \pi}  \Bigg( 1 - (-1)^n  \Bigg)  
\end{eqnarray}
\end{example}

\begin{framed}
L'exploitation de la \textit{parité} du créneau nous a permis de réduire les calculs
de façon significative.

Soit $f$ une fonction $T$-périodique.
\begin{itemize}
\item Si $f$ est paire :
\begin{itemize}
   \item  les $b_n$ sont nuls (et donc sa série de Fourier ne contient que des cosinus qui sont des fonctions paires).
   \item  $a_0 = \frac{1}{T} \int_{[T]} f(t)dt =  \frac{2}{T} \int_{0}^{T/2} f(t)dt$
   \item  $\forall n \geq 1, a_n = \frac{2}{T} \int_{[T]} f(t) \cos(n\omega t) dt =  \frac{4}{T} \int_{0}^{T/2} f(t) \cos(n\omega t) dt$
   \item  \textit{Explication}:  Comme $f(t)$ et $f(t)\cos(n\omega t)$ sont paires, leur intégrale sur $[-T/2,T/2]$ vaut le double de l’intégrale sur $[0,T/2]$.
   \end{itemize}
\item Si $f$ est impaire :
    \begin{itemize}
   \item  les $a_n$ sont nuls (et donc sa série de Fourier ne contient que des sinus qui sont des fonctions impaires).
   \item  $\forall n \geq 1, b_n = \frac{2}{T} \int_{[T]} f(t) \sin(n\omega t) dt =  \frac{4}{T} \int_{0}^{T/2} f(t) \sin(n\omega t) dt$
   \item  \textit{Explication}:  Comme $f(t)\sin(n\omega t)$ est paire, son intégrale sur $[-T/2,T/2]$ vaut le double de l’intégrale sur $[0,T/2]$.
  \end{itemize}
\end{itemize}
\end{framed}

\begin{definition}[Les Harmoniques] 
sont des fonctions de $t$. Elles sont définies par les formules suivantes :
\begin{eqnarray}
 H_0 &=& a_0 \\
\forall n \geq 1, H_n(t) &=& a_n \cos(n \omega t) + b_n \sin(n \omega t)
\end{eqnarray}
\end{definition}

\begin{definition}[Le fondamental]
\label{DSF-fondamental}
La composante $H_1(t)$ s'appelle le fondamental. Elle a la même période $T$ que le signal $f(t)$ dont on calcule la DSF.
\end{definition}

\begin{definition}[Spectre]
\label{DSF-spectre}
le diagramme qui à chaque harmonique de fréquence $nf=n/T$ associe la valeur
$\sqrt{a_n^2+b_n^2}, ~n\geq 0$ s'appelle le spectre du signal. 
\begin{center}
\includegraphics[width=0.4\linewidth]{fig/spectre_DSF.png}
\end{center}
\end{definition}

\begin{theorem}[Dirichlet]
Le théorème de Dirichlet dit que sous certaines conditions qui seront toujours réalisées en GEII, en toute valeur $t$ où $f$ périodique est continue :

\begin{eqnarray}
f(t) &=&  a_0 + \sum_{n=1}^{+\infty} H_n(t) \\
&=& a_0 + \sum_{n=1}^{+\infty} a_n \cos(n \omega t)+b_n \sin(n \omega t)
\end{eqnarray}
\textit{Que se passe-t-il aux points de discontinuité de f ?}~\\
En GEII, les points de discontinuité sont des valeurs $t_0$ en lesquelles la fonction présente un saut fini.
En ces valeurs, la somme infinie des harmoniques ne redonne pas $f(t_0)$ mais la demi-somme (ou la moyenne) des valeurs à gauche et à droite du saut.
\end{theorem}

\begin{example}
Le théorème de Dirichlet appliqué à notre créneau impair, 2-périodique valant $1$ sur $]0,1[$.
On va reconstruire notre créneau en faisant la somme d'une "quantité suffisante d'harmoniques pour obtenir une bonne valeur approchée de signal".

On pose $S_N(t)=H_0 + \sum_{n=1}^{N} H_n(t)$

$S_N$ est la somme partielle des $N+1$ premières harmoniques.
Pour notre créneau, cela donne :
\begin{eqnarray}
S_N(t)&=& \sum_{n=1}^{N} \frac{ 2}{n \pi}  \Bigg( 1 - (-1)^n  \Bigg) \sin(n \pi t)  
\end{eqnarray}
Prenons $N=20$:
\begin{eqnarray}
S_N(t)&=& \sum_{n=1}^{20} \frac{ 2}{n \pi}  \Bigg( 1 - (-1)^n  \Bigg) \sin(n \pi t)  
\end{eqnarray}
La somme des 21 premières harmoniques reconstruite avec Sage:
\begin{verbatim}
a0=0
var('n')
assume(n,'integer')
omega=pi
b(n) =  2/(n *pi) * ( 1 - (-1)^n ) 
S(x)=a0+sum(b(n)*sin(n*omega*x),n,1,20)
plot(S(x),(-2,2))
\end{verbatim}

\begin{center}
\includegraphics[width=0.5\linewidth]{fig/creneau_dirichlet.png}
\end{center}
On constate une \textbf{distorsion} au voisinage des points de \textbf{discontinuité}.
C'est le phénomène de \href{https://fr.wikipedia.org/wiki/Ph\%C3\%A9nom\%C3\%A8ne_de_Gibbs}{Gibbs}.
\end{example}

%!!!! TODO: ici autre exemple avec IPP (e.g. TD OML2)


%\begin{definition}[Puissance moyenne d'un signal]
%En théorie du signal, la \textbf{puissance moyenne} d'un signal T-périodique $f(t)$ est:
%\begin{eqnarray}
%P &=& \frac{1}{T}\int_{[T]} \vert f(t) \vert^2 dt
%\end{eqnarray}
%C'est en fait la valeur moyenne du carré du module du signal sur une période.
%L'égalité de Parseval permet d'exprimer cette puissance moyenne comme somme d'une série numérique basée sur les coefficients de Fourier.
%\end{definition}

\begin{theorem}[La formule de Parseval]
Soit $f$ une fonction $T$-périodique vérifiant les conditions habituelles en GEII.
Soit $a_n$ et $b_n$ les suites de ses coefficients de Fourier. Alors:
\begin{eqnarray}
\frac{1}{T}\int_{[T]} \vert f(t) \vert^2 dt &=& a_0^2 + \frac{1}{2} \sum_{n=1}^{+\infty} (a_n^2 + b_n^2)
\end{eqnarray}
\end{theorem}

\begin{remark}
La formule de Parseval relie le domaine \textbf{temporel} et le domaine \textbf{fréquentiel}.
\end{remark}

\begin{remark}[Valeur efficace et coefficients de Fourier]
Soit $f(t)$ une fonction périodique de période $T$, $a_n$ et $b_n$ ses coefficients de Fourier.
\begin{eqnarray}
 F_{eff} &=& \sqrt{ \frac{1}{T} \int_{0}^{T}f^2(t)dt} \\
 &=& \sqrt{ a_0^2 + \frac{1}{2} \sum_{n=1}^{+\infty} (a_n^2 + b_n^2) } 
 \label{eq.val.efficace.parseval}
\end{eqnarray}
\end{remark}


\begin{remark}[Valeur efficace et coefficients de Fourier d'un signal impair]
Soit $f(t)$ une fonction $T$-périodique \textbf{impaire}, et $b_n$ ses coefficients de Fourier impairs.
L'expression eq.(\ref{eq.val.efficace.parseval}) ci-dessus se simplifie:
\begin{eqnarray}
 F_{eff} &=& \sqrt{ \frac{1}{T} \int_{0}^{T}f^2(t)dt} \\
 &=& \sqrt{ \frac{1}{2} \sum_{1}^{\infty} b_n^2} 
\end{eqnarray}
\end{remark}




\begin{framed}\noindent Exemple GEII: Distorsion Harmonique~\\

Quand on connecte une charge \textbf{non-linéaire} (exemple: alimentation à découpage) 
sur la tension du secteur, la tension et l'intensité sont des sinusoïdes déformées. 

Comment \textbf{quantifier la déformation} ? 
\end{framed}



\begin{definition}[TDH, Taux de Distorsion Harmonique]
Ce taux est le ratio:
\begin{eqnarray}
\label{eq.TDH.temporel}
\text{TDH}(y) &=& 100 \frac{\textrm{valeur efficace de} ~y(t) \textrm{ sans son premier harmonique}}{\text{valeur efficace du premier harmonique}}
\end{eqnarray}
\end{definition}

\begin{remark}%[TDH, Taux de Distorsion Harmonique]
Pour une fonction \textbf{impaire}, grâce à l'égalité de Parseval, on peut le ré-écrire sous la forme:
\begin{eqnarray}
\label{eq.TDH.frequentiel.impair}
 \text{TDH}(y) &=& 100 \frac{\sqrt{ \sum_{\mathbf{n=2}}^{\infty} b_n^2 } }{b_1}
\end{eqnarray}

\end{remark}


\begin{proof}

\begin{eqnarray}
\text{TDH}(y) &=& 100 \frac{\textrm{valeur efficace de} ~y(t) \textrm{ sans son premier harmonique}}{\text{valeur efficace du premier harmonique}} \\
&=& 100 \frac{ \sqrt{\frac{1}{2} \sum_{2}^{\infty} b_n^2}}{ \sqrt{ \frac{1}{2} \sum_{1}^{1} b_n^2}} \\
&=& 100 \frac{ \sqrt{ \sum_{2}^{\infty} b_n^2}}{ \sqrt{ \sum_{1}^{1} b_n^2}} \\
%&=& 100 \frac{ \sqrt{\sum_{2}^{\infty} b_n^2}}{\sqrt{  \sum_{1}^{1} b_n^2}}\\
&=& 100 \frac{ \sqrt{\sum_{2}^{\infty} b_n^2}}{\sqrt{ b_1^2}}\\
&=& 100 \frac{ \sqrt{\sum_{2}^{\infty} b_n^2}}{ b_1}
\end{eqnarray}

\end{proof}

\begin{framed}
Le TDH permet de quantifier l'écart du signal à une sinusoïde pure, comme une distance. 
\begin{itemize}
\item Si le TDH vaut $0$, alors le signal $y(t)$ est sinusoïdal.
\item Plus le TDH est élevé, plus notre signal $y(t)$ s'éloigne d'une sinusoïde.
\end{itemize}
\end{framed}


\begin{example}[TDH d'une sinusoïde pure]
Pour un sinus de pulsation $\omega$ et de préiode $T$, on a :
\begin{eqnarray}
x(t) &=& \sin(\omega t) \\
a_0 &=& 0 \\
a_n &=& 0 \\
b_n &=& \frac{2}{T} \int_{[T]} \sin(\omega t) \sin(n \omega t) dt 
\end{eqnarray}
Pour $n=1$, on trouve un terme $\sin^2$, on va utiliser la formule du cours $\sin^2(a)=\frac{1-\cos(2a)}{2}$:
\begin{eqnarray}
b_1 &=& \frac{2}{T} \int_{[T]} \sin(\omega t) \sin( \omega t)  dt\\
&=& \frac{2}{T} \int_{[T]} \sin^2(\omega t)  dt\\
&=& \frac{2}{T} \int_{[T]} \frac{ 1- \cos(2\omega t) }{2} dt\\
&=& \frac{1}{T} \int_{[T]} 1 dt  - \frac{1}{T} \int_{[T]} \cos(2\omega t) dt\\
&=& \frac{1}{T} [t]_{0}^{T}  -  2 \frac{1}{T} \int_{0}^{\frac{T}{2}} \cos(2\omega t)  dt \\
&=& \frac{1}{T} (T-0) -  \frac{2}{T} \Big[ \frac{1}{2\omega} \sin(2\omega t)  \Big]_0^{\frac{T}{2}}\\
&=& 1 -  \frac{1}{\omega T} \Big( \sin(2 \frac{2\pi}{T}  \frac{T}{2})-0  \Big)\\
&=& 1 -  \frac{1}{\omega T} \Big( \sin(2 \pi)-0  \Big)\\
&=& 1
\end{eqnarray}
Pour $n>1$, on obtient un terme $ \sin(\omega t) \sin(n \omega t) $ qu'on va ré-écrire avec 
les formules d'Euler $\sin(x)=\frac{e^{ix}-e^{-ix}}{2i}$ et $\cos(x)=\frac{e^{ix}+e^{-ix}}{2}$.
\begin{eqnarray}
b_n &=& \frac{2}{T} \int_{[T]} \Big( \frac{e^{i\omega t}-e^{-i\omega t}}{2i} \Big) \Big( \frac{e^{in\omega t}-e^{-in\omega t}}{2i} \Big)  dt\\
&=& -\frac{1}{2T} \int_{[T]} \Big( e^{i (n+1)\omega t}-e^{-i (n-1)\omega t}-e^{i (n-1)\omega t}  +e^{-i (n+1)\omega t} \Big)  dt\\
&=& -\frac{1}{2T}  \int_{[T]} \Big( ( e^{i (n+1)\omega t} +e^{-i (n+1)\omega t})- (e^{-i (n-1)\omega t}+e^{i (n-1)\omega t} ) \Big)  dt\\
&=& -\frac{1}{2T}  \int_{[T]} \Big( 2\cos((n+1)\omega t)   - 2\cos((n-1)\omega t) \Big)  dt\\
&=& -\frac{1}{T}   \Big(  \Big[ \frac{1}{(n+1)\omega} \sin((n+1)\omega t) \Big]^T_0   -  \Big[ \frac{1}{(n-1)\omega} \sin((n-1)\omega t) \Big]^T_0  )  dt\\
&=& 0
\end{eqnarray}
On peut maintenant injecter ces résultats dans la THD, comme le signal est impair:
\begin{eqnarray}
\text{TDH}(\sin(\omega t)) &=& 100 \frac{ \sqrt{\sum_{2}^{\infty} b_n^2}}{ b_1} \\
&=& 100 \frac{ \sqrt{\sum_{2}^{\infty} 0 }}{ 1} \\
&=& 0 \\
\end{eqnarray}

La sinusoïde pure a donc un TDH nul, ce qui est logique.
\end{example}


\begin{example}[Charge linéaire]
Ci-dessous l'intensité $I(t)$ traversant une charge linéaire en fonction du temps, et les coefficients de Fourier. 
\begin{center}
\includegraphics[width=0.6\linewidth]{fig/TDH_lineaire.png}
\end{center}
\end{example}

\begin{example}[Variateur de vitesse]
Ci-dessous l'intensité $I(t)$ traversant un variateur en fonction du temps, et les coefficients de Fourier. 
\begin{center}
\includegraphics[width=0.6\linewidth]{fig/TDH.png}
\end{center}

source: cours F.Vallenet, GEII, IUT-SF.
\end{example}


% TODO: ICI EXEMPLE chateignier (génie elec, dunod)
%      Blagouchine: calculs analytiques du TDH
%      puissance dissipée (chateignier p.101) 
%      transmission puissance (en fréquentiel) 

% -------------------------------------------------------------------------
\section{Ecriture complexe du DSF d'une fonction périodique}

\begin{definition}
\label{fourier-complexe}

Soit une fonction $f(t)$ \textbf{périodique} de période $T$ et $\omega=\frac{2\pi}{T}$.
On peut écrire de manière équivalente la DSF avec les coefficients complexes $c_n$ ci-dessous:
\begin{eqnarray}
c_n &=& \frac{1}{T} \int_{[T]} f(t) e^{-jn\omega t} dt 
\end{eqnarray}
Et en tout point de continuité:
\begin{eqnarray}
f(t) &=& \sum_{n=-\infty}^{+\infty} c_n e^{jn\omega t}
\end{eqnarray}
\end{definition}


\begin{proof}
Pour revenir à l'expression donnée par le théorème de Dirichlet, on part de l'équation 
$f(t) = \sum_{n=-\infty}^{+\infty} c_n e^{jn\omega t}$.

On remplace $c_n$ par sa valeur, et $e^{jn\omega t}$ par son expression avec $\cos$ et $\sin$.
Puis on sépare les termes réels d'un côté et les termes complexes de l'autres.
Comme le signal $f(t)$ est supposé réel ici, la partie imaginaire est nulle.

Du fait de la parité, les sommes $\sum_{n=-\infty}^{+\infty}$ se réecrivent facilement à partir de $\sum_{n=1}^{+\infty}$. 
\end{proof}

\begin{remark}
\label{fourier-complexe}
On a la relation suivante entre $c_n, ~a_n, ~b_n$:

\begin{eqnarray}
\forall n \neq 0, ~c_n &=& \frac{1}{2} (a_n - jb_n) \\
c_0 &=& a_0
\end{eqnarray}
\end{remark}

\begin{proof}
Partir de la définition $c_n = \frac{1}{T} \int_{[T]} f(t) e^{-jn\omega t} dt $, séparer les termes
réels et imaginaires, puis identifier.
\end{proof}

\begin{framed}\noindent Note~\\
Cette écriture nous servira en OML4.
\end{framed}

\begin{example}[Signal rectangulaire]
Signal rectangulaire de période $T_1$, de niveau $E$ et de rapport cyclique 1/2.

Pour $c_0$ on a $c_0 = a_0 = \frac{E}{2} $

\begin{eqnarray}
c_n &=& \frac{1}{T} \int_{[T]} f(t) e^{-jn\omega t} dt \\
\end{eqnarray}

A finir en TD.

Source:  \cite[p.99]{chateignier_2007} 
\end{example}

% ECRITURE COMPLEXE : EXO chateignier p.100)

