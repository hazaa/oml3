(CM_Fourier_continu)=
# CM Transformation de Fourier

Dans cette partie du cours nous continuons à nous intéresser aux signaux à temps continu.

`````{admonition} Prérequis

*  OML3, chapitre {ref}`CM_revisions`.

`````

```{note}
Cette partie du cours se base sur le cours de R. Quéré {cite}`quere_2013` et F.Cottet {cite}`cottet_2020` disponibles en ligne.
```


## I) Convolution et filtrage

Quand on injecte une impulsion brève dans un système de transmission
(linéaire, continu, stationnaire), on n'obtient pas une impulsion en sortie, 
mais une réponse impulsionnelle.

![impulse](fig/reponse_impulsionnelle.svg)


````{prf:example}
:label: convolution-example

Musique (coup bref sur une cimballe) ...
````

%d'où vient la convolution (cottet 3.3.1)


````{prf:definition}
:label: dirac
**Rappel**: l'**impulsion de Dirac** est un signal idéal tel que:

$$
\begin{eqnarray}
\delta(t) &=& 0 ~\textrm{si} ~t \neq 0  \\
\int_{-\infty}^{+\infty}\delta(x) dx&=&1
\end{eqnarray}
$$

On peut le voir comme le passage à la limite du signal suivant quand $A \rightarrow +\infty$:

![dirac](fig/dirac.svg)

Et sa représentation symbolique:
![dirac](fig/dirac_symbole.svg)

Idem avec un retard $t_0$:
![dirac_delay](fig/dirac_retard_symbole.svg)

````

````{prf:definition}
:label: convolution

Soit $f$ et $g$ deux fonctions intégrables sur $\mathbb{R}$.
On appelle produit de convolution de $f$ par $g$ la fonction $f \star g$ telle que:

$$
\begin{eqnarray}
(f \star g)(t) &=& \int_{-\infty}^{+\infty} f(\theta) g(t-\theta) d\theta
\end{eqnarray}
$$
````

````{prf:property}
:label: convolution-prop

* Commutativité:  $(f \star g)(t) = (g\star f)(t)$
* Distributivité: $(f \star (g+h) )(t) = (f \star g+ f \star h )(t) $
* Associativité: $(f \star (g \star h) )(t) = (f \star g \star h )(t) $
* Neutre : $(f \star \delta )(t) = (\delta \star f)(t) = f(t) $, où $\delta(t)$ est le pic de Dirac.
````

````{prf:example}
Convolution d'une fonction porte $\Pi(t)$ par une fonction porte  

![convolution_porte_porte](fig/convolution_porte_porte.svg)

En animation:

![convolution1](fig/Convolucion_Funcion_Pi.gif)

(source: [Wikipedia](https://commons.wikimedia.org/wiki/File:Convolucion_Funcion_Pi.gif))

````

````{prf:example}
Convolution d'une fonction porte $\Pi(t)$ par un Dirac

![convolution_porte_dirac](fig/convolution_porte_dirac.svg)

$$
\begin{eqnarray}
(\Pi \star \delta)(t) &=& \int_{-\infty}^{+\infty} \Pi(\theta) \delta(t-\theta) d\theta \\
\end{eqnarray}
$$

Si $t \in [-1/2; 1/2] $:

$$
\begin{eqnarray}
(\Pi \star \delta)(t) &=& \int_{-\infty}^{+\infty} \delta(t-\theta)  d\theta  
&=& 1 
&=& \Pi (t)
\end{eqnarray}
$$

Sinon:

$$
\begin{eqnarray}
(\Pi \star \delta)(t) &=& \int_{-\infty}^{+\infty} 0 . \delta(t-\theta)  d\theta  
&=& 0 
&=& \Pi (t)
\end{eqnarray}
$$ 

Conclusion:  $(\Pi \star \delta)(t) = \Pi(t)$

NB: c'est un résultat général, il vaut en fait pour toutes les fonctions, comme on l'a vu plus haut.
````



* Exemple: convolution d'une fonction par deux Dirac

![convolution3](fig/Convol_2diracs.png)




II) Transformation de Fourier d'une fonction non périodique
-----

La décomposition en série de Fourier (DSF) n'est définie
que pour les fonctions périodiques. Pour contourner
ce problème, on fait tendre la période $T$ vers l'infini dans la DSF,
afin de définir la **transformée de Fourier**.

```{danger}
Dans cette partie les fonctions ne sont pas périodiques.
```


````{prf:definition}
:label: fourier-transform

$u:t \in \mathbb{R} \rightarrow \mathbb{R}$ un signal tel que:
* $u$ est continue et dérivable sur tout intervalle fermé, sauf en un nombre fini de discontinuités.
* $\int_{-\infty}^{+\infty}  |u(t)|dt$ est finie

La **transformée de Fourier** de $u$ est la fonction 
$U = TF [u(t )]$ de $\mathbb{R}$ dans $\mathbb{C}$
définie par:

$$
\begin{eqnarray}
U(f) &=& \int_{-\infty}^{+\infty} u(t) e^{-j 2 \pi f t}dt
\end{eqnarray}
$$
En général $U(f)$ est complexe et continue (pas comme la DSF).
La courbe $f\rightarrow |U(f)|$ est appelée le **spectre** de $u$. 
````

```{danger}
Ne pas confondre avec la Décomposition en Série de Fourier (DSF)
vue en chapitre {ref}`CM_revisions`, {prf:ref}`DSF`, 
pour une fonction **périodique**. On avait posé dans ce cas:

$$
\begin{eqnarray}
a_0 &=&  \frac{1}{T} \int_{[T]} f(t) dt  \\
a_n &=& \frac{2}{T} \int_{[T]} f(t) \cos(n \omega t) dt \\
b_n &=& \frac{2}{T} \int_{[T]} f(t) \sin(n \omega t) dt 
\end{eqnarray}
$$

ou de manière équivalente en notation complexe, {prf:ref}`fourier-complexe`:

$$
\begin{eqnarray}
c_n &=& \frac{1}{T} \int_{[T]} f(t) e^{-jn\omega t} dt \\
&=& \frac{1}{T} \int_{-T/2}^{T/2} f(t) e^{-j 2\pi n f t} dt 
\end{eqnarray}
$$

```


```{Note}
**Transformée de Laplace (TL)/ Transformée de Fourier**: 

$$
\begin{eqnarray}
U(p) &=& \int_0^{+\infty} e^{-pt} u(t) dt
\end{eqnarray}
$$

* Voir les révisions {ref}`transformee_laplace`.
* La TF est identique à la transformée de Laplace quand le signal est causal (cf {prf:ref}`def-causal`), en posant $p = j 2 \pi f = j \omega$.

```


````{prf:example}
:label: porte

Fonction **porte**: avec la définition suivante:

$$
\begin{equation*}
\Pi(t)=
\left\{
\begin{array}{rl}
1 & \text{si } t \in [-\frac{1}{2},\frac{1}{2}],\\
0 & \text{sinon}.
\end{array} \right.
\end{equation*}
$$

![porte](fig/porte.svg)

Calculons sa TF:

$$
\begin{eqnarray}
U(f) =TF[\Pi(t)](f) &=& \int_{-\frac{1}{2}}^{+\frac{1}{2}} 1. e^{-j 2 \pi f t}dt \\
 &=& -\frac{1}{j2\pi f}\Big[  e^{-j2\pi ft}\Big]_{-\frac{1}{2}}^{+\frac{1}{2}} \\
 &=& -\frac{1}{j2\pi f}\Big[  e^{-j\pi f} -e^{j\pi f} \Big] \\
 &=& \frac{\sin(\pi f)}{\pi f}
\end{eqnarray}
$$

Son graphe est :

![sinc](fig/sinc.svg)


NB: la fonction $sinc(x)=\frac{\sin(x)}{x}$ s'appelle le sinus cardinal.

````

````{prf:example}
Fonction **Dirac** temporelle: le calcul est le même que 
pour la fonction porte $\Pi(t)$.  On part d'un signal $\delta_\varepsilon(t)$
qui approxime l'impulsion de Dirac, puis on on fera tendre $\varepsilon$ vers 0 à la fin.

$$
\begin{equation*}
\delta_\varepsilon(t)=
\left\{
\begin{array}{rl}
\frac{1}{\varepsilon} & \text{si } t \in [-\frac{\varepsilon}{2},\frac{\varepsilon}{2}],\\
0 & \text{sinon}.
\end{array} \right.
\end{equation*}
$$

Calculons sa TF:

$$
\begin{eqnarray}
TF[\delta_\varepsilon(t)](f) &=& \int_{-\frac{\varepsilon}{2}}^{+\frac{\varepsilon}{2}} \frac{1}{\varepsilon}. e^{-j 2 \pi f t}dt \\
 &=& -\frac{1}{j2\pi f} \frac{1}{\varepsilon} \Big[  e^{-j2\pi ft}\Big]_{-\frac{\varepsilon}{2}}^{+\frac{\varepsilon}{2}} \\
 &=& -\frac{1}{j2\pi f \varepsilon}\Big[  e^{-j\pi f \varepsilon} -e^{j\pi f \varepsilon} \Big] \\
 &=& \frac{\sin(\pi f \varepsilon)}{\pi f \varepsilon}
\end{eqnarray}
$$

Par passage à la limite:

$$
\begin{eqnarray}
TF[\delta(t)](f) &=& \lim_{\varepsilon \rightarrow 0} TF[\delta_\varepsilon(t)](f) \\
&=& \lim_{\varepsilon \rightarrow 0}  \frac{\sin(\pi f \varepsilon)}{\pi f \varepsilon} \\
&=& 1
\end{eqnarray}
$$

Car $\lim_{\varepsilon \rightarrow 0} \frac{\sin(\varepsilon)}{\varepsilon}=1 $  (voir OML2: dévéloppement en série entière, développements limités).

Finalement:

$$
\begin{eqnarray}
\forall f, ~TF[\delta(t)](f) &=& 1
\end{eqnarray}
$$

La TF d'une impulsion de Dirac dans le domaine temporel est une fonction **constante de valeur 1** dans le domaine fréquentiel.
````


### Propriétés de la TF

En général, pour calculer une TF, on partira d'une TF **déjà connue**
et on utilisera les propriétés
ci-dessous plutôt que de calculer explicitement l'intégrale 
$\int u(t) e^{-j 2 \pi f t}dt$. 

```{note}
Le tableau des TF connues se trouve ici: [lien](#resume-des-tf-connues)
```


````{prf:definition}
**$TF^{-1}$:** la transformation de Fourier admet une transformation **inverse**:

$$
\begin{equation*}
u(t)=TF^{-1} [U(f )]=
\left\{
\begin{array}{rl}
\int_{-\infty}^{+\infty} U(f) e^{j 2 \pi f t}df & \text{si } u(t) \text{ est continue en } t,\\
\frac{1}{2}( u(t-\epsilon) +u(t+\epsilon)) & \text{sinon }.
\end{array} \right.
\end{equation*}
$$
````

````{prf:example}
$TF^{-1}$ du **Dirac fréquentiel**: par le même calcul que pour l'impulsion de Dirac dans le domaine temporel, on obtient: 

$$
\begin{eqnarray}
\forall t, ~TF^{-1}[\delta(f)](t) &=& 1
\end{eqnarray}
$$

````


````{prf:property}
**Linéarité:** soient $\alpha,\beta$ des réels. Alors:

$$
\begin{eqnarray}
TF[\alpha u(t) + \beta v(t)](f) &=& \alpha TF[ u(t)](f) + \beta TF[ v(t)](f) 
\end{eqnarray}
$$
````

````{prf:property}
**Dilatation:** soit $a$ un réel. Soit $U(f)$ la TF de $u(t)$.
Alors $TF[ u(at) ]=\frac{1}{|a|}U(\frac{f}{a}) $
````
````{prf:proof}
soit $a>0$ un réel. Par le changement de variable $x=at$, on 
a $dx = a dt$. D'où:

$$
\begin{eqnarray}
TF[ u(at) ] &=&  \int_{-\infty}^{+\infty} u(at) e^{-j 2 \pi f t}dt  \\
&=&   \int_{-\infty}^{+\infty} u(x) e^{-j 2 \pi f \frac{x}{a}} \frac{1}{a} dx    \\
&=& \frac{1}{a}U(\frac{f}{a}) 
\end{eqnarray}
$$

Dans le cas négatif, le calcul est presque le même mais 
les bornes de l'intégrale sont renversées, 
d'où la valeur absolue $|a|$ dans la définition.
````


````{prf:property}
:label: prop-translation

**Translation temporelle**:  la TF d'un signal $u(t)$ décalé
d'une durée $t_0$ vaut $TF[ u(t-t_0) ](f)=  e^{-j 2 \pi f t_0}TF[ u(t)](f)$
````

````{prf:proof}
Par le changement de variable $x=t-t_0$, on a
$dx = dt$. D'où:

$$
\begin{eqnarray}
TF[ u(t-t_0) ](f) &=&  \int_{-\infty}^{+\infty} u(t-t_0) e^{-j 2 \pi f t}dt  \\
&=&  \int_{-\infty}^{+\infty} u(x) e^{-j 2 \pi f (x+t_0)}dx  \\
&=&  \int_{-\infty}^{+\infty} u(x) e^{-j 2 \pi f t_0} e^{-j 2 \pi f x}dx  \\
&=&  e^{-j 2 \pi f t_0}  \int_{-\infty}^{+\infty} u(x) e^{-j 2 \pi f x}dx  \\
&=&  e^{-j 2 \pi f t_0} TF[ u(t)](f)
\end{eqnarray}
$$
````

````{prf:property}
**Translation fréquentielle**:  la TF inverse 
de $U(f)$ décalée
d'une fréquence $f_0$ vaut $TF^{-1}[ U(f-f_0) ](t)=  e^{j 2 \pi f t_0}TF^{-1}[ U(f) ](t)$
````
````{prf:proof}
A faire en exercice.
````


````{prf:example}

**TF du cosinus:**
Ceci nous permettra d'appliquer plusieurs propriétés de la TF
vues ci-dessus.

On part de la $TF^{-1}$ du Dirac fréquentiel, qui est connue 
(cf plus haut): $TF^{-1}[\delta(f)](t)=1$

Ensuite on calcule la $TF^{-1}$ du Dirac fréquentiel translatée
de la fréquence $f_0$, grâce à la propriété de translation :

$$
\begin{eqnarray}
TF^{-1}[ \delta(f-f_0) ](t) &=& e^{j2\pi f_0 t}
\end{eqnarray}
$$

Idem pour un Dirac fréquentiel translaté
de la fréquence $-f_0$:

$$
\begin{eqnarray}
TF^{-1}[ \delta(f+f_0) ](t) &=& e^{-j2\pi f_0 t}
\end{eqnarray}
$$

Par la propriété de linéarité on a alors:

$$
\begin{eqnarray}
TF^{-1}\Big[ \frac{ \delta(f-f_0) +\delta(f+f_0)}{2} \Big](t) &=& \frac{ e^{-j2\pi f_0 t}+e^{j2\pi f_0 t} }{2} \\
&=& \cos( 2\pi f_0 t)
\end{eqnarray}
$$

Enfin on applique une $TF$ des 2 côtés de l'équation:

$$
\begin{eqnarray}
TF \Bigg[TF^{-1}\Big[ \frac{ \delta(f-f_0) +\delta(f+f_0)}{2} \Big](t)\Bigg] (f) &=& TF[\cos( 2\pi f_0 t)](f) \\
\end{eqnarray}
$$

Ce qui donne finalement:

$$
\begin{eqnarray}
TF[\cos( 2\pi f_0 t)](f) &=& \frac{ \delta(f-f_0) +\delta(f+f_0)}{2} 
\end{eqnarray}
$$

Ce qui se représente par le diagramme suivant:

![spectre_cos](fig/spectre_cos.svg)

NB: dans ce cas, la TF est réelle, ce qui n'est pas toujours le cas.
````


````{prf:example}
**TF du sinus:**
on part cette fois de $\frac{ \delta(f-f_0) -\delta(f+f_0)}{2j}$.
En suivant les mêmes étapes de calcul on arrive à: 

$$
\begin{eqnarray}
TF[\sin( 2\pi f_0 t)](f) &=& \frac{ \delta(f-f_0) -\delta(f+f_0)}{2j} 
\end{eqnarray}
$$


NB: dans ce cas, la TF est imaginaire pure, ce qui n'est pas toujours le cas.
````



````{prf:property}
**Dérivation:**
Supposons que $u(t)$ et $u'(t)$ admettent des TF. Alors:

$$
\begin{eqnarray}
TF[u'(t)](f) &=& (j2\pi f).TF[u(t)](f)
\end{eqnarray}
$$

````

````{prf:proof}
A faire en exercice, par IPP.
````



````{prf:theorem}
:label: plancherel

**Convolution: théorème de Plancherel**

En {prf:ref}`convolution` on a défini
le produit de convolution  $(f \star g)(t) = \int_{-\infty}^{+\infty} f(\theta) g(t-\theta) d\theta $

Soient $U(f)$ la TF de $u(t)$ et $V(f)$ la TF de $v(t)$:

Alors on a :

$$
\begin{eqnarray}
TF[u \star v](f) &=& U(f).V(f)
\end{eqnarray}
$$

De même:

$$
\begin{eqnarray}
TF[u(t) v(t)](f) &=& U(f) \star V(f)
\end{eqnarray}
$$

````

````{prf:proof}
On commence par la définition de la TF appliquée à la fonction $u \star v$. 
Puis on remplace $u \star v$ par son expression intégrale.

$$
\begin{eqnarray}
TF[u \star v ](f) &=&   \int_{t=-\infty}^{t=+\infty}  (u \star v)(t)  e^{-j 2 \pi f t}dt   \\
&=&  \int_{t=-\infty}^{t=+\infty} \Bigg[ \int_{\theta=-\infty}^{\theta=+\infty} u(\theta) v(t-\theta) d\theta  \Bigg] e^{-j 2 \pi f t}dt   \\
\end{eqnarray}
$$

On intervertit les intégrales $\int_t$ et $\int_\theta$ puis on fait sortir $ u(\theta)$ et $d\theta$:

$$
\begin{eqnarray}
TF[u \star v ](f) &=& \int_{\theta=-\infty}^{\theta=+\infty}   u(\theta) \Bigg[ \int_{t=-\infty}^{t=+\infty} v(t-\theta)  e^{-j 2 \pi f t}dt \Bigg] d\theta  \\
&=&  \int_{\theta=-\infty}^{\theta=+\infty}   u(\theta)   TF[v(t-\theta)](f)  d\theta  
\end{eqnarray}
$$

Car on a reconnu l'expression d'une TF.
Puis on se souvient que $TF[v(t-\theta)]=TF[v].e^{-j 2 \pi f \theta}$, 
comme vu en {prf:ref}`prop-translation`. Donc:

$$
\begin{eqnarray}
TF[u \star v ](f) &=&  \int_{-\infty}^{+\infty}   u(\theta)   ~TF[v(t)](f) e^{-j 2 \pi f \theta}  d\theta  
\end{eqnarray}
$$

On peut faire sortir $TF[v(t)](f)$  de l'intégrale car ce terme ne dépend pas de $\theta$. D'où:

$$
\begin{eqnarray}
TF[u \star v ](f) &=& TF[v] \int_{-\infty}^{+\infty}   u(\theta)   e^{-j 2 \pi f \theta}  d\theta  \\
&=&   TF[v].TF[u]  \\
&=& U(f).V(f)
\end{eqnarray}
$$

````



````{prf:property}
**Filtrage:**

*  le filtrage d'un signal $u(t)$ dont la TF est $U(f)$
peut se voir dans le domaine **fréquentiel** comme un produit $H(f) U(f)$, où $H(f)$
caractérise le filtre.
* Conséquence: dans le domaine **temporel**, la sortie du filtre est alors $h(t) \star u(t)$, selon le {prf:ref}`plancherel`.
````


````{prf:example}
Par exemple pour un passe-bas, on prendra une fonction porte **dans
le domaine fréquentiel**  $H(f) = \Pi_{2 f_c}(f)$ avec une fréquence de coupure $f_c$.

![porte_freq](fig/porte_freq.svg)


````


`````{admonition} Exemple GEII: filtrage analogique
:class: GEII

Réalisation avec un circuit électronique:

![RC_filter](fig/RC_filter.svg)
`````

````{prf:definition}
:label: echantillonnage

**Echantillonnage idéal:**
Echantillonner un signal analogique $s(t)$ consiste à en extraire des valeurs
régulièrement espacées dans le temps, avec la période $T_e$.

Le signal échantillonné (continu) peut s'écrire:

$$
\begin{eqnarray}
s_e(t) &=& \sum_{k=-\infty}^{+\infty} s(t) \delta(t-k.T_e) \\
 &=& s(t) \sum_{k=-\infty}^{+\infty}  \delta(t-k.T_e)
 &=& s(t) \text{Ш}_{T_e}(t)
\end{eqnarray}
$$

où $\text{Ш}_{T_e}(t)$ est le **peigne de Dirac**.
L'échantillonnage **idéal** peut donc se représenter comme la multiplication par un peigne de Dirac.

![echantillonnage](fig/echantillonnage.svg)
````


````{prf:property}

**TF d'un signal échantillonné:**

Soit $s_e(t)$ un signal échantillonné à partir de $s(t)$ avec la période $T_e$.
Alors la TF de $s_e(t)$ est la **périodisation** de la TF de $s(t)$

On peut le représenter graphiquement par:

![spectre_periodise](fig/spectre_periodise.svg)


Ceci s'écrit mathématiquement:

$$
\begin{eqnarray}
TF[s_e(t)](f)  &=& f_e  \sum_{k=-\infty}^{+\infty} TF[s](f-k.f_e)
\end{eqnarray}
$$

On parle aussi de **périodisation du spectre**.
````

````{prf:proof}

On a vu en {prf:ref}`echantillonnage` que le signal échantillonné
s'écrivait $s_e(t) = s(t) \text{Ш}_{T_e}(t)$.

On peut appliquer la $TF$ des deux côtés:

$$
\begin{eqnarray}
TF[s_e(t)](f) &=&  TF[ s(t) \text{Ш}_{T_e}(t)  ](f) 
\end{eqnarray}
$$


Puis on utilise le théorème de Plancherel pour la TF, qui dit que
$TF[u(t) v(t)](f) = U(f) \star V(f)$  (cf {prf:ref}`plancherel`).

D'où:

$$
\begin{eqnarray}
TF[s_e(t)](f) &=&  TF[s](f) \star TF[ \text{Ш}_{T_e}(t) ](f)
\end{eqnarray}
$$

Or la TF du peigne de Dirac temporel est un peigne de Dirac fréquentiel:


$$
\begin{eqnarray}
TF[ \text{Ш}_{T_e}(t) ](f) &= &  f_e \text{Ш}_{f_e}(f)  \\
&= & f_e  \sum_{k=-\infty}^{+\infty}  \delta(f-k.f_e)
\end{eqnarray}
$$

NB: ceci peut se voir de plusieurs manières: 
* à partir de la DSF ( cf {cite}`quere_2013`). Voir le TD4.
* en calculant la TF directement (cf [Peigne de Dirac](https://en.wikipedia.org/wiki/Dirac_comb) )
* à partir du tableau des TF connues [lien](#resume-des-tf-connues)

Donc par linéarité de la convolution: 

$$
\begin{eqnarray}
TF[s_e(t)](f) &=&  TF[s](f) \star \Bigg[  f_e  \sum_{k=-\infty}^{+\infty}  \delta(f-k.f_e) \Bigg] \\
&=&   f_e  \sum_{k=-\infty}^{+\infty} TF[s](f) \star  \delta(f-k.f_e) 
\end{eqnarray}
$$

Or on a vu ({prf:ref}`convolution-prop`) que le pic de Dirac était l'élément neutre de la convolution. 
Donc pour $k$ quelconque, $TF[s](f) \star  \delta(f-k.f_e)= TF[s](f-k.f_e)$

Finalement: 

$$
\begin{eqnarray}
TF[s_e(t)](f)  &=& f_e  \sum_{k=-\infty}^{+\infty} TF[s](f-k.f_e)
\end{eqnarray}
$$

On voit donc bien que la TF du signal échantillonné est la somme des copies
de la TF du signal de départ, translatées de la fréquence $k.f_e$.

````

````{prf:theorem}
**théorème d'échantillonnage de Shannon-Nyquist:**

Soit un signal continu $s(t)$ de **largeur spectrale limitée**, c'est à dire que tout
son spectre est nul en dehors de $[-f_{max};f_{max}]$. 

On échantillonne ce signal à la fréquence $f_e$.

Si $s(t)$ ne contient pas de fréquence plus grande que $\frac{f_e}{2}$, 
alors $s_e(t)$ détermine complétement $s(t)$, autrement dit on peut
reconstruire $s(t)$ à partir de $s_e(t)$, autrement dit on ne perd pas d'information
lors de l'échantillonnage. 

La condition $2 f_{max} < f_e $ est la condition de Shannon-Nyquist.

Si la condition n'est pas remplie, il y a **recouvrement** du spectre (cf {cite}`cottet_2020` chap. 7.1-2 pour plus de détail).
````

```{note}
**Dans la réalité** l'échantillonnage n'est pas idéal. De même un filtrage passe-bas
idéal n'est pas réalisable. 
La reconstruction n'est donc pas si simple, voir {cite}`cottet_2020` pour plus de précisions.
```



### Résumé des TF connues

On résume ci-dessous les TF rencontrées dans ce cours.

|  Signal temporel | Transformée de Fourier |
| ---      | ---      |
|  $\delta(t)$ |  1 |
| $\alpha u(t) + \beta v(t)$ | $\alpha TF[u(t)](f)+\beta TF[v(t)](f)$  |
|  $u(at)$ | $\frac{1}{\vert a \vert}U(\frac{f}{a})$  |
| $u(t-t_0)$  | $e^{-j 2 \pi f t_0}TF[u(t)](f) $  |
|  $\cos( 2\pi f_0 t)$    |    $\frac{ \delta(f-f_0) +\delta(f+f_0)}{2}$ |
|  $\sin( 2\pi f_0 t)$   | $\frac{ \delta(f-f_0) -\delta(f+f_0)}{2j} $ |
| $\Pi(t)$ | $\frac{\sin(\pi f)}{\pi f}$|
|  $u(t) \star v(t)$     | $ U(f).V(f)$  |
| $Pgn_{T_e}(t)$   |   $f_e Pgn_{f_e}(f)$   |



```{note}
Vous trouverez plus de résultats dans {cite}`cottet_2020` Annexe C, ou [lien direct](http://univ.scholarvox.com.ezproxy.u-pec.fr/reader/docid/88902771/page/397)
```

```{note}
Sachant le lien entre TF et Transformée de Laplace (TL), on peut déduire les TF d'après les TL déjà connues.
```



Pour aller plus loin
-----

* filtrage des signaux analogiques: 
   *   Déﬁnition. Les ﬁltres sont déﬁnis comme des systèmes de transmission linéaires, continus et stationnaires, sont des systèmes de convolution.{cite}`cottet_2020` p.51
   *   filtrage temporel Cottet 9.1.3 (temps discret).
   *   filtrage fréquentiel cottet p.71
   *   Exemple: passe bas cottet 4.12;  exo 4.1 p.77

