# TP1

**Objectif:** Utiliser le logiciel SAGE pour:
* calculer les coefficients du **DSF** (développement en série de Fourier) des fonctions **périodiques**, pour vérifier la convergence de la série de Fourier.
* calculer des **transformées de Laplace** pour étudier un système.


On utilisera markdown pour présenter un calcul codé en Latex.

Vous soumettrez dans EPREL le fichier au format `ipynb` en indiquant votre nom et éventuellement celui de vôtre binôme **dans le nom du fichier**.

Le modèle `TP1-revisions.ipynb` se trouve à l'adresse [www](https://gitlab.com/hazaa/oml3/-/blob/main/book/TP1-revisions.ipynb) 


I) DSF-Développement en séries de Fourier (2 heures)
----

### 1) Rappels théoriques

On a vu OML2 et OML3 la définition des coefficients de Fourier et leur application à l’approximation des fonctions périodiques.

Soit $f$ une fonction $T$-périodique et intégrable sur sa periode, on pose alors pour tout entier $n\in\mathbb{N}^*$:

$$
\begin{align}
    a_0 &=\frac{1}{T}\int_{-T/2}^{T/2} f(x)\,dx\\ \\
    a_n &=\frac{2}{T}\int_{-T/2}^{T/2} f(x)\cos(n\omega x)\,dx\\ \\
    b_n &=\frac{2}{T}\int_{-T/2}^{T/2} f(x)\sin(n\omega x)\,dx 
\end{align}
$$


avec $\displaystyle\omega=\frac{2\pi}{T}$.

Le théorème de Dirichlet dit qu'aux points de continuité de la fonction $f$ on a
$$ f(x)=a_0+\sum_{n=1}^\infty a_n \cos(n\omega x)+ b_n \sin(n\omega x) $$

### 2) Consignes générales

Pour chaque fonction demandée vous devez:

1. Faire calculer les coefficients de Fourier par SAGE
2. Vérifier le calcul de SAGE en calculant les coefficients à la main et en rédigeant en Latex les calculs
3. Faire afficher par SAGE le graphique de la somme partielle $S_{10}(x)$ des 10 premiers coefficients de la série de Fourier pour vérifier s'il y a un début de convergence vers la fonction initiale.
4. Calculer la valeur efficace à la main et avec Sage, de deux manières (grâce à Parseval).
5. Calculer le TDH à la main et avec Sage.

Vous ferez ces calculs dans un nouveau notebook. La présentation du notebook sera largement prise en compte dans la notation. Veillez en particulier (et de manière non exhaustive):
* A écrire les titres de sections, dans un format adapté (police plus grande)
* A écrire un code Latex lisible (parenthèses et crochets adaptés en taille, retour à la ligne, etc...)
* A écrire des numéros de sous-section

Vous pouvez à cet effet vous inspirer librement de ce notebook.

### 3) Un exemple: le triangle $2\pi$-periodique égal à $x$ sur $[0,\pi]$

Déclaration des variables:


```python
var('n')
assume(n,'integer')
T=2*pi
omega=1
```

* 1- On fait calculer les coefficients de Fourier pour SAGE: quelques préliminaires,
    * Comme la fonction triangle $f$ est paire alors
$$ a_0=\frac{2}{T}\int_0^\pi f(x)\,dx=\frac{2}{T}\int_0^\pi x\,dx $$
    * Comme la fonction triangle $f$ est paire et que pour tout entier naturel $n$ non nul la fonction $x\mapsto\sin(n\omega x)$ est impaire alors la fonction $x\mapsto f(x)\sin(n\omega x)$ est impaire donc $b_n=0$.
    * Comme la fonction triangle $f$ est paire et que pour tout entier naturel $n$ non nul la fonction $x\mapsto\cos(n\omega x)$ est paire alors la fonction $x\mapsto f(x)\cos(n\omega x)$ est paire donc
$$ a_n=\frac{4}{T}\int_0^\pi f(x)\cos(n\omega x)\,dx=\frac{4}{T}\int_0^\pi x\cos(n\omega x)\,dx $$


```python
a0=(2/T)*integral(x,x,0,pi)
b(n)=0
a(n)=(4/T)*integral(x*cos(n*omega*x),x,0,pi)

show(a0)
show(factor(a(n)))
```


<html><script type="math/tex; mode=display">\newcommand{\Bold}[1]{\mathbf{#1}}\frac{1}{2} \, \pi</script></html>



<html><script type="math/tex; mode=display">\newcommand{\Bold}[1]{\mathbf{#1}}\frac{2 \, {\left(\left(-1\right)^{n} - 1\right)}}{\pi n^{2}}</script></html>


* 2- On vérifie les calculs de SAGE:

$$\begin{align}
    a_0 &=\frac{2}{2\pi}\int_0^\pi x\,dx\\
        &=\frac{1}{\pi}\left[\frac{x^2}{2}\right]_0^\pi\\
        &=\frac{1}{\pi}\left(\frac{\pi^2}{2}-0\right)\\
        &=\frac{\pi}{2}
\end{align}$$

$$\begin{align} 
    a_n &=\frac{4}{2\pi}\int_0^\pi x\cos(nx)\,dx\\
        &=\frac{2}{\pi}\left( \left[x\frac{\sin(nx)}{n}\right]_0^\pi-\int_0^\pi\frac{\sin(nx)}{n}\,dx \right)\\
        &=\frac{2}{\pi}\left(0-0-\frac{1}{n}\left[\frac{-\cos(nx)}{n}\right]_0^\pi \right)\\
        &=\frac{2((-1)^n-1)}{\pi n^2}
\end{align}$$

* 3- Affichage du graphique de la somme partielle


```python
S(x)=a0+sum(a(n)*cos(n*omega*x),n,1,10)
plot(S(x),(-2*pi,2*pi))
```




    
![png](TP1-revisions_files/TP1-revisions_22_0.png)
    



* 4- Calcul de la valeur efficace

La définition de la valeur efficace d'une fonction $x(t)$ est :

$$\begin{align}
    x_{eff} &= \sqrt{ \frac{1}{T}\int_{[T]} \vert x(t) \vert^2 \,dt }\\
\end{align}$$


On dispose également de l'égalité de Parseval qui dit que:

$$\begin{align}
    \frac{1}{T}\int_{[T]} \vert x(t) \vert^2 &= a_0^2 + \frac{1}{2} \sum_{n=1}^{+\infty} \Big( a_n^2 + b_n^2\Big) \\
\end{align}$$

En utilisant les calculs SAGE ci-dessus, en déduire le calcul de $x_{eff}$ par les 2 méthodes (par l'intégrale puis par la somme des coefficients de la DSF).

Par l'intégrale temporelle:


```python
xeff_sq=2*(1/T)*integral(abs(x)^2,x,0,pi)
print(xeff_sq)
print(N(xeff_sq))
```

>    1/3*pi^2
>   3.28986813369645


Par la somme des coefficients de la DSF:


```python
xeff_sq= a0^2 + 1/2*sum(a(n)^2+b(n)^2,n,1,oo)
print(xeff_sq)
```

>    1/4*pi^2 - 2*sum(2*((-1)^n - 1)/n^4, n, 1, +Infinity)/pi^2


On voit donc que Sage n'arrive pas à simplifier, on fait une troncature et on demande une évaluation numérique avec `N()`:


```python
print(N(a0^2 + 1/2*sum(a(n)^2+b(n)^2,n,1,1000))) # troncature

```

>    3.28986813356136


On retrouve bien le même résultat numérique que ci-dessus, mais on veut une expression symbolique. On remplace le terme $(-1)^n-1$ par $0$ pour les termes $n$ pairs, et $-2$ pour les termes $n$ impairs:


```python
k = var('k')
simplify(1/4*pi^2 + 8/pi^2*sum(1/(2*k+1)^4, k, 0, +Infinity))
```

>    1/3*pi^2


* 5- Taux de distorsion harmonique (TDH)

Dans le cours on a vu que pour les fonctions impaires, le TDH s'exprimait:

$$\begin{align}
    TDH &= 100 \frac{ \sqrt{ \sum_{n=2}^{+\infty} b_n^2 }}{b_1} \\
\end{align}$$

Comme ici on a une fonction paire, on adapte la définition et on calcule:
$$\begin{align}
    TDH &= 100 \frac{ \sqrt{ \sum_{n=2}^{+\infty} a_n^2 }}{\vert a_1\vert } \\
\end{align}$$

Rappel: pour un cos pur, le TDH doit valoir 0\%    


```python
100 * sqrt(sum(a(n)^2,n,2,oo))/abs(a(1))
```

>    50*pi*sqrt(-sum(2*((-1)^n - 1)/n^4, n, 2, +Infinity)/pi^2)



On voit donc que Sage n'arrive pas à simplifier, on fait une troncature et on demande une évaluation numérique avec `N()`:


```python
N(100 * sqrt(sum(a(n)^2,n,2,1000))/abs(a(1)))
```

>    12.1152925831470


On remplace le terme $(-1)^n-1$ par $0$ pour les termes $n$ pairs, et $-2$ pour les termes $n$ impairs:

```python
TDH=100*sqrt(simplify(16/pi^2*sum(1/(2*k+1)^4, k, 1, +Infinity)))/abs(a(1))
show(TDH)
```

<html><script type="math/tex; mode=display">\newcommand{\Bold}[1]{\mathbf{#1}}25 \, \sqrt{\frac{1}{6}} \sqrt{\pi^{4} - 96}</script></html>



```python
N(TDH)
```

>    12.1152926519304



On retrouve bien un résultat proche de l'évaluation numérique par troncature de la somme.


### 4) Travail demandé

Dans un nouveau notebook, traiter de manière similaire les fonctions suivantes:
* La dent de scie $2\pi$-periodique égale à $x$ sur $]-\pi,\pi]$.
* (A FAIRE A LA FIN SI TOUT EST FINI: Le créneau $2\pi$-periodique impair égal à $1$ sur $]0,\pi[$.)


II) La transformée de Laplace (2 heures)
-----

**Attention**: la variable de la transformée de Laplace est parfois notée $p$, parfois $s$.

<h3>1) TL des fonctions de base</h3>

### 1) TL des fonctions de base

Soit une fonction $f$, on calcule la TL dans Sage:

```
t,s = var('t,s')
f = function('f')(t)
f(t).laplace(t,s)
```

ou bien:

```
f(t) = t*cos(t)
laplace(f,t,s)
```

    
Et la TL inverse:

```
t,s = var('t,s')
f(s)=s/(s^2 + 1)
f.inverse_laplace(s,t)
```

ou bien:

```
inverse_laplace(1/(s - 1), s, t)
```

**Exemple**: impulsion de Dirac $t \rightarrow \delta(t)$

L'impulsion de Dirac s'écrit `dirac_delta(t)` en Sage. Sa TL vaut:


```python
t,s = var('t,s')
dirac_delta(t).laplace(t,s)
```

1

On retrouve bien le résultat vu en cours.

### 2) Travail demandé


Pour chacune des fonctions ci-dessous: 

* calculer à la main la TL (ou la retrouver dans le tableau), et l'écrire en **markdown** dans votre notebook.
* vérifier les TL avec Sage.
 
Fonctions: 

| Fonction | Commande Sage |
|---|---|
|échelon $u(t)$|`heaviside(t)` |
|rampe||
|dérivée d'une fonction $f$ | ```f = function('f')(t); g = f.diff(t)```|


Références:
    
* transformée de laplace sous Sage: [laplace](https://doc.sagemath.org/html/en/reference/calculus/sage/calculus/calculus.html?highlight=dirac#sage.calculus.calculus.laplace)
* transformée inverse [inverse_laplace](https://doc.sagemath.org/html/en/reference/calculus/sage/calculus/calculus.html?highlight=dirac#sage.calculus.calculus.inverse_laplace)
* Sagebook p.232

### 3) Réponse d'un système

**Exemple:** premier ordre

Soit un système du premier ordre représenté par son entrée $e(t)$, sa sortie $s(t)$ et son équation différentielle: 
   
$$   
\tau \frac{ds(t)}{dt} + s(t) = Ke(t)  
$$  
    
Par calcul à la main, on trouve sa TL:

$$   
p \tau S(p) + S(p) = KE(p)  
$$  
   
    
On peut vérifier ce résultat à la main avec Sage:

```
t,p,tau = var('t,p,tau')
f= function('f')(t);
e= function('e')(t);
laplace( tau * f.diff(t) + f - e, t,p)
```

Ce qui donne:

```
(p*laplace(f(t), t, p) - f(0))*tau - laplace(_e(t), t, p) + laplace(f(t), t, p)
```

Et on en déduit la **fonction de transfert**:

$$   
H(p)= \frac{S(p)}{E(p)  } = \frac{K}{ 1+\tau p }
$$  


Pour obtenir la **réponse impulsionnelle**, on pose $E(p)=1$ car la transformée de Laplace du Dirac vaut $1$.

Puis on calcule la **transformée inverse** en recherchant dans la **table** du cours.  On trouve dans le cours que:

* $e^{-at} \stackrel{TL}{\rightarrow} \frac{1}{p+a}$  
* $\frac{1}{\tau}e^{-\frac{t}{\tau}} \stackrel{TL}{\rightarrow} \frac{1}{1+\tau p}$  


Puis on vérifie avec Sage:


```python
t,p,tau = var('t,p,tau')
f_impuls(t) = inverse_laplace( 1/(1+ tau*p), p,t   )
f_impuls
```

t |--> e^(-t/tau)/tau

Ce qui correspond à la formule du cours.


Puis en prenant par exemple $K=1, ~\tau=10^{-3}s$, on peut **tracer** le graphe:


```python
plot( exp(-t/0.001)/0.001, t, 0,0.01)
```

De même pour la **réponse indicielle**, c'est-à-dire la réponse à un échelon $u(t)$, on pose $E(p)=\frac{1}{p}$ car la TL d'un échelon est $\frac{1}{p}$.

On a donc :

$$
S(p) = \frac{K}{ 1+\tau p } \frac{1}{p}
$$

On effectue la DES (Décomposition en Elements simples) de la fraction rationelle avec Sage:


```python
t,p,tau = var('t,p,tau')
S(p) = 1/(1+ tau*p) *1/p
S(p).partial_fraction()
```


```python
-tau/(p*tau + 1) + 1/p
```

Puis on calcule la **transformée inverse** comme ci-dessus en recherchant les TL de chacun des termes dans la **table** du cours. On trouve:

$
s(t) = -\exp^{-\frac{t}{\tau}}+u(t)
$

où $u(t)$ est l'échelon.

On **vérifie** avec Sage:


```python
t,p,tau = var('t,p,tau')
s_indice(t) = inverse_laplace( 1/(1+ tau*p) *1/p , p,t   )
s_indice
```


```python
t |--> -e^(-t/tau) + 1
```


```python
On retrouve bien la même expression.

On trace: 
```


```python
plot( -exp(-t/0.001) + 1 , t, 0,0.01)
```

Enfin pour la **réponse en fréquence**, on remplace $p$ par $j\omega$ dans $H(p)$ et on calcule le module $\vert H(j\omega) \vert$:


```python
p,w = var('t,w')
H(p) = 1/(1+0.001*p)
plot( abs(H(i*w)), w,0,1000 , axes_labels=[r'$\omega$', r'$\vert H(j\omega) \vert$']) 
```

<h3>4) Travail demandé</h3>

### 4) Travail demandé

1. Faire toutes les étapes du 3).
1. Pour le système dont l'équation différentielle est la suivante :

$$   
\tau^2 \frac{d^2s(t)}{dt^2} + 2m \tau \frac{ds(t)}{dt} + s(t) = Ke(t)  
$$  


On demande de:
    
* calculer la TL et la fonction de transfert à la main et l'écrire en **markdown** dans votre notebook.
* vérifier la TL avec Sage.
* calculer à la main la réponse impulsionnelle et l'écrire en **markdown** dans votre notebook.
* vérifier avec Sage la réponse impulsionnelle et la tracer.
* calculer à la main la réponse indicielle et l'écrire en **markdown** dans votre notebook.
* vérifier avec Sage la réponse indicielle et la tracer.
* calculer et tracer la réponse en fréquence.

Pour les applications numériques, prendre $\tau=0.001 ~s, ~K=1$ et $m=0.42$.

Références:
    
    
* https://cpjobling.github.io/eg-247-textbook/labs/lab03/index.html
* https://www.engr.colostate.edu/ECE423/signals_labs/lab04/Lab_Notes_4.pdf
* https://people.eecs.ku.edu/~perrins/class/S08_360/lab/lab11.pdf


```python

```
