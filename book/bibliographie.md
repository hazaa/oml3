(biblio)=
Bibliographie
=====

%local bibliographies: https://jupyterbook.org/en/stable/content/citations.html#local-bibliographies
%```{bibliography}
%:filter: docname in docnames
%```

```{bibliography}
:style: alpha
```
