DS blanc 
======

Attention:

* Les documents et téléphones sont interdits.
* L'usage des calculatrices collège est autorisé.
* Durée: 1h30


1 DSF
-----

Soit $f(t)$ un signal 3-périodique défini par la rampe de valeur $2t$ sur $[0;3]$.

1. Calculez les coefficients de la décomposition en série de Fourier (DSF).
1. Ecrivez la DSF grâce au théorème de Dirichlet.
1. Calculez sa valeur efficace dans le domaine temporel. 
1. Calculez sa valeur efficace dans le domaine fréquentiel à l'aide de la Formule de Parseval.

2 SFD
-----

Soit $x[n]=\cos( \omega_0 n)$. On pose $\omega_0=\frac{4\pi}{N}$ avec $N=8$ un entier. 


1. Le signal est-il périodique ?
1. Calculer $X[k]$ les coefficients de la Série de Fourier Discrète (SFD) pour $k=0,1,2,3,4$.
1. En déduire $X[k]$ pour $k=0,-1,-2,-3$ (parité de $\mathcal{Re}(X[k])$ pour $x[n]$ réel).
1. En déduire $X[k]$ pour $k=5,6,7$ (périodicité de $X[k]$).


3 SFD
-----

Soit la suite $N$-périodique suivante:

$$
\begin{eqnarray}
x[n] &=& -1+ \cos(\frac{2\pi}{N}n)  + \sin(\frac{4\pi}{N}n)
\end{eqnarray}
$$

1. Pour chaque terme de la somme dans $x[n]$ calculer sa SFD. 
1. En déduire $X[k]$ pour $k \in [-2,2]$. 
1. Tracer $\mathcal{Re}(X[k])$ et $\mathcal{Im}(X[k])$.

4 Echantillonnage
-----

Ici on considère le temps comme continu.

On veut échantillonner des signaux analogiques issus de mesures. On représente 
leur spectre ci-dessous.

|signal a| signal b|
| ---      | ---      | 
|![voice](fig/spectre_voice.svg) | ![voice](fig/spectre_wide_bandwith.svg) |



Pour chaque signal répondre aux questions:

1. Le spectre est-il borné par une fréquence $f_{max}$ ?
1. Peut-on reconstruire le signal échantillonné sans perte ?
1. Si oui quelle est la fréquence d'échantillonnage minimale ?


5 TZ
-----

Soit le filtre à temps discret défini par l'équation de récurrence suivante:

$$
\begin{eqnarray}
y_k &=& x_k + x_{k-1}
\end{eqnarray}
$$

1. Ce filtre est-il récursif ou non ?
1. Calculez les $a_0, a_1, \ldots$ et $b_0, b_1, \ldots$. En déduire la transmittance en $z$, notée $H(z)$ associée à l'équation de récurrence.
1. Pour calculer la réponse en fréquence, on remplace $z$ par $e^{j \omega}$. Calculez le module $\vert H(e^{j \omega}) \vert $.
    Tracer le graphe de la réponse en fréquence en fonction de $\omega$, pour $\omega \in [0; \pi]$. 
1. Quel est le type du filtre (passe bas, passe-haut, ...) ?




Rappel:

* $\cos(2a)=2 \cos^2(a)-1$
* $\cos^2(a) + \sin^2(a)=1$
* $\sin(2a)=2\sin(a)\cos(a)$
