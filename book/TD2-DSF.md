# TD2 Révisions 


1 DSF
-----

Pour chacun des signaux suivants, il s'agit :

* de calculer ses coefficients de Fourier sur papier (en TP on les vérifiera sur un logiciel) ;
* d'écrire le théorème de Dirichlet sur papier ;
  *  (en TP on vérifiera en affichant la somme des premières harmoniques avec un logiciel) ;
  *  et éventuellement d'appliquer à certaines valeurs de $t$ spécifiées pour trouver la somme de certaines séries numériques ;
*  d'écrire la formule de Parseval pour en déduire la somme attendue.

Les  signaux :

*  Le créneau 4-périodique impair valant $-2$ sur $[ 0 , 2 ]$.  
*  Le triangle 2-périodique pair valant $2t-1$ sur $[0 , 1 ]$.



1 DSF
-----

Pour chacun des signaux suivants, il s'agit :

* de calculer ses coefficients de Fourier sur papier (en TP on les vérifiera sur un logiciel) ;
* d'écrire le théorème de Dirichlet sur papier ;
  *  (en TP on vérifiera en affichant la somme des premières harmoniques avec un logiciel) ;
  *  et éventuellement d'appliquer à certaines valeurs de $t$ spécifiées pour trouver la somme de certaines séries numériques ;
*  d'écrire la formule de Parseval pour en déduire la somme attendue.

Les signaux :


*  La rampe 3-périodique valant $2t$ sur $[ 0 , 3 ]$
*  Le trapèze 5-périodique pair dont le motif est délimité par les sommets de coordonnées $(-2.5,0), (-2,1), (2,1)$ et $(2.5,0)$.



2 Taux de Distorsion Harmonique (TDH)
-----

Soit le courant $i(t)$ représenté ci-dessous. 

1. Calculer et tracer son harmonique fondamental $H_1(t)$. 
1. Quel est son TDH ? Pour l'application numérique on approximera une somme infinie $\sum^{\infty}$ par ses premiers termes.

![exo_mohan](fig/creneau_exo_mohan.svg)


Source: {cite}`mohan_2011` example 8.1 p.144.


Bonus
-----

redressement simple/double alternance: 
[Cottet](univ.scholarvox.com.ezproxy.u-pec.fr/reader/docid/88902771/page/42)
