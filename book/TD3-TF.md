# TD3 Transformée de Fourier

```{danger}
ATTENTION: ces énoncés ne seront pas utilisés, car la série de Fourier en temps continu n'est plus vue en cours.
```

1 Convolution
-----

Calculer la convolution $(f \star g)(t)$ d'un signal porte par un autre signal porte.

Rappel:

$$
\begin{equation*}
\Pi(t)=
\left\{
\begin{array}{rl}
1 & \text{si } t \in [-\frac{1}{2},\frac{1}{2}],\\
0 & \text{sinon}.
\end{array} \right.
\end{equation*}
$$

1. Pour $t \leq -1$, dessiner $\Pi(\theta)$, $\Pi(t-\theta)$, et en déduire $(\Pi \star \Pi)(t)$
1. Idem pour $t \in [-1;0]$
1. Idem pour $t \in [0;1]$
1. Idem pour $t \geq 1$


2 Convolution
-----

Soit $\Pi(t)$ la fonction porte définie ci-dessus. On veut calculer $(\Pi \star \delta)(t)$.

1. Pour $t < -1/2$, dessiner $\Pi(\theta)$, $\delta(t-\theta)$, et en déduire $(\Pi \star \delta)(t)$
1. Idem pour $t \in [-1/2;1/2]$
1. Idem pour $t > 1/2$ 


3 TF d'une fonction porte
-----

Soit une fonction porte paramétrée par $\tau$ avec la définition suivante:

$$
\begin{equation*}
\Pi_\tau(t)=
\left\{
\begin{array}{rl}
1 & \text{si } t \in [-\frac{\tau}{2},\frac{\tau}{2}],\\
0 & \text{sinon}.
\end{array} \right.
\end{equation*}
$$

1. Calculez sa TF.


4 TF d'une impulsion laser
-----


On modélise une impulsion laser comme la multiplication d'une fonction porte (définie ci-dessus)
par une fonction cosinus de fréquence $f$.

$$
\begin{eqnarray}
x(t) = \Pi_\tau(t) cos(2\pi f_0 t)
\end{eqnarray}
$$


1. Rappelez l'expression de la TF d'un cosinus.
1. Rappelez l'expression de la TF du produit de deux signaux temporels.
1. En déduire la TF de $x(t)$.
1. Que se passe-t-il quand $\tau$ diminue ?



5 DSF et TF du peigne de Dirac
-----

Soit $\delta_\tau(t)$ un créneau de période $T_e$ valant $\frac{1}{\tau}$ sur 
$[-\frac{\tau}{2} ; \frac{\tau}{2} ]$ et $0$ ailleurs.

Comme $\delta_\tau(t)$ est périodique de période $T_e=\frac{1}{f_e}$
on peut calculer sa DSF à coefficients complexes  $\sum_{n=-\infty}^{+\infty} c_n e^{j 2  \pi n f_e t}$.

1. Calculer les coefficients complexes $c_n$.
1. Faire tendre $\tau$ vers $0$.
1. Retrouver la formule du cours sur la TF du peigne de Dirac.

NB: cf {cite}`quere_2013` p.81-82.
