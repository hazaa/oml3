(CM_revisions)=
# CM Révisions


Dans cette partie du cours nous révisons une partie d'OML2 pour préparer les chapitres suivants.

`````{admonition} Prérequis

OML2, 1ere année, lien direct [sharepoint](https://upecnumerique.sharepoint.com/sites/OML/Supports%20de%20cours/Forms/AllItems.aspx). Chapitres:

*  Développement en Série de Fourier.
*  Transformée de Laplace.
*  Séries entière $\sum x^k$.

`````


I) Révision: développement en série de Fourier (DSF) des fonctions périodiques
-----


```{note}
Cette partie du cours est adaptée du cours de Guillaume Bourlet à l'IUT SF.
```

```{important}
La DSF est au centre de la théorie de Fourier. Elle signifie qu'on peut **reconstruire un signal continu et périodique**
(sous-entendu non sinusoïdal) comme **somme infinie de signaux sinusoïdaux** (ses harmoniques).
```



````{prf:definition}
:label: DSF

**Coefficients de Fourier réels**

Les coefficients de Fourier d'une fonction T-périodique sont définis par les formules :

$$
\begin{eqnarray}
a_0 &=&  \frac{1}{T} \int_{[T]} f(t) dt  \\
a_n &=& \frac{2}{T} \int_{[T]} f(t) \cos(n \omega t) dt \\
b_n &=& \frac{2}{T} \int_{[T]} f(t) \sin(n \omega t) dt 
\end{eqnarray}
$$

Notation: $\int_{[T]}$ signifie que l'intégrale est calculée sur une période quelconque, par exemple $[t_0; t_0+T]$.
````

````{prf:example}
:label: DSF-example_creneau

Le créneau impair $C(t)$, 2-périodique valant $1$ sur $]0;1[$

![creneau](fig/creneau.svg)


On a $T=2$ et $\omega=\frac{2\pi}{T}=\pi$

* Calcul de $a_0$

Le créneau est impair donc sa valeur moyenne sur une période est nulle.

$a_0=0$

*  Calcul de $a_n, n \geq 1$ 

$$
\begin{eqnarray}
a_n &=& \frac{2}{T} \int_{[T]} C(t) \cos(n \omega t) dt \\
\end{eqnarray}
$$

Comme le créneau $C(t)$ est impair et que la fonction 
$cos⁡(n \omega t)$ est paire, le produit $C(t)cos⁡(n\omega t)$ est
impair donc son intégrale est nulle sur toute période.

$\forall n \geq 1, a_n=0$

* Calcul de $b_n, n \geq 1$ 

$$
\begin{eqnarray}
b_n &=& \frac{2}{T} \int_{[T]} C(t) \sin(n \omega t) dt \\
\end{eqnarray}
$$

Comme le créneau $C(t)$ est impair et que la fonction 
$sin⁡(n\omega t)$ est impaire, le produit $C(t)sin⁡(n\omega t)$ 
est pair donc :

$$
\begin{eqnarray}
\int_{-T/2}^{T/2} C(t) \sin(n \omega t) dt  &=& 2 \int_{0}^{T/2} C(t) \sin(n \omega t) dt \\
\end{eqnarray}
$$

On calcule donc :

$$
\begin{eqnarray}
b_n &=& 2 \frac{2}{2} \int_{0}^1 C(t) \sin(n \omega t) dt \\
&=& 2  \int_{0}^1 \sin(n \omega t) dt \\
&=& 2  \Bigg[ \frac{ -\cos(n \omega t) }{n \omega} \Bigg]_{0}^1 \\
&=& -\frac{ 2}{n \omega}  \Bigg(  \cos(n \omega )  -\cos(0) \Bigg) \\
&=& -\frac{ 2}{n \pi}  \Bigg(  \cos(n \pi )  -\cos(0) \Bigg) \\
&=& -\frac{ 2}{n \pi}  \Bigg( (-1)^n -1 \Bigg) \\
&=& \frac{ 2}{n \pi}  \Bigg( 1 - (-1)^n  \Bigg) \\
\end{eqnarray}
$$

* Bilan:

$$
\begin{eqnarray}
a_0 &=& 0 \\
\forall n \geq 1, a_n &=& 0 \\
\forall n \geq 1, b_n &=& \frac{ 2}{n \pi}  \Bigg( 1 - (-1)^n  \Bigg)  \\
\end{eqnarray}
$$

````

```{hint}
L'exploitation de la **parité** du créneau nous a permis de **réduire les calculs** de façon significative.

Soit $f$ une fonction $T$-périodique.

* Si $f$ est paire :

   *  les $b_n$ sont nuls (et donc sa série de Fourier ne contient que des cosinus qui sont des fonctions paires).
   *  $a_0 = \frac{1}{T} \int_{[T]} f(t)dt =  \frac{2}{T} \int_{0}^{T/2} f(t)dt$
   *  $\forall n \geq 1, a_n = \frac{2}{T} \int_{[T]} f(t) \cos(n\omega t) dt =  \frac{4}{T} \int_{0}^{T/2} f(t) \cos(n\omega t) dt$
   *  **Explication**:  Comme $f(t)$ et $f(t)\cos⁡(n\omega t)$ sont paires, leur intégrale sur $[−T/2,T/2]$ vaut le double de l’intégrale sur $[0,T/2]$.

* Si $f$ est impaire :

   *  les $a_n$ sont nuls (et donc sa série de Fourier ne contient que des sinus qui sont des fonctions impaires).
   *  $\forall n \geq 1, b_n = \frac{2}{T} \int_{[T]} f(t) \sin(n\omega t) dt =  \frac{4}{T} \int_{0}^{T/2} f(t) \sin(n\omega t) dt$
   *  **Explication**:  Comme $f(t)\sin⁡(n\omega t)$ est paire, son intégrale sur $[−T/2,T/2]$ vaut le double de l’intégrale sur $[0,T/2]$.
  

```

````{prf:definition}
**Les harmoniques**  sont des fonctions de $t$. Elles sont définies par les formules suivantes :

$$
\begin{eqnarray}
 H_0 &=& a_0 \\
\forall n \geq 1, H_n(t) &=& a_n \cos(n \omega t) + b_n \sin(n \omega t)\\
\end{eqnarray}
$$

````

````{prf:definition}
:label: DSF-fondamental

La composante $H_1(t)$ s'appelle le **fondamental**. Elle a la même période $T$ que le signal $f(t)$ dont on calcule la DSF.
````

````{prf:definition}
**Le théorème de Dirichlet**

Le théorème de Dirichlet dit que sous certaines conditions qui seront toujours réalisées en GEII, en toute valeur $t$ où $f$ périodique est continue :

$$
\begin{eqnarray}
f(t) &=&  a_0 + \sum_{n=1}^{+\infty} H_n(t) \\
&=& a_0 + \sum_{n=1}^{+\infty} a_n \cos(n \omega t)+b_n \sin(n \omega t)
\end{eqnarray}
$$

**Que se passe-t-il aux points de discontinuité de f ?**

En GEII, les points de discontinuité sont des valeurs $t_0$ 
en lesquelles la fonction présente un saut fini.
En ces valeurs, la somme infinie des harmoniques ne redonne pas $f(t_0)$ mais la demi-somme (ou la moyenne) des valeurs à gauche et à droite du saut.
````

````{prf:example}
Le théorème de Dirichlet appliqué à notre créneau impair, 2-périodique valant $1$ sur $]0,1[$.

On va reconstruire notre créneau en faisant la somme d'une "quantité suffisante d'harmoniques pour obtenir une bonne valeur approchée de signal".

On pose $S_N(t)=H_0 + \sum_{n=1}^{N} H_n(t)$

$S_N$ est la somme partielle des $N+1$ premières harmoniques.

Pour notre créneau, cela donne :

$$
\begin{eqnarray}
S_N(t)&=& \sum_{n=1}^{N} \frac{ 2}{n \pi}  \Bigg( 1 - (-1)^n  \Bigg) \sin(n \pi t)  \\ 
\end{eqnarray}
$$

Prenons $N=20$:

$$
\begin{eqnarray}
S_N(t)&=& \sum_{n=1}^{20} \frac{ 2}{n \pi}  \Bigg( 1 - (-1)^n  \Bigg) \sin(n \pi t)  \\ 
\end{eqnarray}
$$

La somme des 21 premières harmoniques reconstruite avec Sage:
 
```python
a0=0
var('n')
assume(n,'integer')
omega=pi
b(n) =  2/(n *pi) * ( 1 - (-1)^n ) 
S(x)=a0+sum(b(n)*sin(n*omega*x),n,1,20)
plot(S(x),(-2,2))
```

![creneau_dirichlet](fig/creneau_dirichlet.png)

On constate une **distorsion** au voisinage des points de **discontinuité**. C'est le phénomène de [Gibbs](https://fr.wikipedia.org/wiki/Ph%C3%A9nom%C3%A8ne_de_Gibbs).

````


````{prf:definition}
**La formule de Parseval**

En théorie du signal, la **puissance moyenne** d'un signal T-périodique f(t) est:

$$
\begin{eqnarray}
P &=& \frac{1}{T}\int_{[T]} \vert f(t) \vert^2 dt
\end{eqnarray}
$$

C'est en fait la valeur moyenne du carré du module du signal sur une période.

L'égalité de Parseval permet d'exprimer cette puissance moyenne comme somme d'une série numérique basée sur les coefficients de Fourier.

Soit $f$ une fonction $T$-périodique vérifiant les conditions habituelles en GEII.

Soit $a_n$ et $b_n$ les suites de ses coefficients de Fourier.

Alors:

$$
\begin{eqnarray}
\frac{1}{T}\int_{[T]} \vert f(t) \vert^2 dt &=& a_0^2 + \frac{1}{2} \sum_{n=1}^{+\infty} (a_n^2 + b_n^2)
\end{eqnarray}
$$
````

````{prf:definition}
:label: DSF-spectre
**Spectre:**  le diagramme qui à chaque harmonique
de fréquence $nf=n/T$ associe la valeur $\sqrt{a_n^2+b_n^2}, ~n\geq 0$ s'appelle le spectre du signal. 
````

![spectre](fig/spectre_DSF.svg)

````{prf:property}
:label: DSF-spectre-prop
Le spectre d'un signal $u(t)$ périodique est **discret**.

````


`````{admonition} Exemple GEII: Distorsion Harmonique
:class: GEII

Quand on connecte une charge **non-linéaire** sur la tension du secteur,
la tension et l'intensité sont des sinusoïdes
déformées. Comment **quantifier la déformation** ? 

**Rappel**: Valeur efficace 

$$
\begin{eqnarray}
 Y_{eff} &=& \sqrt{ \frac{1}{T} \int_{0}^{T}y^2(t)dt} \\
 &=& \sqrt{ \sum_{1}^{\infty} b_n^2} ~(\text{Parseval})
\end{eqnarray}
$$

où on a supposé que $y$ était **impaire** pour simplifier, 
et où $b_n$ sont ses coefficients de Fourier impairs.

**Taux de Distorsion Harmonique**:

Ce taux s'interprète comme le ratio:

$$
100 \frac{\text{valeur efficace de} ~y(t) \text{ sans son premier harmonique}}{\text{valeur efficace du premier harmonique}}
$$

Grâce à l'égalité de Parseval, on peut le ré-écrire sous la forme:

$$
\begin{eqnarray}
 \text{TDH} &=& 100 \frac{\sqrt{ \sum_{\mathbf{n=2}}^{\infty} b_n^2 } }{b_1}
\end{eqnarray}
$$


**Exemple:** variateur de vitesse

![TDH](fig/TDH.svg)


source: cours F.Vallenet, GEII, IUT-SF.
`````


### Ecriture complexe du DSF d'une fonction périodique



````{prf:definition}
:label: fourier-complexe

Soit une fonction $f(t)$ **périodique** de période $T$ et $\omega=\frac{2\pi}{T}$.
On peut écrire de manière équivalente la DSF avec les coefficients complexes $c_n$ ci-dessous:

$$
\begin{eqnarray}
c_n &=& \frac{1}{T} \int_{[T]} f(t) e^{-jn\omega t} dt 
\end{eqnarray}
$$

Et en tout point de continuité:

$$
\begin{eqnarray}
f(t) &=& \sum_{n=-\infty}^{+\infty} c_n e^{jn\omega t}
\end{eqnarray}
$$
````

````{prf:proof}
Pour revenir à l'expression données par le théorème de Dirichlet, on part de l'équation 
$f(t) = \sum_{n=-\infty}^{+\infty} c_n e^{jn\omega t}$.

On remplace $c_n$ par sa valeur, et $e^{jn\omega t}$ par son expression avec $\cos$ et $\sin$.
Puis on sépare les termes réels d'un côté et les termes complexes de l'autres.
Comme le signal $f(t)$ est supposé réel ici, la partie imaginaire est nulle.

Du fait de la parité, les sommes $\sum_{n=-\infty}^{+\infty}$ se réecrivent facilement à partir de $\sum_{n=1}^{+\infty}$. 
````

````{prf:definition}
:label: fourier-complexe

On a la relation suivante entre $c_n, ~a_n, ~b_n$:

$$
\begin{eqnarray}
\forall n \neq 0, ~c_n &=& \frac{1}{2} (a_n - jb_n) \\
c_0 &=& a_0
\end{eqnarray}
$$
````

````{prf:proof}
Partir de la définition $c_n = \frac{1}{T} \int_{[T]} f(t) e^{-jn\omega t} dt $, séparer les termes
réels et imaginaires, puis identifier.
````


````{prf:example}
:label: fourier-complexe-example

Vérifier qu'on retrouve bien l'expression de $f(t)$ avec la DSF complexe.

````

```{important}
Le théorème de Dirichlet est au centre de la théorie de Fourier. Il signifie qu'on peut **reconstruire un signal continu et périodique** (sous-entendu non sinusoïdal) comme **somme infinie de signaux sinusoïdaux** (ses harmoniques).
```


(transformee_laplace)=
II) Transformée de Laplace
-----

`````{admonition} Prérequis

OML, 1ere année

*  Fichiers `S27.pdf` et `S28.pdf`

`````


La Transformée de Laplace (TL) est **l'outil de base** en automatique/asservissements
pour étudier le comportement des régulations (systèmes bouclés).

````{prf:definition}
:label: def-causal

**Causal**: un signal $s(t)$ est dit causal si $s(t)=0$ pour $t<0$.
````

````{prf:definition}
:label: def-laplace

**Transformée de Laplace (TL)**: soit un signal $f(t)$, sa TL est:

$$
\begin{eqnarray}
\mathcal{L}(f)(p) = F(p) &=& \int_0^{+\infty} e^{-pt} f(t) dt
\end{eqnarray}
$$


* Rappel: Lien [Vidéo](https://upecnumerique.sharepoint.com/:v:/r/sites/OML1-FI1/Supports%20de%20cours/Semestre%202/S27_Laplace1_FctCausales.mp4?csf=1&web=1&e=pNWnun) OML2.

````

````{prf:example}
**Echelon unité**:

L'échelon unité $u(t)$ vaut $1$ si $t>0$ et $0$ sinon.

![echelon](fig/echelon_unite.svg)

Calculons sa TL:

$$
\begin{eqnarray}
\mathcal{L}(u)(p) &=& \int_{0}^{+\infty} e^{-p t}. 1 ~dt \\
U(p) &=&   \Big[ -\frac{1}{p} e^{-pt}\Big]_{0}^{+\infty} 
&=&  -\frac{1}{p} ( 0 -1 )
&=& \frac{1}{p} 
\end{eqnarray}
$$


* Rappel définition échelon unité: Lien [Vidéo](https://upecnumerique.sharepoint.com/:v:/r/sites/OML1-FI1/Supports%20de%20cours/Semestre%202/S27_Laplace1_FctCausales.mp4?csf=1&web=1&e=pNWnun) OML2, vers 1min15.
* Rappel du calcul de la TL de l'échelon:  [Vidéo]( https://upecnumerique.sharepoint.com/:v:/r/sites/OML1-FI1/Supports%20de%20cours/Semestre%202/S27_Laplace2_Def.mp4?csf=1&web=1&e=7ARmQY) OML2, vers 2min40.
````


````{prf:definition}
:label: Dirac

L'**impulsion de Dirac** est un signal idéal tel que:

$$
\begin{eqnarray}
\delta(t) &=& 0 ~\textrm{si} ~t \neq 0  \\
\int_{-\infty}^{+\infty}\delta(x) dx&=&1
\end{eqnarray}
$$

On peut le voir comme le passage à la limite du signal suivant quand $A \rightarrow +\infty$:

![dirac](fig/dirac.svg)

Et sa représentation symbolique:
![dirac](fig/dirac_symbole.svg)

Idem avec un retard $t_0$:
![dirac_delay](fig/dirac_retard_symbole.svg)

````

````{prf:example}
:label: TL-dirac-example

**Impulsion de Dirac**:

Dans cet example on approxime l'**impulsion de Dirac** par la fonction suivante:

![dirac](fig/dirac2.svg)

$$
\begin{equation*}
\delta_\varepsilon(t)=
\left\{
\begin{array}{rl}
\frac{1}{\varepsilon} & \text{si } t \in [0,\varepsilon],\\
0 & \text{sinon}.
\end{array} \right.
\end{equation*}
$$

On voit bien que $\int_0^{+\infty} \delta_\varepsilon(t) =1$ quand $\varepsilon \rightarrow 0$.

Calculons sa TL:

$$
\begin{eqnarray}
TL[\delta_\varepsilon(t)](p) &=& \int_{0}^{\varepsilon} \frac{1}{\varepsilon}. e^{-p t}dt \\
 &=& -\frac{1}{p} \frac{1}{\varepsilon} \Big[  e^{-pt}\Big]_{0}^{\varepsilon} \\
 &=& -\frac{1}{p \varepsilon}\Big(  e^{-p \varepsilon} -1 \Big) \\
\end{eqnarray}
$$

**Rappel**: $e^x \approx 1+x$ quand $x$ proche de 0 (voir OML2 Séries de Taylor, lien [vidéo](https://upecnumerique.sharepoint.com/:v:/r/sites/OML1-FI1/Supports%20de%20cours/Semestre%202/S23-S%C3%A9ries2-Formule_de_Taylor.mp4?csf=1&web=1&e=sJSUcK)).
Donc:

$$
\begin{eqnarray}
TL[\delta_\varepsilon(t)](p) &=& \frac{1-(1- p \varepsilon)}{p \varepsilon} \\
 &=& 1
\end{eqnarray}
$$

Donc $\lim_{\varepsilon \rightarrow 0} TL[\delta_\varepsilon(t)](f) =1$

On en déduit que la TL d'une impulsion de Dirac vaut $1$.

* Rappel définition Dirac: Lien [Vidéo](https://upecnumerique.sharepoint.com/:f:/r/sites/OML1-FI1/Supports%20de%20cours/Semestre%202?csf=1&web=1&e=mq8gm8) OML2, vers 8min30.
````



````{prf:property}
**Linéarité:** soient $\alpha,\beta$ des réels. Alors:

$$
\begin{eqnarray}
\mathcal{L}(\alpha f + \beta g)(p) &=& \alpha F(p) + \beta G(p) 
\end{eqnarray}
$$
````

````{prf:property}
**Homothétie:**  soit $a$ un réel. Soit $F(p)$ la TL de $f(t)$.
Alors $\mathcal{L}( f(at) )(p)=\frac{1}{|a|}F(\frac{p}{a}) $
````
````{prf:proof}

Par changement de variable.
````

````{prf:property}
**Retard:** la TL d'un signal $f(t)$ décalé d'une durée $a$
vaut $\mathcal{L}(f(t-a))(p) =  e^{-pa} F(p)$
````
````{prf:proof} 

Par changement de variable: 

$$
\begin{eqnarray}
\mathcal{L}(f(t-a))(p) &=& \int_{0}^{+\infty} e^{-p t} f(t-a) dt \\
\end{eqnarray}
$$

On pose le changement de variable $t-a=u$, d'où $dt=du$. On a:

$$
\begin{eqnarray}
\mathcal{L}(f(t-a))(p) &=& \int_{u(0)}^{u(+\infty)} e^{-p (u+a)} f(u) du \\
&=&  \int_{u(0)}^{u(+\infty)} e^{-p a} e^{-p u} f(u) du \\
&=&  e^{-p a} \int_{u(0)}^{u(+\infty)}  e^{-p u} f(u) du \\
&=&  e^{-p a} \int_{-a}^{u(+\infty)}  e^{-p u} f(u) du   \\
&=&  e^{-p a} \int_{0}^{u(+\infty)}  e^{-p u} f(u) du  ~\textrm{car la fonction est causale} \\
&=&  e^{-p a} F(p) \\
\end{eqnarray}
$$

Détail: voir cours M.Soranzo S27.pdf p.7

% Lien [Vidéo](https://upecnumerique.sharepoint.com/:v:/r/sites/OML1-FI1/Supports%20de%20cours/Semestre%202/S27_Laplace5_Retard.mp4?csf=1&web=1&e=6vAJs0) OML2.
````

````{prf:property}
**Dérivation:** soit $f(t)$ une fonction. 
Alors $\mathcal{L}( \frac{d ~f(t)}{dt} )= p F(p) -f(0) $
````
````{prf:proof} 

Par IPP.

$$
\begin{eqnarray}
\mathcal{L}( f'(t))(p) &=& \int_{0}^{+\infty} f'(t) e^{-p t}  dt 
\end{eqnarray}
$$

On se souvient de la formule de l'IPP: $\int u'v = [uv]-\int uv'$.

On identifie $v$ avec $e^{-pt}$ et $u'$ avec $f'$. D'où:

$$
\begin{eqnarray}
\mathcal{L}( f'(t))(p) &=& [f(t)e^{-p t}]_{0}^{+\infty} - \int_{0}^{+\infty} f(t)  \frac{d}{dt} \Big(e^{-p t} \Big)  dt \\
&=& \lim_{+\infty} f(t)e^{-p t} - f(0)e^0 - \int_{0}^{+\infty} f(t) (-p) e^{-p t}  dt \\
&=& 0- f(0)- (-p)  \int_{0}^{+\infty} f(t) e^{-p t}  dt \\
&=& p  \int_{0}^{+\infty} f(t) e^{-p t}  dt  - f(0)\\
&=& p  \mathcal{L}(f)(p) - f(0)
\end{eqnarray}
$$


Détail: cf cours M.Soranzo S28.pdf p.1

%*  Lien [Vidéo](https://upecnumerique.sharepoint.com/:v:/r/sites/OML1-FI1/Supports%20de%20cours/Semestre%202/S27_Laplace4_Deriv%C3%A9.mp4?csf=1&web=1&e=bBMb61) OML2, vers 0min45.
````

````{prf:property}
**Dérivation:** dans les conditions de **Heaviside**:

$0=f(0)=f'(0)=\ldots$

Alors la formule précédente devient: $\mathcal{L}( \frac{d ~f(t)}{dt})= p F(p) $
````

````{prf:property}
**Intégration:** 

$\mathcal{L}( \int f)(p)= \frac{1}{p}F(p)$
````

````{prf:proof} 
Par IPP.
````


`````{admonition} Exemple: Fonction de transfert
:class: GEII

Soit le circuit RC suivant, $e(t)$ la tension en entrée et $s(t)$ la tension en sortie.


![RC_filter](fig/RC_filter.svg)

On écrit la loi des mailles et la tension aux bornes du condensateur  :

$$
\begin{eqnarray}
e(t) &=& R i(t) + s(t) \\
i(t) &=& C \frac{d~s}{dt} 
\end{eqnarray}
$$

D'où:

$$
\begin{eqnarray}
e(t) &=& R C \frac{d~s}{dt} + s(t) 
\end{eqnarray}
$$

Puis on calcule la Transformée de Laplace des deux côtés:

$$
\begin{eqnarray}
E(p) &=& RC (p S(p) - s(0)) + S(p) 
\end{eqnarray}
$$

En supposant $s(0)=0$, et en posant $\tau=RC$:

$$
\begin{eqnarray}
E(p) &=& (1+\tau p) S(p) 
\end{eqnarray}
$$

Finalement on obtient la fonction de transfert $H(p)$:

$$
\begin{eqnarray}
S(p)  &=&  H(p) E(p) \\
H(p)  &=&  \frac{1}{1+\tau p} 
\end{eqnarray}
$$

```{note}
En automatique, on dit que ce système est du **premier ordre**.
```

* Lien OML2 [Vidéo](https://upecnumerique.sharepoint.com/:v:/r/sites/OML1-FI1/Supports%20de%20cours/Semestre%202/S28-LaplaceComp7-FctTransfert.mp4?csf=1&web=1&e=AFl0y5) 
`````


````{prf:definition}
On appelle **réponse impulsionnelle** la sortie temporelle du système, $s(t)$,
quand l'entrée est l'impulsion de Dirac $e(t)=\delta(t)$. 
````


`````{admonition} Example GEII: réponse impulsionnelle du circuit RC
:class: GEII

On reprend l'exemple du circuit RC. On a vu que:

$$
\begin{eqnarray}
H(p)  &=&  \frac{1}{1+\tau p} 
\end{eqnarray}
$$

On a vu en {prf:ref}`TL-dirac-example` que la TL du Dirac valait $E(p)=1$.
On a donc:

$$
\begin{eqnarray}
S(p)  &=&  H(p) E(p) \\
  &=&  \frac{1}{1+\tau p} 
\end{eqnarray}
$$

On prend la $TL^{-1}$ de deux côtés et on obtient:

$$
\begin{eqnarray}
TL^{-1} [ S(p) ]  &=&  TL^{-1} [ \frac{1}{1+\tau p} ] \\
\end{eqnarray}
$$

La $TL^{-1}$ de $S(p)$ n'est autre que $s(t)$. D'où:

$$
\begin{eqnarray}
s(t) &=&  TL^{-1} [ \frac{1}{1+\tau p} ] \\
\end{eqnarray}
$$

Pour obtenir la $TL^{-1}$ de $\frac{1}{1+\tau p}$ on se reporte au tableau 
récapitulatif ci-dessous qui nous donne:

$$
\begin{eqnarray}
TL^{-1} [ \frac{1}{p+a} ] &=& e^{-at}
\end{eqnarray}
$$

Finalement:

$$
\begin{eqnarray}
s(t) &=&  TL^{-1} [ \frac{1}{1+\tau p} ] \\
&=& TL^{-1} \Bigg[ \frac{1}{\tau} \frac{1}{\frac{1}{\tau} +p} \Bigg]\\
&=& \frac{1}{\tau}  TL^{-1}   \Bigg[  \frac{1}{\frac{1}{\tau} +p} \Bigg] \\
&=& \frac{1}{\tau}  e^{-\frac{t}{\tau}}
\end{eqnarray}
$$

![RC_impuls](fig/RC_reponse_impulsionnelle.svg)

Lien OML2 [Vidéo](https://upecnumerique.sharepoint.com/:v:/r/sites/OML1-FI1/Supports%20de%20cours/Semestre%202/S28-LaplaceComp7-FctTransfert.mp4?csf=1&web=1&e=AFl0y5) et [Vidéo](https://upecnumerique.sharepoint.com/:v:/r/sites/OML1-FI1/Supports%20de%20cours/Semestre%202/S28-LaplaceComp8-FctTransfert2.mp4?csf=1&web=1&e=TDaSk6)
`````
````{prf:proof} Vérification avec Sage

Avec Sage on peut vérifier directement la transformée de Laplace inverse

```python
tau, p = var('tau, p')
S(p) = 1/(1+tau*p)
S(p).inverse_laplace(p, t)
# output: e^(-t/tau)/tau
```

````


````{prf:definition}
On appelle **réponse indicielle** la sortie temporelle du système, $s(t)$,
quand l'entrée est l'échelon unité ($e(t)=1$ si $t<0$ et $0$ sinon). 
````


`````{admonition} Example GEII: réponse indicielle du circuit RC
:class: GEII

L'entrée est un échelon, dont la TL est $\frac{1}{p}$ comme vu ci-dessus.

$$
\begin{eqnarray}
S(p)  &=&  H(p). E(p) \\
  &=&  \frac{1}{1+\tau p}. \frac{1}{p} 
\end{eqnarray}
$$

On prend la $TL^{-1}$ de deux côtés et on obtient:

$$
\begin{eqnarray}
s(t) &=&  TL^{-1} [ \frac{1}{p} .  \frac{1}{1+\tau p} ] \\
\end{eqnarray}
$$


D'abord on effectue la Décomposition en éléments simples (DES):

$$
\begin{eqnarray}
  \frac{1}{p} . \frac{1}{1+\tau p}  &=&   \frac{a}{p} + \frac{b}{1+\tau p}   \\
\end{eqnarray}
$$

Pour trouver $a$ on multiplie des deux côtés par $p$ et on pose $p=0$. On a:

$$
\begin{eqnarray}
\frac{1}{1+\tau p}  &=&   a + \frac{b p}{1+\tau p}   \\
a &=& 1
\end{eqnarray}
$$

De même pour trouver $b$ on multiplie des deux côtés par $1+\tau p$ et on pose $p=-\frac{1}{\tau}$.
On a:

$$
\begin{eqnarray}
\frac{1}{p} &=&   b   \\
b &=& -\tau
\end{eqnarray}
$$

On continue avec la $TL^{-1}$:

$$
\begin{eqnarray}
s(t) &=&  TL^{-1} [ \frac{1}{p} - \frac{\tau}{1+\tau p} ] \\
&=&  TL^{-1} [ \frac{1}{p}]  -  TL^{-1} [\frac{\tau}{1+\tau p} ] \\
\end{eqnarray}
$$

On sait que la $TL^{-1}$ de $\frac{1}{p}$ est $u(t)$ l'échelon unité.
De plus on a déjà trouvé la $TL^{-1}$ de $\frac{1}{1+\tau p}$ ci-dessus:

$$
\begin{eqnarray}
TL^{-1} \Bigg[ \frac{1}{\tau +p} \Bigg] &=& \frac{1}{\tau}  e^{-\frac{t}{\tau}}
\end{eqnarray}
$$

On l'injecte dans $s(t)$:

$$
\begin{eqnarray}
s(t) &=&  u(t) -TL^{-1} [ \frac{\tau}{1+\tau p} ] \\
&=& u(t) -\tau \frac{1}{\tau}  e^{-\frac{t}{\tau}} \\
&=& u(t) -  e^{-\frac{t}{\tau}}
\end{eqnarray}
$$
où $u(t)$ est la fonction échelon.


![RC_indice](fig/RC_reponse_indicielle.svg)

NB: on retrouve bien le comportement de charge du condensateur.

Lien OML2 [Vidéo](https://upecnumerique.sharepoint.com/:v:/r/sites/OML1-FI1/Supports%20de%20cours/Semestre%202/S28-LaplaceComp9-FctTransfert3.mp4?csf=1&web=1&e=nQBd2Y)
`````

````{prf:proof} Vérification avec Sage

Avec Sage on peut d'abord vérifier notre DES:


```python
tau, p = var('tau, p')
X(p) = 1/(p*(1+tau*p))  
X(p).partial_fraction()
# output: -tau/(p*tau + 1) + 1/p
```

On peut aussi vérifier directement la transformée de Laplace inverse

```python
tau, p = var('tau, p')
X(p) = 1/(p*(1+tau*p))  
X(p).inverse_laplace(p, x)
# output: -e^(-t/tau) + 1
```

````

### Résumé de quelques TL connues

| Fonction | Transformée de Laplace  | 
| ---      | ---      | 
| échelon unité $u(t)$| $\frac{1}{p}$|
| Dirac $\delta(t)$| 1|
| $t$ | $\frac{1}{p^2}$|
| $\alpha f(t) + \beta g(t)$  | $\alpha F(p) + \beta G(p)$|
| $ f(at)$ | $\frac{1}{\vert a \vert}F(\frac{p}{a})$|
| $ f(t-a)$ | $e^{-pa} F(p)$|
| $\frac{d ~f(t)}{dt}$  | $p F(p) -f(0)$ |
|$\int f$|$\frac{1}{p}F(p)$|
| $e^{-at}$  | $\frac{1}{p+a}$  | 
| $e^{at}$  | $\frac{1}{p-a}$  | 
| $e^{at}f(t)$  | $\mathcal{L}(f) \Big\arrowvert_{p-a}$  | 
| $\cos(\omega t)$| $\frac{p}{p^2+\omega^2}$|
| $\sin(\omega t)$| $\frac{\omega}{p^2+\omega^2}$|

NB: certaines formules n'ont pas été prouvées ci-dessus, mais le sont dans le cours de 1ere année.

%TODO
%* (l'inverse ??)
%* le tableau recap 
%* exemple: l'inverse d'un passe bas $a/(p-p_i)$
%sources: cottet 4.1

(CM_revisions_series)=
III) Séries
-----

````{prf:definition}
On appelle **série** une somme infinie $\sum_{n=0}^{+\infty} u_n $ dans laquelle $u_n$ est une suite réelle.

L'étude des séries, et en particulier les conditions de leur convergence ne sera pas vue en cours.
````


````{prf:definition}
On appelle **série entière** une série de la forme $\sum u_n x^n$ dans laquelle $u_n$ est une suite réelle et $x$ une variable réelle.
````

````{note}
La notion de série entière généralise en quelque sorte la notion de polynôme. Les séries entières sont des "polynômes ayant un nombre infini de termes".
````

````{prf:example}
* $\sum nx^n, \sum \frac{x^n}{n}, \sum \frac{x^n}{n!}, \sum \frac{x^{2n}}{(n+1)!}$ sont des séries entières.
* $\sum n e^{x/n}, \sum \frac{\cos⁡(nx)}{n}, \sum \frac{\ln⁡(nx)}{n!}$ ne sont pas des séries entières.
````

````{prf:example}
En OML2 vous avez vu les **séries de Taylor**, qui sont des cas particuliers de séries entières. 

Exemple:

$$
\begin{eqnarray}
e^x &=& \sum_0^{+\infty} \frac{x^n}{n!}, \forall x \in \mathbb{R} \\
&=& 1 + \frac{x}{1} + \frac{x^2}{2!} + \frac{x^3}{3!} \ldots
\end{eqnarray}
$$

où $n!$ est la factorielle, et vaut $n!=n.(n-1).(n-2) \ldots 3.2.1$

Lien OML2 [vidéo](https://upecnumerique.sharepoint.com/:v:/r/sites/OML1-FI1/Supports%20de%20cours/Semestre%202/S23-S%C3%A9ries2-Formule_de_Taylor.mp4?csf=1&web=1&e=RAi9lJ)
````

```{important}
On peut montrer que pour $\vert x \vert <1$:

$$
\begin{eqnarray}
\frac{1}{1-x} &=& \sum_0^{+\infty} x^n \\
&=& 1 +x +x^2 +x^3 +\ldots
\end{eqnarray}
$$
```
````{prf:proof}
On utilise la formule ci-dessous, valable pour $x \neq 1$:

$$
\begin{eqnarray}
\sum_0^{N} x^n &=& \frac{1-x^{N+1}}{1-x}
\end{eqnarray}
$$

Cette formule se vérifie facilement en faisant passer le dénominateur $(1-x)$ du côté gauche, et en distribuant (voir TD).

Puis, on fait tendre $N$ vers $+\infty$. Comme on a supposé $\vert x \vert <1$, $\lim_{N \rightarrow +\infty} x^{N+1}=0$.
````





%$$
%\begin{circuitikz}[american voltages]
%\draw
%  % rotor circuit
%  (0,0) to [short, *-] (6,0)
  %to [V, l_=$\mathrm{j}{\omega}_m \underline{\psi}^s_R$] (6,2) % rotor emf
  %to [R, l_=$R_R$] (6,4) % rotor resistance
  %to [short, i_=$\underline{i}^s_R$] (5,4) % rotor current
%
%  % stator circuit
%  (0,0) to [open, v^>=$\underline{u}^s_s$] (0,4) % stator voltage
%  to [short, *- ,i=$\underline{i}^s_s$] (1,4) % stator current
%  to [R, l=$R_s$] (3,4) % stator resistance
%  to [L, l=$L_{\sigma}$] (5,4) % leakage inductance
%  to [short, i_=$\underline{i}^s_M$] (5,3) % magnetizing current
%  to [L, l_=$L_M$] (5,0); % magnetizing inductance
%\end{circuitikz}
%$$


%$$
%\begin{tikzpicture}
%\datavisualization [scientific axes,
%visualize as smooth line]
%data [format=function] {
%var x : interval [0:100];
%func y = sqrt(\value x);
%};
%\end{tikzpicture}
%$$
