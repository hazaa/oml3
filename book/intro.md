# Outils Mathématiques et Logiciels (OML3)

**Programme national (PN)**
%-----

Extrait du programme national (PN) LP-BUT GEII 2022, R3.04:

* Transformation de Fourier : impulsion de Dirac, échantillonnage, produit de convolution, modulation:
   * Calculer la transformée de Fourier de signaux de base ;
   * Déterminer le produit de convolution de signaux de base ;
   * Manipuler les méthodes de calcul dédiées aux traitements du signal (impulsion de Dirac, modulation, échantillonnage, théorème de Shannon) ;
   * Reconstituer un signal par une méthode numérique (avec table et/ou logiciel) ;
   * Représenter les spectres d’un signal non périodique (application au traitement du signal...., modulation....).
* Transformation en Z : définition, propriétés, formules usuelles, inverse :
  *  Calculer la transformée en Z directe et indirecte (inverse).


%**Plan du cours**
%-----

%```{tableofcontents}
%```

**Planning 2023-2024**
%-----

| Nature| Thème  | Evaluation | A | B | FA2 |
| ---      | ---      | ---      | ---      | ---      |---      |
| CM1   | Révisions |  | 05/09 |  | idem |
| CM2   | Fourier temps discret |  |  18/09|  | idem  |
| CM3   |   TZ |  |  22/09|  | idem |
| TD1   | Révisions |  | 08/09 |  | 08/09 |
| TD2   |  DSF |  | 13/09 |  | 13/09 |
| TD3   | Fourier temps discret |  |  18/09|  | 18/09 |
| TD4   | Fourier temps discret |  | 22/09 |   | 22/09  |
| TD5   | TZ |  | 25/09 |  | 25/09  |
| TD6   | DS blanc |  | 26/09 |  | 26/09  |
| **DS**   |  |  | 6/10 |  | 06/10  |

| Nature| Thème  | Evaluation | A1 | A2 | B1 | FA2 A1 |
| ---      | ---      | ---      | ---      | ---      |---      |---|
| TP1   | SFD|   |  /10| /10 |  | 18/11|
| TP2   | TZ | | /11 | /11 |  | /11|
| TP3   |  | CTP 2h | /12 | /11 |  | /12|

