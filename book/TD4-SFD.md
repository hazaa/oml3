TD4 SFD
=======

1 Spectrogramme à trous
-------------

Soit un signal $x[n]$ réel périodique de période $N=5$.
On donne une partie des valeurs de $X[k]$:

$$
\begin{equation}
X[0]=0, X[1]=-j, X[2]=0
\end{equation}
$$


1. Tracer le diagramme de $\mathcal{Re}(X[k])$ et $\mathcal{Im}(X[k])$ avec les valeurs disponibles.
1. En utilisant les propriétés de parité et de périodicité de la SFD, compléter les valeurs manquantes. 
1. Compléter le diagramme de $\mathcal{Re}(X[k])$ et $\mathcal{Im}(X[k])$ pour $k \in [-N,N]$.

2 Signal constant
-------------

Soit la suite N-périodique $x[n]=1$ pour tout $n$.

1. Calculer les coefficients de sa SFD.  

3 Cosinus $N$ quelconque
-----

Soit $x[n]=\cos(\frac{2\pi}{N}n)$. 
%La notation $\delta[k]$ représente une impulsion de Dirac dans le domaine fréquentiel.

1. Ecrire $X[k]$.


4 Somme
-------------

Soit la suite $N$-périodique suivante:

$$
\begin{eqnarray}
x[n] &=& 1+ \sin(\frac{2\pi}{N}n) + 3\cos(\frac{2\pi}{N}n) + \cos(\frac{4\pi}{N}n)
\end{eqnarray}
$$

1. Pour chaque terme de la somme dans $x[n]$ calculer sa SFD en utilisant la notation $\delta[k]$.
1. En déduire $X[k]$ pour $k \in [-2,2]$. 
1. Tracer $\mathcal{Re}(X[k])$ et $\mathcal{Im}(X[k])$.

( cf {cite}`oppenheim_1997` 3.1 p.216) 

5 THD
-------------

Soit une charge nonlinéaire alimentée par une alimentation sinusoïdale. 
Le courant $i(t)$ qui traverse cette charge n'est pas sinusoïdal. On veut calculer la THD (Total Harmonic Distorsion).
On mesure et on échantillonne le courant $i[n]$, et on obtient un vecteur de longueur $N=64$.
Ce vecteur contient $4$ périodes entières du signal périodique $i[n]$ étudié.
L'information utile sera donc portée par $k_1=4$ ainsi que les harmoniques $mk_1$ où $m$ est un entier.
On donne le spectrogramme de $I[k]$ ci-dessous, et ses valeurs non nulles:

$$
\begin{equation}
I[4]= -26.2j, ~I[12]= -3.6j, ~I[20]=0.9j, ~I[28]= 0.8j
\end{equation}
$$

On donne aussi une formule du THD adaptée au cas du temps discret:

$$
\begin{eqnarray}
 \text{THD} &=& 100 \frac{\sqrt{ \sum_{ m =2 }^{N/(2k_1)-1} |X[mk_1]|^2 } }{|X[k_1]|}
\end{eqnarray}
$$

1. Le signal $i[n]$ est-il sinusoïdal ?
1. Calculer la valeur du THD.
1. Dans cette formule, pourquoi la sommation se fait sur $m \in [2, N/(2k_1)-1]$ ?


% UGLY HACK, WAITING FOR BUGFIX !!!!!!!!!!!!!https://jupyterbook.org/en/stable/explain/migration.html?highlight=glue#limitations-for-figures-and-math
![fig_sin_THD](fig/temp/fig_sin_THD.png)

6 Dirac fréquentiel
-----

Soit un signal $x[n]$ $N$-périodique. Les coefficients de sa SFD sont donnés par $X[k]=\delta[k]$.

1. Calculer $x[n]$ pour tout $n$.

(cf {cite}`oppenheim_2010` , Exemple 8.2)

7 Porte
------

Soit le signal porte, périodique de période $N=10$ défini comme suit:

$$
\begin{equation*}
x[n] = \left\{
    \begin{array}{rl}
        1 & \text{si } n = 0,1,2,3,4\\
        0 & \text{sinon }
    \end{array} \right.
\end{equation*}
$$

1. Calculer $X[k]$ pour $k \in [0,5]$.
2. Compléter les valeurs manquantes par parité et périodicité.
3. Tracer $\mathcal{Re}(X[k])$.

( cf {cite}`oppenheim_2010`, Exemple 8.3 p.627)


Bonus
--------

* Convolution: calcul convolution périodiq Examp 8.4 Opp10; Exam 8.11, circular convol neq periodic convol); Willsky Exo 3.57 c
* Dérivée discrète  Willsky p.223
* Etalement fréquentiel cf CM $\frac{2\pi}{14}$
* (DTFT Opp10 exo 2.47. correc ? exo 2.55. correc ? exo 2.57. correc ?)
