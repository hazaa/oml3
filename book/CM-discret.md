(CM_TZ)=
#  CM Transformée en Z

`````{admonition} Prérequis

*  OML3, chapitre {ref}`CM_revisions`.

En particulier:

* Transformée de Laplace. 
* Série entière: $\sum_{n=0}^{+\infty} x^n=1/(1-x)$ , pour la TZ (exemple passe bas).

`````

```{danger}
* Dans ce chapitre le temps est **discret**: les **signaux** sont des suites $x_k$ ou $x[k]$, avec $k \in \mathbb{N}$. 
* Contrairement au chapitre {ref}`CM_SFD`, les suites $x[k]$ ne sont **pas nécessairement périodiques**.
* On ne calcule plus des **intégrales** $\int x(t)dt$ mais des **sommes** $\sum_n x_n$.
```

```{note}
Dans cette partie du cours nous nous appuyons sur le livre de F.Cottet {cite}`cottet_2020`(disponible en format électronique à la bibliothèque universitaire de l'UPEC)
ainsi que sur {cite}`oppenheim_1997` et {cite}`oppenheim_2010`
```

Système
-----

```{important}
Dans ce chapitre on passe de la notion de **signal** à la notion de **système**.

* **signal**: on dispose d'une mesure, en fonction du temps, par exemple $x[n]$. (On peut transforme cette mesure 
pour mieux l'analyser, en passant par exemple dans l'espace des fréquences).
* le **système** va **modifier** un signal $x[n]$ en **entrée**, pour donner un signal en **sortie** $y[n]$. (On peut transformer 
chacune de ces deux mesures en passant dans l'espace des fréquences).

(cf {cite}`oppenheim_1997` §1.5)
```

```{important}
On se restreint à une classe de système dit **LTI** (Linear and Time Invariant):

* le système modifie $x[n]$ en appliquant uniquement des opérations linéaires.
* le système est *time-invariant*, ce qui signifie qu'il modifie $x[n]$ toujours de la même manière, quel que soit l'instant de temps.
```

Filtrage
-----

````{prf:example}
:label: def-filtrage
 
Un **ﬁltre** numérique est un système: il transforme une suite de nombres en entrée $x_n$ 
en une suite de nombres en sortie $y_n$. 
````


````{prf:definition}
:label: ex-filtrage-non-recursif
 
**Filtre non récursif**:

$$
\begin{eqnarray}
y_k &=& \sum_{i=0}^M b_i x_{k-i} \\
&=& b_0 x_k + b_1 x_{k-1}+b_2 x_{k-2}+\ldots
\end{eqnarray}
$$

* Ce filtre est dit "non-récursif" car $y_k$ ne dépend pas des valeurs précédentes de $y$ (par exemple $y_{k-1},\ldots$)
* On parle aussi de filtre RIF (réponse impulsionnelle finie), ou FIR en anglais.
````

````{prf:example}
:label: TZ-ex-moyenne

**Moyenne glissante** : ce filtre est l'un des plus simples, il consiste à moyenner les $m$ derniers échantillons.

$$
\begin{eqnarray}
y_k &=& \frac{1}{m} \sum_{i=0}^{m-1}  x_{k-i}
\end{eqnarray}
$$

Ce filtre est un cas particulier de filtre non-récursif, défini en {prf:ref}`ex-filtrage-non-recursif`. 

* Avantage: Il permet de lisser un signal bruité.
* Inconvénient: Retard. On perd des informations (e.g. amplitude maximale).

**Exemple**: impulsion, $m=4$

%```{glue:figure} fig_filter_sliding_window
%:figwidth: 700px
%:name: ""
%```
```{glue} fig_filter_sliding_window
:doc: notebooks.ipynb
```
![fig_filter_sliding_window](fig/temp/fig_filter_sliding_window.png)

En effet on a :

$$
\begin{eqnarray}
x_k &=& \delta[k] \\
y_0 &=& \frac{1}{m} \sum_{i=0}^{m-1}  x_{0-i} \\
&=& \frac{1}{m} ( \delta[0]+\delta[-1]+\delta[-2]+\delta[-3]) \\
&=& \frac{1}{m} \delta[0] = \frac{1}{m} \\
y_1 &=& \frac{1}{m} \sum_{i=0}^{m-1}  x_{1-i}\\
&=& \frac{1}{m} ( \delta[1]+\delta[0]+\delta[-1]+\delta[-2]) = \frac{1}{m} \\
y_4 &=& \frac{1}{m} \sum_{i=0}^{m-1}  x_{4-i}\\
&=& \frac{1}{m} ( \delta[4]+\delta[3]+\delta[2]+\delta[1])= 0 \\
\end{eqnarray}
$$


**Exemple**: créneau, $m=4$

%```{glue:figure} fig_filter_sliding_window_gate
%:figwidth: 700px
%:name: "fig_filter_sliding_window_gate"
%```

```{glue} fig_filter_sliding_window_gate
:doc: notebooks.ipynb
```
![fig_filter_sliding_window_gate](fig/temp/fig_filter_sliding_window_gate.png)


**Exemple**: sinus bruité, $m=4$

%```{glue:figure} fig_filter_sliding_window_sine
%:figwidth: 700px
%:name: "fig_filter_sliding_window_sine"
%```
```{glue} fig_filter_sliding_window_sine
:doc: notebooks.ipynb
```
![fig_filter_sliding_window_sine](fig/temp/fig_filter_sliding_window_sine.png)


````

````{prf:definition}
:label: tz-convol-def
 
**Produit de convolution:**

Soient deux suites $x_k$ et $y_k$. Le produit de convolution entre ces suites définit une **nouvelle suite** $u_k$:

$$
\begin{eqnarray}
u_k &=& (x_k \star y_k)[k] \\
&=& \sum_{i=-\infty}^{+\infty} x_i y_{k-i} 
\end{eqnarray}
$$ 
````

````{prf:corollary}
Comme les suites avec lesquelles on travaille sont **causales**, la suite obtenue par produit 
de convolution s'écrira:
 
$$
\begin{eqnarray}
u_k &=& \sum_{i=0}^{+\infty} x_i y_{k-i} 
\end{eqnarray}
$$ 
````

````{prf:property}
**Commutativité du produit de convolution:**

Le produit de convolution est commutatif: 
 
$$
\begin{eqnarray}
x_k \star y_k &=& y_k \star x_k
\end{eqnarray}
$$ 

(cf {cite}`oppenheim_1997` §2.3)
````

`````{admonition} Remarque 
:class: tip

Cette définition a un équivalent en temps **continu**:

$f \star g(t)  = \int_{-\infty}^{+\infty} f(\theta) g(t-\theta) d\theta$

Ceci ne sera pas traité en cours mais si le sujet vous intéresse, reportez-vous au chapitre {ref}`CM_Fourier_continu`.
`````

````{prf:definition}
:label: ex-filtrage-recursif
 
**Filtre récursif**:

$$
\begin{eqnarray}
y_k &=& \sum_{i=0}^M b_i x_{k-i} - \sum_{i=1}^N a_j y_{k-j}\\
&=& b_0 x_k + b_1 x_{k-1}+\ldots - ( a_1 y_{k-1}+a_2 y_{k-2}+\ldots)
\end{eqnarray}
$$

* Ce filtre est dit "récursif" car $y_k$ **dépend** des valeurs précédentes de $y$ (par exemple $y_{k-1},\ldots$).
* On parle aussi de filtre RII (réponse impulsionnelle infinie), ou IIR en anglais.
````
```{danger}
Remarquez que $i$ commence à $0$ dans la première somme, et à $1$ dans la seconde somme.
```

```{admonition} Problème

* Comment choisir les coefficients du filtre ?
* Par exemple, pour avoir une réponse fréquentielle $H(f)$ donnée (passe-bas, passe-haut...). 
```

```{note}
Les ﬁltres numériques sont, pour les signaux **échantillonnés**, les équivalents des
ﬁltres analogiques pour les signaux **continus**.

```

%* notion de réponse impulsionnelle (pas défini jusque là). cottet 3.3.1
%   pour les LTI dans le cas discret:  eq. 9.4


Transformée en Z
-----

Pour faciliter l'étude des signaux numériques et notamment le filtrage, on utilise
la Transformée en Z (TZ), qui est l'équivalent de la Transformée de Laplace (TL) dans
le domaine des signaux numériques.


````{prf:definition}
:label: TZ-def

Soit un signal continu et causal $s(t)$. On l'échantillonne à la période $T_e$,
ce qu'on note $s(kT_e)=s_k$. La **Transformée en Z** de $s_k$ est:

$$
\begin{eqnarray}
S(z) &=& \sum_{i=0}^{+\infty} s_k z^{-k} \\
&=& s_0 + s_1 z^{-1}+s_2 z^{-2}+\ldots
\end{eqnarray}
$$

Rappel: "causal": voir {prf:ref}`def-causal`.
````

````{prf:proof}
Pour voir d'où vient cette définition, on calcule la Transformée de Laplace du
signal échantillonné:

$$
\begin{eqnarray}
s_e(t) &=& \sum_{k=0}^{+\infty} s(k T_e) \delta(t - kT_e) \\
S_e(p) &=&  \mathcal{L}\Big(s_e(t) \Big)(p)  \\
  &=& \int_0^{+\infty} e^{-pt}  s_e(t)  dt \\
&=& \int_0^{+\infty} e^{-pt} \Bigg( \sum_{k=0}^{+\infty} s(k T_e) \delta(t - kT_e) \Bigg)  dt \\
\end{eqnarray}
$$

On fait sortir la somme $\sum$, car on a le droit d'intervertir $\int$ et $\sum$. 
On peut aussi faire sortir $s(k T_e)$ de l'intégrale car il ne dépend pas de $t$:

$$
\begin{eqnarray}
S_e(p) &=&  \sum_{k=0}^{+\infty}  s(k T_e) \int_0^{+\infty} e^{-pt}   \delta(t - kT_e)   dt \\
\end{eqnarray}
$$

Le terme dans l'intégrale est en fait la Transformée de Laplace de la fonction 
$t \rightarrow \delta(t - kT_e) $.
Or, la TL d'un Dirac vaut $1$ (voir {ref}`CM_revisions`), et la TL d'une fonction
retardée vaut $\mathcal{L} \Big( u(t-a) \Big)(p)  = e^{-ap} \mathcal{L} \Big( u(t)  \Big)(p)$. Donc:

$$
\begin{eqnarray}
S_e(p) &=& \sum_{k=0}^{+\infty} s(k T_e) e^{-p k T_e}  \\
&=& \sum_{k=0}^{+\infty} s_k  e^{-p k T_e} \\
S(z)&=& \sum_{k=0}^{+\infty} s_k z^{-k}
\end{eqnarray}
$$

avec $z = e^{p T_e}$
````


````{prf:property}
:label: TZ-prop-lineaire

**Linéarité**:  soit $s_k=a x_k + b y_k$, alors:

$$
\begin{eqnarray}
S(z) &=& a X(z) + b Y(z)
\end{eqnarray}
$$

````

````{prf:property}
:label: TZ-prop-retard

**Retard**:

Soit $S(z)$ la TZ d'un signal $s[k]$.
La TZ de $s[k-m]$, où $m$ est un entier, est $z^{-m} S[z]$.
% La TZ de $s(t-T_e)$, où $T_e$ est la période d'échantillonnage, est $\frac{1}{z}S(z)$, ou $z^{-1} S(z)$.
````
````{prf:proof}
**Méthode 1**:

Soit $S_m(z)$ la TZ du signal retardé $s[k-m]$. On a:

$$
\begin{eqnarray}
S_m(z)&=& \sum_{k=0}^{+\infty} s[k-m] z^{-k}
\end{eqnarray}
$$

Par changement de variable $l=k-m$ on a :

$$
\begin{eqnarray}
S_m(z)&=& \sum_{l=-m}^{+\infty} s[l] z^{-l-m}
\end{eqnarray}
$$

Comme $s[l]$ est causale, les termes $s[-1],~s[-2],\ldots$ sont nuls. D'où:

$$
\begin{eqnarray}
S_m(z) &=& \sum_{l=0}^{+\infty} s[l] z^{-l-m}
&=& z^{-m} \sum_{l=0}^{+\infty} s[l] z^{-l}
&=& z^{-m}  S(z)
\end{eqnarray}
$$

**Méthode 2**:
On repasse par le signal continu du temps $s(t)$. Traitons le cas $m=1$:
La TL d'une fonction retardée vaut $\mathcal{L} \Big(  u(t-a) )(p) = e^{-ap} \mathcal{L} \Big(  u(t) \Big)(p)$.

Donc $\mathcal{L} \Big(  s(t-T_e) \Big)(p) = e^{-T_ep} \mathcal{L} \Big( s(t))(p)$.

Or on a posé plus haut $z = e^{p T_e}$.

Donc $TZ[s(t-T_e)] = \frac{1}{z}S(z)$
````


````{prf:property}
:label: TZ-prop-convolution

**TZ des signaux élémentaires**:

* impulsion unité : 
$
\begin{eqnarray}
TZ[ \delta(k)]&=&1
\end{eqnarray}
$
* échelon:  
$
\begin{eqnarray}
TZ[ u(k)]&=&\frac{1}{1-z^{-1}}
\end{eqnarray}
$
* exponentielle/géometrique:  
$
\begin{eqnarray}
TZ[ a^k]&=&\frac{1}{1-az^{-1}}
\end{eqnarray}
$

````
````{prf:proof}
Calculons les TZ:
* impulsion unité : soit $x_k=\delta(k)$. Alors: 

$$
\begin{eqnarray}
TZ[ x_k] &=& \sum_{k=0}^{+\infty} x_k z^{-k} \\
&=& \sum_{k=0}^{+\infty} \delta(k) z^{-k} \\
&=& 1 . z^{0} + 0+0 + \ldots\\
&=& 1
\end{eqnarray}
$$
  

* échelon:  

$$
\begin{eqnarray}
TZ[ u_k] &=& \sum_{k=0}^{+\infty} u_k z^{-k} \\
&=& \sum_{k=0}^{+\infty} 1. z^{-k} \\
&=& \sum_{k=0}^{+\infty}  (z^{-1})^k \\
&=& \frac{1}{1-  z^{-1}}\\
\end{eqnarray}
$$

car $\sum_{k=0}^{+\infty}x^k = \frac{1}{1-x}$ quand $\vert x \vert <1$, voir {ref}`CM_revisions_series`. 

* exponentielle:  soit $x_k=a^k$

$$
\begin{eqnarray}
TZ[ a^k] &=& \sum_{k=0}^{+\infty} a^k z^{-k} \\
&=& \sum_{k=0}^{+\infty}  (az^{-1})^k \\
&=& \frac{1}{1-  az^{-1}}\\
\end{eqnarray}
$$
````


````{prf:property}
:label: TZ-prop-convolution

**TZ du produit de convolution**:

On a défini en {prf:ref}`tz-convol-def` le produit de convolution  $x_k \star y_k$  entre deux suites  $x_k, ~y_k$. 
La TZ du produit de convolution est le produit des TZ:

$$
\begin{eqnarray}
TZ[ x_k \star y_k ] = X(z)Y(z)
\end{eqnarray}
$$ 
````
````{prf:proof}   
Calculons la TZ de $u_k=x_k \star y_k $:

$$
\begin{eqnarray}
U(z) &=& \sum_{k=0}^{+\infty} u_k z^{-k}
       &=& \sum_{k=0}^{+\infty} \Bigg( \sum_{i=0}^{+\infty} y_i x_{k-i}  \Bigg)  z^{-k}
\end{eqnarray}
$$ 

On peut intervertir les sommes et sortir le $y_i$:

$$
\begin{eqnarray}
U(z) &=&   \sum_{i=0}^{+\infty} y_i  \sum_{k=0}^{+\infty}x_{k-i}  z^{-k}
\end{eqnarray}
$$ 

On peut faire apparaître un $z^{-i}$ sous la somme :

$$
\begin{eqnarray}
U(z) &=&   \sum_{i=0}^{+\infty} y_i z^{-i} \sum_{k=0}^{+\infty}x_{k-i}  z^{-(k-i)}  
\end{eqnarray}
$$ 

Par changement de variable, on reconnaît que $\sum_{k=0}^{+\infty}x_{k-i}  z^{-(k-i)}$ vaut $X(z)$.
Comme ce terme ne dépend plus de $i$, on peut le faire sortir de la somme.

On a alors:

$$
\begin{eqnarray}
U(z) &=& X(z)  \sum_{i=0}^{+\infty} y_i z^{-i} 
&=& X(z)Y(z)
\end{eqnarray}
$$ 

**NB**: en temps continu, l'équivalent est le théorème de Plancherel, cf {prf:ref}`plancherel` (hors programme)
````



Filtres et TZ
-----

Dans cette section on revient aux filtres, et comment ils sont représentés par la TZ.


````{prf:definition}
:label: TZ-transmittance

Soit un filtre qui transforme une suite $x_k$ en une suite $y_k$. Soient $X(z)$ et $Y(z)$ leurs TZ.

On nomme **transmittance en $Z$**, notée $H(z)$, la quantité:

$$
\begin{eqnarray}
\frac{Y(z)}{X(z)} &=& H(z)
\end{eqnarray}
$$
````

```{important}
Cette notion est très proche de la **fonction de transfert** $H(p)$ vue avec la transformée de Laplace
pour les signaux **analogiques**, ou avec les réponses fréquentielles $H(j\omega)$.
```

````{prf:property}
:label: TZ-prop-transmit-non-rec
La transmittance en Z d'un filtre **non récursif** est:

$$
\begin{eqnarray}
H(z ) = \frac{Y(z)}{X(z)} &=&  \sum_{i=0}^M b_i z^{-i} \\
&=& b_0 + b_1 z^{-1}+b_2 z^{-2}+\ldots
\end{eqnarray}
$$ 
````

````{prf:proof}
Reprenons le filtre non récursif:

$$
\begin{eqnarray}
y_k &=& \sum_{i=0}^M b_i x_{k-i}
\end{eqnarray}
$$

On utilise la formule $TZ[ x_k \star y_k ] = TZ[ x_k].TZ[ y_k]$, et on obtient:  

$$
\begin{eqnarray}
Y(z) &=& TZ[\sum_{i=0}^M b_i x_{k-i}] \\
  &=& TZ[ b_k \star x_k] \\
  &=& TZ[b_k].TZ[x_k]\\
  &=& TZ[b_k].X(z)
\end{eqnarray}
$$

D'où:

$$
\begin{eqnarray}
H(z ) = \frac{Y(z)}{X(z)} &=&  \sum_{i=0}^M b_i z^{-i} 
\end{eqnarray}
$$ 

````

````{prf:property}
:label: TZ-H-recursif

La transmittance en Z d'un filtre **récursif** est:

$$
\begin{eqnarray}
H(z ) = \frac{Y(z)}{X(z)} &=&  \frac{\sum_{i=0}^M b_i z^{-i} }{\sum_{i=0}^N a_i z^{-i}} \\
&=& \frac{ b_0 + b_1 z^{-1}+b_2 z^{-2}+\ldots}{1+ a_1 z^{-1}+a_2 z^{-2}+\ldots}
\end{eqnarray}
$$ 

car $a_0=1$.
````
````{prf:proof}
On utilise le même principe que pour le fitre récursif des deux côtés, pour faire apparaître également un $Y(z)$
du côté des termes retardés en $y$. 

On part de la définition du filtre récursif:

$$
\begin{eqnarray}
y_k &=& \sum_{i=0}^M b_i x_{k-i} - \sum_{i=1}^N a_j y_{k-j}
\end{eqnarray}
$$

Soit, en regroupant les termes en $y$:

$$
\begin{eqnarray}
\sum_{i=0}^N a_j y_{k-j} &=& \sum_{i=0}^M b_i x_{k-i} 
\end{eqnarray}
$$

avec $a_0=1$. 

On applique la TZ des deux côtés, et la formule de convolution:

$$
\begin{eqnarray}
TZ[a_k] Y(z) &=& TZ[b_k] X(z)\\
\frac{Y(z)}{X(z)} &=& \frac{TZ[b_k]}{TZ[a_k]}\\
H(z) &=&  \frac{\sum_{i=0}^M b_i z^{-i} }{\sum_{i=0}^N a_i z^{-i}}
\end{eqnarray}
$$

````

````{prf:property}
:label: TZ-H-reponse-freq

**Réponse en fréquence:**

Pour étudier la réponse en fréquence on met en entrée du système **une exponentielle complexe** et on
observe la sortie.
 
Dans le cas des système **LTI** (Linear and Time invariant), qui sont étudiés dans ce cours:

* en **temps continu**, si l'entrée vaut $x(t) = e^{j \omega t}$ alors la sortie vaut $y(t)=H(j\omega) e^{j\omega t}$, où $H(s)$ est la fonction de transfert dans le domaine de Laplace. C'est ce terme qui fixe l'amplitude de la sortie (cf {cite}`oppenheim_1997` §3.2, §9.7 p.693). 
* en **temps discret**, si l'entrée vaut $x_n =e^{j \omega n}$  alors la sortie vaut $y_n=H( e^{j \omega}) e^{j\omega n}$, où $H(z)$ est la **transmittance en Z**.


En **temps discret**, la réponse en fréquence s'obtient donc en remplaçant $z$ par $e^{j\omega}$, et en calculant le module $\vert H(e^{j\omega})\vert$
pour $\omega \in [-\pi, \pi]$.

(cf {cite}`oppenheim_1997` §6.6, cf {cite}`oppenheim_2010` §5.3)
````

````{prf:proof}
cf {cite}`oppenheim_1997` §3.2
````

Synthèse de filtre
-----

Dans cette section on voit comment la TZ peut nous aider à choisir 
les coefficients des filtres. 


````{prf:example}
**Filtre de lissage**: les filtres avec les équations de récurrence ci-dessous sont des filtres 
de lissage, leurs coefficients sont donnés par le triangle de Pascal.

$$
\begin{eqnarray}
y_k &=&  x_k +  x_{k-1} \\
y_k &=&  x_k + 2 x_{k-1} + x_{k-2}
\end{eqnarray}
$$

On montre (voir TD) qu'ils ont une réponse en fréquence $\vert H(e^{j\omega})\vert$ 
de filtre **passe-bas**.

%cf morain-nicollier p.16
````

````{prf:example}
**Filtre dérivateur**: le filtre avec l'équation de récurrence ci-dessous est  
l'analogue en temps discret de la dérivation d'ordre 1:

$$
\begin{eqnarray}
y_k &=&  x_k -  x_{k-1}
%y_k &=&  x_k  - 2 x_{k-1}+x_{k-2}
\end{eqnarray}
$$

Sa réponse en fréquence $\vert H(e^{j\omega})\vert$ est $2  \sin(\frac{\omega}{2}) $: 

%```{glue:figure} fig_filter_derivative
%:figwidth: 700px
%:name: "fig_filter_derivative"
%
%Filtrage passe-haut numérique
%```

```{glue} fig_filter_derivative
:doc: notebooks.ipynb
```
![fig_filter_derivative](fig/temp/fig_filter_derivative.png)



%cf morain-nicollier p.15
````
````{prf:proof}
On part de l'équation de récurrence:

$$
\begin{eqnarray}
y_k &=&  x_k -  x_{k-1}
\end{eqnarray}
$$

On en déduit la transmittance en $z$:

$$
\begin{eqnarray}
H(z) &=&  1 - z^{-1}
\end{eqnarray}
$$

D'où $H(e^{j\omega})$ en remplaçant $z$ par $e^{j\omega}$:

$$
\begin{eqnarray}
H(e^{j\omega}) &=&  1 - e^{-j\omega}
\end{eqnarray}
$$

Enfin, le module de $H(e^{j\omega})$ vaut:

$$
\begin{eqnarray}
\vert H(e^{j\omega})\vert &=&  \vert 1 - e^{-j\omega}\vert \\
&=&  \vert (1 - ( \cos(\omega) -j\sin(\omega)) )  \vert \\
&=&  \sqrt{  (1 -  \cos(\omega))^2 + (\sin( \omega) )^2  }\\
&=&  \sqrt{  1  -2  \cos(\omega)  +\cos(\omega)^2 + (\sin(\omega) )^2  }\\
&=&  \sqrt{  2  -2  \cos(\omega)   }\\
&=&  \sqrt{  2  (1-  \cos(\omega))   }\\
\end{eqnarray}
$$

On se rappelle la formule $\sin^2(\theta)=\frac{1-\cos(2\theta)}{2}$

D'où:

$$
\begin{eqnarray}
\vert H(e^{j\omega})\vert &=&  \sqrt{  2  (1-  \cos(\omega))   }\\
&=&    \sqrt{  2  ( 2 \sin^2(\frac{\omega}{2})) }\\
&=&     2  \sin(\frac{\omega}{2}) \\
\end{eqnarray}
$$

````

```{important}
Il existe plusieurs méthodes pour synthétiser un filtre. L'une d'entre elles est 
la **transposition** en temps discret d'un **filtre analogique**, c'est-à-dire défini 
à temps continu.

Autrement dit: on va partir des fonctions de transfert $H(p)$ connues, et obtenir $H(z)$.

Pour les autres méthodes, consulter {cite}`cottet_2020`. 
```

`````{admonition} Transformation d’Euler ou équivalence de la dérivation
On part d'une fonction de transfert connue $H(p)$, dans le domaine de Laplace.
Pour obtenir $H(z)$ on remplace $p$ comme suit :

$$
\begin{eqnarray}
p &\rightarrow & \frac{1}{T_e}(1-z^{-1})
\end{eqnarray}
$$ 

où $T_e$ est la période d'échantillonnage.
`````

````{prf:proof}
Pour voir d'où vient cette transformation, on part d'un exemple: un système intégrateur continu $y(t) = \frac{dx(t)}{dt}$.

Par Transformée de Laplace, on obtient $Y(p)=p.X(p)$, soit $H(p)=p$.

Pour passer dans le domaine discret, on utilise la méthode des rectangles vue en OML2, ce qui donne :

$$
\begin{eqnarray}
y_k &=&\frac{x_k-x_{k-1}}{T_e} \\
      &=&\frac{1}{T_e}  x_k- \frac{1}{T_e} x_{k-1} \\
     &=& \sum_{i=0}^M b_i x_{k-i} \\
     &=& b_0 x_k - b_1 x_{k-1} 
\end{eqnarray}
$$ 

A partir de cette équation de récurrence on passe à la transmittance en $Z$ d'un filtre non récursif:

$$
\begin{eqnarray}
H(z ) = \frac{Y(z)}{X(z)} &=&  \sum_{i=0}^M b_i z^{-i} \\
&=& b_0 + b_1 z^{-1}  \\
&=& \frac{1}{T_e}  - \frac{1}{T_e}  z^{-1}  \\
&=& \frac{1}{T_e} \Bigg( 1-  z^{-1} \Bigg) 
\end{eqnarray}
$$ 

**Résumé**:

* en continu on a $H(p)=p$ 
* en discret on a $H(z) = \frac{1}{T_e} \Bigg( 1-  z^{-1} \Bigg) $

C'est pour cela que la transformation par équivalence de la dérivation remplace $p$ par $\frac{1}{T_e} \Bigg( 1-  z^{-1} \Bigg) $

cf {cite}`cottet_2020` 9.2.2 b) pour plus de détails.
````



````{prf:example}
Soit le filtre passe-bas de fonction de transfert: 

$$
\begin{eqnarray}
H(p) & = & \frac{1}{1+\tau .p}
\end{eqnarray}
$$ 

On effectue le remplacement $p \rightarrow  \frac{1}{T_e}(1-z^{-1})$:

$$
\begin{eqnarray}
H(z) & = & \frac{1}{1+ \frac{\tau}{T_e}(1-z^{-1}) } \\
        & = & \frac{1}{ (1+ \frac{\tau}{T_e}) -\frac{\tau}{T_e} z^{-1} } \\
        &=& \frac{1}{ (1+ \frac{\tau}{T_e}) -\frac{\tau}{T_e} z^{-1} } \\
        &=&  \frac{ \frac{1}{1+ \frac{\tau}{T_e}} }{ 1 -\frac{\frac{\tau}{T_e}}{1+ \frac{\tau}{T_e}} z^{-1} } \\
        &=&  \frac{ \frac{T_e}{T_e+\tau} }{ 1 -\frac{\tau}{\tau+T_e} z^{-1} } \\
\end{eqnarray}
$$ 

On remarque que le dénominateur est un polynôme en $z^{-1}$, il s'agit donc d'un **filtre récursif**.

On identifie avec l'expression générale vue en {prf:ref}`TZ-H-recursif`:

$$
\begin{eqnarray}
H(z)  &=&  \frac{ \frac{T_e}{T_e+\tau} }{ 1 -\frac{\tau}{\tau+T_e} z^{-1} } \\
&=& \frac{\sum_{i=0}^M b_i z^{-i} }{\sum_{i=0}^N a_i z^{-i}}
\end{eqnarray}
$$ 

Soit:

$$
\begin{eqnarray}
b_0 &=& \frac{T_e}{ T_e + \tau} \\
a_1 &=& -\frac{\tau}{T_e+\tau}
\end{eqnarray}
$$ 

Nous pouvons donc réinjecter dans l'expression de l'équation de récurrence vue en {prf:ref}`ex-filtrage-recursif`:

$$
\begin{eqnarray}
y_k &=& \sum_{i=0}^M b_i x_{k-i} - \sum_{i=1}^N a_j y_{k-j}
\end{eqnarray}
$$

Soit:

$$
\begin{eqnarray}
y_k &=& b_0 x_{k} -  a_1 y_{k-1}\\
      &=& \frac{T_e}{ T_e + \tau}  x_{k} +  \frac{\tau}{T_e+\tau}  y_{k-1}
\end{eqnarray}
$$

**Application numérique**: $\tau=1 \text{ms}$, $T_e=100 \mu s$ 

$$
\begin{eqnarray}
y_k &= 0.09 x_{k} +  0.90  y_{k-1}
\end{eqnarray}
$$

%```{glue:figure} fig_filter_lowpass
%:figwidth: 700px
%:name: "fig_filter_lowpass"
%
%Filtrage passe-bas numérique
%```

```{glue} fig_filter_lowpass
:doc: notebooks.ipynb
```
![fig_filter_lowpass](fig/temp/fig_filter_lowpass.png)


Et on peut vérifier que le filtre est bien passe-bas en inspectant sa **réponse fréquentielle** $\omega \rightarrow \vert H(e^{j\omega})\vert$:

%```{glue:figure} fig_filter_lowpass_freqz
%:figwidth: 700px
%:name: "fig_filter_lowpass_freqz"
%```

```{glue} fig_filter_lowpass_freqz
:doc: notebooks.ipynb
```
![fig_filter_lowpass_freqz](fig/temp/fig_filter_lowpass_freqz.png)


````


%`````{admonition}Transformation homographique ou équivalence de l'intégration
%coco
%`````

%````{prf:proof}
%coco
%````

%````{prf:example}
%coco
%````


## Résumé des formules


|  Suite $x_k$| TZ |
| ---      | ---      |
|$s_k$|$\sum_{i=0}^{+\infty} s_k z^{-k} $|
|Linéarité||
|Retard||
|Convolution||



| Equation aux différences | $H(z)$ |
| ---      | ---      |
| $y_k = \sum_{i=0}^M b_i x_{k-i} $ | $\sum_{i=0}^M b_i z^{-i} $  |
| $y_k = \sum_{i=0}^M b_i x_{k-i} - \sum_{i=1}^N a_j y_{k-j}$ | $ \frac{\sum_{i=0}^M b_i z^{-i} }{\sum_{i=0}^N a_i z^{-i}}, ~a_0=1 $  |


Applications
-----

* Traitement de signal:
   * Télécom (traitement de la voix).
   * Médical (1D audition, 2D imagerie...)   

* Asservissements:
   * Robotique. 
   * Electrotechnique.


Pour aller plus loin
-----

Ce cours n'est qu'une initiation, beaucoup de thèmes n'ont pas été abordés:

* Modulation. 
* Bruit, débruitage.
* 2D: image
* Synthèse et réalisation de filtre FIR, IIR (R4.ESE.07 : Electronique spécialisée)
* Spectre échantillonné (TFD, FFT) ; R3.ESE.15 : Electronique spécialisée
   *  cf {cite}`cottet_2020` §8


