(CM_SFD)=
#  CM Série de Fourier discrète

`````{admonition} Prérequis

*  OML3, chapitre {ref}`CM_revisions`.

En particulier:

* Décomposition en série de Fourier
* Séries $\sum_{k=0}^{\infty} x_k$

`````


```{note}

Le but de ce cours est de:

* introduire les signaux à temps discrets, qui sont utilisés en pratique.
* appliquer la décomposition en série de Fourier (DSF) à ces signaux à temps discret.


Dans cette partie nous nous appuyons sur les livres {cite}`oppenheim_1997` et {cite}`oppenheim_2010`.
```


Temps discret, signal numérique
-----


### Classiﬁcation des signaux 

(source: [wikipedia](https://commons.wikimedia.org/wiki/File:Digital.signal.discret.svg)).

![classif_signal](fig/digital_signal_classification.svg)


````{prf:definition}
:label: signal-num-def

Un signal **numérique** est:

* **échantillonné**: on connaît ses valeurs $s(nT_e)$ pour $n$ entier.
* **quantifié**: il prend ses valeurs dans un ensemble **fini**.
````

### Quelques signaux classiques



````{prf:example}
**Signaux classiques**


%```{glue:figure} fig_signal_elementary
%:figwidth: 700px
%:name: "fig_signal_elementary"
%```

```{glue} fig_signal_elementary
:doc: notebooks.ipynb
```

% UGLY HACK, WAITING FOR BUGFIX !!!!!!!!!!!!!https://jupyterbook.org/en/stable/explain/migration.html?highlight=glue#limitations-for-figures-and-math
%![fig_signal_elementary](_build/html/_images/notebooks_6_1.png)
![fig_signal_elementary](fig/temp/notebooks_6_1.png)
````

````{prf:definition}
:label: dirac-discret-def

L'impulsion de Dirac en temps discret:

$$
\begin{equation*}
\delta[n] = \left\{
    \begin{array}{rl}
        1 & \text{si } n = 0,\\
        0 & \text{sinon }
    \end{array} \right.
\end{equation*}
$$

(cf {cite}`oppenheim_1997` p.30)
````

```{danger}
* dans le cas du **temps continu**, vu au chapitre {ref}`CM_revisions`:
   * le Dirac $t\rightarrow \delta(t)$ a une amplitude **infinie** en $0$. 
   *  $\int_{-\infty}^{\infty} \delta(t) dt=1$
* dans ce chapitre, le temps est **discret**: 
   * **Attention**: l'impulsion unité $\delta[n]$ a une amplitude **finie** de valeur $1$. 
   * la somme du signal vaut toujours $1$: $\sum_{n=-\infty}^{\infty} \delta[n]=1$
```

### Echantillonnage

````{prf:definition}
:label: echantillonnage

**Echantillonnage idéal:**

Echantillonner un signal analogique $s(t)$ consiste à en extraire des valeurs
régulièrement espacées dans le temps, avec la période $T_e$.

D'un point de vue **mathématique**, le signal échantillonné (continu) peut s'écrire:

$$
\begin{eqnarray}
s_e(t) &=& \sum_{k=-\infty}^{+\infty} s(t) \delta(t-k.T_e) \\
 &=& s(t) \sum_{k=-\infty}^{+\infty}  \delta(t-k.T_e)
 &=& s(t) \text{Ш}_{T_e}(t)
\end{eqnarray}
$$

où $\text{Ш}_{T_e}(t)$ est le **peigne de Dirac**.
L'échantillonnage **idéal** peut donc se représenter comme la multiplication par un peigne de Dirac.

![echantillonnage](fig/echantillonnage.svg)
````


```{note}
Dans ce cours OML3 on suppose que le signal est **déjà échantillonné ou numérisé**.

On ne s'intéresse pas à la manière d'échantillonner du point de vue électronique.

Pour savoir **comment** cette opération est réalisée, chercher "CAN, CNA" dans: 

*  BUT GEII: Ressource R3.08 : Electronique; 
*  BUT GEII: Ressource R3.ESE.15 : Electronique spécialisée
* voir {cite}`cottet_2020` chapitre 7.
```


```{attention}
* En échantillonnant, on **perd** forcément de l'information ????
* **Pas forcément**, si le signal et l'échantillonnage respectent une **condition**.
```




````{prf:theorem}
**Théorème d'échantillonnage de Shannon-Nyquist:**

Soit un signal continu $s(t)$ de **largeur spectrale limitée**, c'est à dire que tout
son spectre est nul en dehors de $[-f_{max};f_{max}]$. 

On échantillonne ce signal à la fréquence $f_e$.

Si $s(t)$ ne contient pas de fréquence plus grande que $\frac{f_e}{2}$, 
alors $s_e(t)$ détermine complétement $s(t)$, autrement dit on peut
reconstruire $s(t)$ à partir de $s_e(t)$, autrement dit on ne perd pas d'information
lors de l'échantillonnage. 

La condition $f_e  > 2 f_{max} $ est la condition de **Shannon-Nyquist**.

Si la condition n'est pas remplie, il y a **recouvrement** du spectre (cf {cite}`cottet_2020` chap. 7.1-2 pour plus de détail).
````
````{prf:proof}
La preuve se fait dans le cadre de la transformée de Fourier à temps continu, qui n'est pas traitée dans ce cours.
Pour en savoir plus, lisez le chapitre Bonus {ref}`CM_Fourier_continu`.
````


````{prf:example}

% UGLY HACK, WAITING FOR BUGFIX !!!!!!!!!!!!!https://jupyterbook.org/en/stable/explain/migration.html?highlight=glue#limitations-for-figures-and-math
![fig_sine_shannon](fig/temp/fig_sine_shannon.png)

```{glue} fig_sine_shannon
:doc: notebooks.ipynb
```

* Si on échantillonne avec $f=f_e$, le signal échantillonné est constant.
* Si on échantillonne avec $f=2f_e$, le changement de signe est bien présent dans le signal échantillonné.
````

```{note}
**En pratique** Pour éviter ce phénomène, il y a toujours un filtre passe bas analogique appliqué
avant l'échantillonnage (on le nomme parfois "anti-repliement").

Ceci pose des problèmes pratiques, car ni le filtrage passe-bas ni l'échantillonnage 
ne sont idéaux. La reconstruction n'est donc pas si simple, voir {cite}`cottet_2020` pour plus de précisions.
```



Série de Fourier à temps discret
-----


La décomposition en série de Fourier (DSF) vue en 1ere année et révisée au chapitre {ref}`CM_revisions`
n'est définie que pour les fonctions **périodiques et continues du temps**. 

Or, dans la pratique, les appareils de mesure travaillent aujourd'hui majoritairement sur des 
signaux **numériques**, qui sont donc en **temps discret**.

Pour contourner ce problème, on utilisera les **séries de Fourier discrète (SFD)**.


```{important}
* Dans la suite du chapitre le temps sera **discret**: on le notera $n \in \mathbb{N}$. 
* Les **signaux** sont représentés par des suites indicées par $n$, qu'on notera $x_n$ ou $x[n]$.
* On ne calcule plus des **intégrales** $\int x(t)dt$ mais des **sommes** $\sum_n x_n$.
```

<h3> Propriétés des suites exponentielles   </h3>

### Propriétés des suites exponentielles 

````{prf:definition}
:label: periodic-discrete

Un signal discret $x[n]$ est périodique de période $N>0$ si $\forall n, ~x[n]=x[n+N]$

(cf {cite}`oppenheim_1997` p.211)
````

````{prf:corollary}
:label: discr-exp-is-periodic

Un signal exponentiel complexe discret $x[n]=e^{j \omega_0 n}$ est périodique de période $N>0$ si et seulement si 
il existe deux entiers $m,N$ tels que $\frac{\omega}{2\pi}=\frac{m}{N}$

(cf {cite}`oppenheim_1997` 1.3.3 p.25)
````

````{prf:proof}
La condition $x[n]=x[n+N]$ se ré-écrit :

$$
\begin{eqnarray}
x[n]=x[n+N] & \Leftrightarrow & e^{j \omega_0 n} = e^{j \omega_0 (n+N)}\\
& \Leftrightarrow & 1 = e^{j \omega_0 N}\\
& \Leftrightarrow & \omega_0 N = m 2 \pi\\
& \Leftrightarrow & \frac{\omega_0}{2 \pi}  = \frac{m}{N}
\end{eqnarray}
$$

````


````{prf:property}
:label: periodic-discrete-exp

La suite exponentielle complexe $x[n]=e^{j\frac{2\pi}{N}n}$ est périodique de période $N$.

(cf {cite}`oppenheim_1997` p.211)
````

````{prf:example}
Posons $N=8$, et considérons la suite $x[n]=e^{j\frac{2\pi}{8}n}$:

```{glue} fig_ej2pi_n
:doc: notebooks.ipynb
```
% UGLY HACK, WAITING FOR BUGFIX !!!!!!!!!!!!!https://jupyterbook.org/en/stable/explain/migration.html?highlight=glue#limitations-for-figures-and-math
![fig_ej2pi_n](fig/temp/fig_ej2pi_n.png)
````

````{prf:property}
:label: periodic-discrete-exp-kn

Les suites exponentielles complexes périodiques de période $N$ sont les suites:

$\phi_k[n]=e^{j\frac{2\pi}{N}kn}$  pour $k \in [0,N-1]$ 

(cf {cite}`oppenheim_1997` p.211)
````


````{prf:example}
:label: periodic-discrete-exp-kn-example

Si on prend $n=N+1$, on trouve :

$$
\begin{eqnarray}
e^{j\frac{2\pi}{N}k(N+n)} &=& e^{j\frac{2\pi}{N}kN }e^{j\frac{2\pi}{N}kn }\\
&=& e^{j 2\pi k } e^{j\frac{2\pi}{N}kn}
&=&  e^{j\frac{2\pi}{N}kn}
\end{eqnarray}
$$

````

````{prf:property}
Il existe un $k$ tel que la fréquence soit **maximale**. Dans le cas où $N$ est pair il suffit de prendre:

$$
\begin{eqnarray}
k_{max}&=&\frac{N}{2}
\end{eqnarray}
$$

On obtient alors:

$$
\begin{eqnarray}
e^{j\frac{2\pi}{N}k_{max}n} &=& e^{j\frac{2\pi}{N} \frac{N}{2} n} \\
&=&  e^{jn\pi} \\
&=& (-1)^n
\end{eqnarray}
$$

Cette suite prend alternativement les valeur $1$ et $-1$, c'est la **fréquence maximale** qu'on peut obtenir 
avec un signal à **temps discret**.
````

````{prf:example}
Pour $N=8$ comme ci-dessus, on obtient $k_{max}=\frac{N}{2}=4$ soit $e^{j\frac{2\pi}{8} 4 n}= e^{jn\pi}$.
````


````{prf:property}
Si on augmente $k$ au dela de $k_{max}$, on obtient un signal de fréquence inférieure.


(cf {cite}`oppenheim_1997`, schéma p.27)
````

````{prf:example}
Pour $N=8$, soit la suite $x[n]=\mathcal{Re}(e^{j \frac{2 \pi}{8} kn }) = \cos(\frac{ \pi}{4} kn )$. En augmentant $k$, on constate:

* une fréquence maximale est obtenue pour $k=4$.
* pour $k=5$, on revient sur le même signal que pour $k=3$. 

```{glue} fig_ej2pi_kn
:doc: notebooks.ipynb
```
% UGLY HACK, WAITING FOR BUGFIX !!!!!!!!!!!!!https://jupyterbook.org/en/stable/explain/migration.html?highlight=glue#limitations-for-figures-and-math
![fig_ej2pi_kn](fig/temp/fig_ej2pi_kn.png)

````


```{danger}
A l'inverse du cas continu, pour une exponentielle complexe de période $N$, on ne **peut pas augmenter**
indéfiniment la fréquence des fonctions périodiques de période $N$.
```

````{prf:property}
:label:  sum-discrete-exp-kn

La somme $\sum_{n \in \langle N \rangle}  e^{j\frac{2\pi}{N}k n}$ d'une exponentielle complexe périodique
est nulle sur sa période, sauf si  $k=0, ~N, 2N, \ldots$.

(cf {cite}`oppenheim_1997`, p.213)
````

````{prf:example}
Lorsque $N$ est pair, les termes symétriques par rapport à $0$ s'annulent deux à deux.
Par exemple pour $N=4, ~k=1$ on obtient:

$$
\begin{eqnarray}
\sum_{n = 0}^3 e^{j \frac{2 \pi}{N} kn } &=&  \sum_{n = 0}^3 e^{j \frac{2 \pi}{N} n } \\
&=&  e^0 + e^{\frac{\pi}{2}}+ e^{\frac{2\pi}{2}}+ e^{\frac{3\pi}{2}}  \\
&=&  1 + i + (-1) + (-i)  \\
&=& 0
\end{eqnarray}
$$
 
````

````{prf:example}
Lorsque $N$ est impair, on n'a pas de symétrie des termes de la somme par rapport à $0$. 
En revanche on a une symétrie par rapport à l'axe des $x$, ce qui peut donner une idée
sur la manière de prouver le résultat. Ci-dessous on se contente d'exemples.

Pour $N=3, ~k=1$ on sépare la partie réelle et imaginaire et on obtient:

$$
\begin{eqnarray}
\sum_{n = 0}^2 e^{j \frac{2 \pi}{N} 1n } &=& e^0 + e^{j \frac{2 \pi}{3}  } +e^{j \frac{4 \pi}{3} } \\
&=& \cos(0)+\cos(\frac{2 \pi}{3})+\cos(\frac{4 \pi}{3} )+ j\Big( \sin(0) +\sin(\frac{2 \pi}{3}) +\sin(\frac{4 \pi}{3} ) \Big) \\
&=& 1-\frac{1}{2}-\frac{1}{2} + j( 0 + \frac{\sqrt{3}}{2}-\frac{\sqrt{3}}{2})\\
&=& 0
\end{eqnarray}
$$

Pour $N=5, ~k=1$ on peut effectuer le même type de calcul grâce aux valeurs connues
(cf [wikipedia](https://en.wikipedia.org/wiki/Exact_trigonometric_values)) ou 
vérifier avec le logiciel Sage:

```python
i=var('i')
N=5; 
print(simplify(sum(cos(i*2*pi/N),i, 0, N-1)))
print(simplify(sum(sin(i*2*pi/N),i, 0, N-1)))
```

Les deux dernières lignes peuvent être remplacées par l'évaluation numérique (si la simplification symbolique échoue):

```python
print(n(sum(cos(i*2*pi/N),i, 0, N-1)))
print(n(sum(sin(i*2*pi/N),i, 0, N-1)))
```

````

<h3> Série de Fourier discrète  </h3>

### Série de Fourier discrète


````{prf:definition}
:label: discrete-fourier-series

Les coefficients de la **série de Fourier discrète (SFD)** d'une suite $x[n]$ réelle $N$-périodique sont {cite}`oppenheim_1997`:

$$
\begin{eqnarray}
X[k] &=& \frac{1}{N}\sum_{n \in \langle N \rangle} x[n] e^{-j \frac{2 \pi}{N} kn }
\end{eqnarray}
$$

où $\sum_{n \in \langle N \rangle}$ signifie qu'on calcule la somme sur une période de la suite.

Pour indiquer que $X[k]$ sont les coefficients de la SFD de $x[n]$, on notera parfois :

$$
\begin{eqnarray}
x[n]  & \stackrel{SFD}{\leftrightarrow} X[k]
\end{eqnarray}
$$

NB: en général les $X[k]$ sont complexes (pas comme la DSF).


A partir de ces coefficients on peut **reconstruire** le signal de départ avec la formule:

$$
\begin{eqnarray}
x[n] &=& \sum_{k \in \langle N \rangle} X[k] e^{j \frac{2 \pi}{N} kn }
\end{eqnarray}
$$

````

```{important}
On peut comparer ceci avec l'**écriture complexe** des coefficients de la **DSF** (Décomposition en série de Fourier) 
vue en chapitre {ref}`CM_revisions`, {prf:ref}`DSF`, pour une fonction **périodique continue**.

 
$$
\begin{eqnarray}
c_k &=& \frac{1}{T} \int_{[T]} f(t) e^{-j\frac{2\pi}{T} kt} dt \\
f(t) &=& \sum_{n=-\infty}^{+\infty} c_k e^{j\frac{2\pi}{T} kt}
\end{eqnarray}
$$
 
On voit que:
 
* l'intégrale $\int$ dans la DSF devient une somme $\sum$ dans la SFD, car on est à temps discret.
* $x(t)$ dans la DSF devient $x[n]$. 
* $k$ joue le même rôle: quand il augmente, la fréquence associée augmente.
* dans le terme exponentiel: 
   * $t$ dans le coefficient complexe $c_k$ DSF devient $k$ dans le coefficient $X[k]$.
   * $T$ dans le coefficient complexe $c_k$ DSF devient $N$ dans le coefficient $X[k]$.
``` 
     
```{danger}
On trouve parfois une  convention différente, avec la normalisation suivante, e.g. {cite}`oppenheim_2010`: $x[n] =\frac{1}{N} \sum_{k \in \langle N \rangle} X[k] e^{j \frac{2 \pi}{N} kn }$
```

### Exemples de base



````{prf:example}
:label: sfd-sinus

**SFD du sinus:**

Soit $x[n]=\sin( \omega_0 n)$.  Ce signal est $N$-périodique quand $\omega_0$ s'écrit 
$\frac{2\pi}{N}$ avec $N$ un entier. 

Ecrivons la formule d'Euler pour le sinus:

$$
\begin{eqnarray}
\sin(\frac{2\pi}{N}n) &=& \frac{e^{j \frac{2\pi}{N}n}-e^{-j \frac{2\pi}{N}n}}{2j}
\end{eqnarray}
$$

On a vu ci-dessus la formule des coefficients $X[k]$:

$$
\begin{eqnarray}
X[k] &=& \frac{1}{N}\sum_{n \in \langle N \rangle} x[n] e^{-j \frac{2 \pi}{N} kn }
\end{eqnarray}
$$

Pour $k=1$:

$$
\begin{eqnarray}
X[1] &=& \frac{1}{N}\sum_{n \in \langle N \rangle} x[n] e^{-j \frac{2 \pi}{N}  1 n } \\
&=& \frac{1}{N}\sum_{n \in \langle N \rangle} \frac{e^{j \frac{2\pi}{N}n}-e^{-j \frac{2\pi}{N}n}}{2j} e^{-j \frac{2 \pi}{N} n } \\
&=& \frac{1}{2jN}\Big( \sum_{n \in \langle N \rangle} e^{j \frac{2\pi}{N}n} e^{-j \frac{2 \pi}{N} n }  -  \sum_{n \in \langle N \rangle} e^{-j \frac{2\pi}{N}n} e^{-j \frac{2 \pi}{N} n } \Big) \\
&=& \frac{1}{2jN}\Big( \sum_{n \in \langle N \rangle} e^{0}  -  \sum_{n \in \langle N \rangle} e^{-j \frac{4\pi}{N}n}\Big) \\
&=& \frac{1}{2jN}\Big( N  - 0 \Big) 
\end{eqnarray}
$$

En effet on se rappelle que $\sum_{n \in \langle N \rangle} e^{-j \frac{4\pi}{N}n}$ est la somme temporelle des
termes d'une exponentielle complexe sur sa période, et cette somme vaut $0$ (cf {prf:ref}`sum-discrete-exp-kn`).

On en conclut que :

$$
\begin{eqnarray}
X[1] &=& \frac{1}{2j}
\end{eqnarray}
$$

De même pour $k=-1$:

$$
\begin{eqnarray}
X[-1] &=& \frac{1}{N}\sum_{n \in \langle N \rangle} x[n] e^{-j \frac{2 \pi}{N} (-1) n } \\
&=& \frac{1}{N}\sum_{n \in \langle N \rangle} \frac{e^{j \frac{2\pi}{N}n}-e^{-j \frac{2\pi}{N}n}}{2j} e^{-j \frac{2 \pi}{N} (-1) n } \\
&=& \frac{1}{2jN}\Big( \sum_{n \in \langle N \rangle} e^{j \frac{2\pi}{N}n} e^{j \frac{2 \pi}{N} n }  -  \sum_{n \in \langle N \rangle} e^{-j \frac{2\pi}{N}n} e^{j \frac{2 \pi}{N}  n } \Big) \\
&=& \frac{1}{2jN}\Big( \sum_{n \in \langle N \rangle} e^{j \frac{2\pi}{N}n} e^{j \frac{2 \pi}{N} n }  -  \sum_{n \in \langle N \rangle} e^{-j \frac{2\pi}{N}n} e^{j \frac{2 \pi}{N}  n } \Big) \\
&=& \frac{1}{2jN}\Big( \sum_{n \in \langle N \rangle} e^{j \frac{4\pi}{N}n}  -  \sum_{n \in \langle N \rangle} e^{0} \Big) \\
&=& \frac{1}{2jN}\Big( 0  - N \Big) 
\end{eqnarray}
$$

On en conclut que :

$$
\begin{eqnarray}
X[-1] &=& -\frac{1}{2j}
\end{eqnarray}
$$

Pour $N-1$ on retrouve le même résultat que pour X[-1] (on verra en {prf:ref}`Xk-periodic` que ceci est dû à la périodicité de $X[k]$):

$$
\begin{eqnarray}
X[N-1] &=& \frac{1}{N}\sum_{n \in \langle N \rangle} x[n] e^{-j \frac{2 \pi}{N} (N-1) n } \\
&=& \frac{1}{N}\sum_{n \in \langle N \rangle} x[n] e^{-j \frac{2 \pi}{N} N n } +\frac{1}{N}\sum_{n \in \langle N \rangle} x[n] e^{-j \frac{2 \pi}{N} (-1) n } \\
&=& \frac{1}{N}\sum_{n \in \langle N \rangle} x[n] e^{-j 2 \pi n } +\frac{1}{N}\sum_{n \in \langle N \rangle} x[n] e^{-j \frac{2 \pi}{N} (-1) n } \\
&=& \frac{1}{N}\sum_{n \in \langle N \rangle} x[n] +\frac{1}{N}\sum_{n \in \langle N \rangle} x[n] e^{-j \frac{2 \pi}{N} (-1) n } \\
&=& \frac{1}{N}\sum_{n \in \langle N \rangle} x[n] e^{-j \frac{2 \pi}{N} (-1) n } \\
&=&  X[-1]
\end{eqnarray}
$$

car $\sum_{n \in \langle N \rangle} x[n] =0 $, en tant que sinus périodique.

Pour les autres valeurs de $k$, on trouve $X[k]=0$ (à vérifier en exercice).

**En résumé:**

$$
\begin{equation*}
X[k] = \left\{
    \begin{array}{rl}
        \frac{1}{2j} & \text{si } k = 1, k=1 \pm N, \ldots \\
        -\frac{1}{2j} & \text{si } k = -1, k=-1 \pm N, \ldots  \\
        0 & \text{sinon }
    \end{array} \right.
\end{equation*}
$$

**Exemple** pour $N=5$, on obtient le diagramme suivant:

```{glue} fig_spectre_sin_discr
:doc: notebooks.ipynb
```
% UGLY HACK, WAITING FOR BUGFIX !!!!!!!!!!!!!https://jupyterbook.org/en/stable/explain/migration.html?highlight=glue#limitations-for-figures-and-math
%![spectre_cos](fig/spectre_cos.svg)
![spectre_sin](fig/temp/spectre_sin_discr.png)


NB:

* dans ce cas, la SFD est imaginaire pure.
* ce résultat est similaire au cas de la TF d'un sinus en temps continu, que nous ne traiterons pas. Reportez-vous au chapitre {ref}`CM_Fourier_continu` pour en savoir plus.

(cf {cite}`oppenheim_1997`, Exemple 3.10 p.214)

````

%````{important}
%Les coefficients de la SFD du sinus peuvent également se noter avec un **Dirac** dans le domaine **fréquentiel**:
%
%$$
%\begin{eqnarray}
%\sin(\frac{2\pi}{N}n)  &\stackrel{SFD}{\leftrightarrow} &  \frac{1}{2j}(-\delta[k+1]+\delta[k-1] ),  ~k=mN, ~m\in \mathbb{Z} 
%\end{eqnarray}
%$$
%````


````{prf:example}
SFD d'un **Dirac** temporel périodique: 

On a rappelé en {prf:ref}`dirac-discret-def` la définition de l'impulsion de Dirac en temps discret.
Dans ce chapitre, toutes les suites sont **périodiques**. 
On considère donc un Dirac qui se répète périodiquement, avec une période $N$.

Pour $k$ quelconque dans $[0,N-1]$, les coefficients $X[k]$:

$$
\begin{eqnarray}
X[k] &=& \frac{1}{N}\sum_{n \in \langle N \rangle} x[n] e^{-j \frac{2 \pi}{N} kn }\\
&=& \frac{1}{N}\sum_{n \in \langle N \rangle} \delta[n] e^{-j \frac{2 \pi}{N} kn }\\
&=& \frac{1}{N}  \delta[0] e^{-j \frac{2 \pi}{N} k 0 }\\
&=& \frac{1}{N} \times 1 \times 1
\end{eqnarray}
$$


Finalement:

$$
\begin{eqnarray}
\forall k \in [0,N-1], ~X[k] &=& \frac{1}{N}
\end{eqnarray}
$$

La SFD d'une impulsion de Dirac dans le domaine temporel est une fonction **constante de valeur $\frac{1}{N}$** dans le domaine fréquentiel.


NB: ce résultat est comparable à :

* la transformée de Laplace d'un Dirac (en temps continu), chapitre {ref}`CM_revisions`.
* la transformée de Fourier continue d'un Dirac (non traité dans ce cours).


(cf {cite}`oppenheim_2010`, Exemple 8.1 p.626)
````

````{prf:example}
:label: porte

SFD de la **porte**: 

Soit une porte qui se répète périodiquement, avec une période $N$, telle que $x[n]=0$ pour $n \in [0,4]$.

Voir TD.

( cf {cite}`oppenheim_2010`, Exemple 8.3 p.627, cf aussi {cite}`oppenheim_1997` Exa3.12 p.218)
````


### Quelques propriétés

````{prf:property}
:label: Xk-periodic

Les coefficients $X[k]$, associés à un signal $x[n]$ périodique, sont périodiques en $k$, de période $N$. 
````
````{prf:proof}

Ceci découle des propriétés des suites exponentielles complexes périodiques $\phi_k[n] = e^{-j \frac{2 \pi}{N} kn}$.
Par exemple si on pose $k=N+1$, on revient à $k=1$ :

$$
\begin{eqnarray}
e^{-j\frac{2\pi}{N}(N+1)n} &=& e^{-j\frac{2\pi}{N}n-j2\pi n}\\
&=& e^{-j\frac{2\pi}{N}n}e^{-j2\pi n}\\
&=& e^{-j\frac{2\pi}{N}n}
\end{eqnarray}
$$

Donc lorsqu'on injecte dans la définition des coefficients $X[k+N]$:

$$
\begin{eqnarray}
X[k+N] &=& \frac{1}{N}\sum_{n \in \langle N \rangle} x[n] e^{-j \frac{2 \pi}{N} (k+N)n }\\
&=& \frac{1}{N} \sum_{n \in \langle N \rangle} x[n] e^{-j \frac{2 \pi}{N} kn } e^{-j \frac{2 \pi}{N} N n }  \\
&=& \frac{1}{N} \sum_{n \in \langle N \rangle} x[n] e^{-j \frac{2 \pi}{N} kn } e^{-j 2 \pi n }  \\
&=& X[k]
\end{eqnarray}
$$

````

````{prf:example}
On a vu dans le cas du sinus $N$-périodique que $X[N-1]=X[-1]$, cf {prf:ref}`sfd-sinus`.
````

```{important}
Cette propriété de pédiodicité ne se retrouve pas dans le cas du temps continu.  
```

````{prf:property}
:label: xk-real

**Parité**: Quand $x[n]$ est **réel** (ce qui sera toujours le cas dans ce cours) on a la symétrie par rapport à $n=0$:

$$
\begin{eqnarray}
X[k]&=& X^*[-k] \\
\mathcal{Re}(X[k])&=&\mathcal{Re}( X[-k]) \\
\mathcal{Im}(X[k])&=& -\mathcal{Im}( X[-k]) \\
\end{eqnarray}
$$
où $^*$ représente le conjugué.

Autrement dit: 

* la partie réelle de $X[k]$ est **paire** (symmétrique par rapport à $k=0)$.
* la partie imaginaire de $X[k]$ est **impaire** .


(cf {cite}`oppenheim_1997` p.221; {cite}`oppenheim_2010` Tab. 8.1 p.634)
````
````{prf:proof}
Prendre la conjuguée de la définition de $X[k]$.
````

````{prf:example}
Voir l'example {prf:ref}`sfd-sinus` ci-dessus, avec le $\sin(\omega_0 n)$: 

  * la partie réelle est nulle, elle est bien paire.
  * la partie imaginaire de $X[k]$ est bien impaire.
````

````{prf:property}
:label: prop-translation

**Retard**: la SFD d'un signal retardé de $n_0$ vaut $e^{-j \frac{2 \pi}{N} k n_0} X[k]$, 
où $X[k]$ est la SFD du signal sans retard.
````

````{prf:proof}
Par changement de variable en posant $m=n-n_0$:

$$
\begin{eqnarray}
X_{n_0}[k] &=& \frac{1}{N}\sum_{n \in \langle N \rangle} x[n-n_0] e^{-j \frac{2 \pi}{N} kn }\\
\end{eqnarray}
$$
````


````{prf:property}
**Parseval**: 

Les coefficients $X[k]$ conservent la puissance du signal:

$$
\begin{eqnarray}
\frac{1}{N}\sum_{n \in \langle N \rangle} |x[n]|^2  &=& \sum_{k \in \langle N \rangle} |X[k]|^2 
\end{eqnarray}
$$

(cf {cite}`oppenheim_1997` 3.7.3 p.223)
````
````{prf:corollary}

Dans le cas continu vu au chapitre {ref}`CM_revisions`, on avait rappelé la définition du **Taux de Distorsion Harmonique** (TDH ou THD),
qui s'interprètait comme le ratio:

$$
\begin{eqnarray}
THD &=& 100 \frac{\text{valeur efficace de} ~y(t) \text{ sans son premier harmonique}}{\text{valeur efficace du premier harmonique}}
\end{eqnarray}
$$

En temps discret, grâce à l'égalité de Parseval, on peut ré-écrire ce taux pour un signal $y[n]$,
grâce aux coefficients $X[k]$ (voir TD).

$$
\begin{eqnarray}
THD &=&  100 \frac{\sqrt{ \sum_{ m =2 }^{N/(2k_0)-1} |X[mk_0]|^2 } }{|X[k_0]|}
\end{eqnarray}
$$
````
````{prf:example}

Supposons qu'on parte d'un sinus de fréquence $\frac{k_0}{N}$, qu'on écrit $x[n] = \sin( \frac{2 \pi}{N} k_0 n )$.
Alors on sait que  $X[k]$ va se concentrer en $k_0$ et $-k_0$.

Ci-dessous on prend $N=64$ et $k_0=4$.

Supposons qu'on **déforme** ce signal $x[n]$ **de plus en plus** (de haut en bas dans la figure ci-dessou).

* Il y a toujours un **fondamental** $k_0=4$
* On voit apparaître des **harmoniques** pour des valeurs multiples des $k_0$.
* Elles sont de **plus en plus** importantes **par rapport au fondamental**, à mesure que la déformation augmente.

```{glue} fig_sin_THD_more_distorsion
:doc: notebooks.ipynb
```
% UGLY HACK, WAITING FOR BUGFIX !!!!!!!!!!!!!https://jupyterbook.org/en/stable/explain/migration.html?highlight=glue#limitations-for-figures-and-math
![fig_sin_THD_more_distorsion](fig/temp/fig_sin_THD_more_distorsion.png)

Pour la première ligne le **THD** ici va valoir $0 \%$ car il n'y a pas d'harmoniques.

Pour la deuxième ligne (saturation à $y=0.7$) le **THD** va valoir:

$$
\begin{eqnarray}
THD &=&  100 \frac{\sqrt{ \sum_{ m =2 }^{N/(2k_0)-1} |X[mk_0]|^2 } }{|X[k_0]|} \\
        &=&  100 \frac{\sqrt{ |X[8]|^2+|X[12]|^2+ |X[16]|^2+ |X[20]|^2+ |X[24]|^2+|X[28]|^2 } }{ |X[4]|}\\
        &=&  100 \frac{\sqrt{ |0|^2+|-3.5|^2+ |0|^2+ |-1.6|^2+ |0|^2+|-0.4|^2 } }{ |-12.0|}\\
        &=&  100 \frac{\sqrt{ 14.7 } }{ |-12.0|}\\
        &=&  100 \frac{3.8}{ 12.0}\\
        &=& 31.9 \%
\end{eqnarray}
$$

````

````{prf:property}
**Reconstruction/approximation**

On peut reconstruire un signal $x[n]$ à partir des $X[k]$.

On peut **approximer** un signal $x[n]$ à partir d'une partie des $X[k]$.

(cf {cite}`oppenheim_1997` 3.106 p.219)
````
````{prf:remark}
**Différence** importante avec le cas en continu: on peut obtenir une reconstruction **exacte** d'un
signal non sinusoïdal, avec un nombre fini de coefficients.
````
````{prf:example}
A faire en TP.
````

````{prf:property}
**Produit, produit de Convolution**

Soient $x[n]$ et $y[n]$ deux suites périodiques de période $N$.
Alors la série de Fourier Discrète $Z[k]$ du produit $z[n]=x[n]y[n]$ sécrit:

$$
\begin{eqnarray}
x[n]y[n]  & \stackrel{SFD}{\longleftrightarrow}& Z[k] = \sum_{l \in \langle N \rangle} X[l]Y[k-l]
\end{eqnarray}
$$

Le terme $\sum_{l \in \langle N \rangle} X[k]Y[k-l]$ s'appelle un **produit de convolution**, on le verra de 
nouveau apparaître au chapitre {ref}`CM_TZ`.

(cf {cite}`oppenheim_1997` p.221)
````

<h3>  Exemples d'application des propriétés  </h3>

### Exemples d'application des propriétés



````{prf:example}
SFD d'un **Dirac fréquentiel**: 

Voir TD.

(cf {cite}`oppenheim_2010`, Exemple 8.2)
````

````{prf:example}
**Linéarité**

Voir TD.

(cf {cite}`oppenheim_1997`, Exemple 3.1 p.216)
````


````{prf:example}
**Parseval**

Voir TD.
````

### Résumé des SFD connues

On résume ci-dessous les SFD rencontrées dans ce cours, où $x[n]$ est **réel périodique** de période $N$. 

|  Signal temporel $x[n]$| SFD $X[k]$ |
| ---      | ---      |
|$\delta[n]$ |  $\frac{1}{N}$ |
|$\sin(\frac{2\pi}{N}n)$|  $\frac{1}{2j}$  si $k = 1, k=1 \pm N, \ldots$ ||
|                                     |   $-\frac{1}{2j}$  si $k = -1, k=-1 \pm N, \ldots$|
|                                     |       $0$ sinon |
| $x[n]$ est réel|$\mathcal{Re}(X[k])$ paire|
||$\mathcal{Im}(X[k])$ impaire|
|Retard $x[n-n_0]$|$e^{-j \frac{2 \pi}{N} k n_0} X[k]$|
|Périodicité |$X[k+N]=X[k]$|
|Parseval| $\frac{1}{N}\sum_{n \in \langle N \rangle} \mid x[n]\mid^2  = \sum_{k \in \langle N \rangle} \mid X[k]\mid^2$|
|Produit $z[n]=x[n]y[n]$ | $Z[k] = \sum_{l \in \langle N \rangle} X[l]Y[k-l]$|
   


(cf {cite}`oppenheim_2010` Tableau 8.1, {cite}`oppenheim_1997` 3.7 p.221)

Mise en oeuvre pratique
-----

Comme vu au début de ce cours, on part d'une **grandeur mesurée continue**, et on 
souhaite faire des calculs sur un ordinateur qui fonctionne avec des **états discrets**.
Les premières étapes sont :

* le filtrage analogique anti-repliement (théorème de Shannon-Nyquist).
* la numérisation du signal (CAN).
 
En plus de cela, on doit sélectionner **une partie** du signal total pour calculer la SFD. 
Cette étape s'appelle le **fenêtrage** (windowing) et peut se faire de plusieurs façons.

Ces différentes étapes sont résumées ci-dessous :

(cf {cite}`oppenheim_2010` Fig.10.1)

```{important}
Pour calculer les coefficients de la SFD, on utilisera la fonction `fft()` sous Python, ce qui
signifie Fast Fourier Transform.
```

```{important}
Ci-dessous on va voir les problèmes pratiques qui se posent lors du calcul de la SFD sur ordinateur.
```


````{prf:example}
**Sinus N-périodique**: 

Supposons qu'on effectue toutes les étapes précédentes et qu'on obtienne
le signal $x[n]=\cos(\frac{2\pi}{16}n)$ pris sur une fenêtre $N=64$.

Le signal $x[n]$ est bien $N$-périodique. Sa période correspond à celle de la fonction de base 
$\phi_{k}[n]  = e^{j\frac{2\pi}{N}kn}$ pour $k=4$:

$$
\begin{eqnarray}
\phi_{k=4}[n] &=&  e^{j\frac{2\pi}{N}kn} \\
            &=&  e^{j\frac{2\pi}{64}4n} \\
            &=&  e^{j\frac{2\pi}{16}n} \\
\end{eqnarray}
$$

On utilise le code Python suivant pour générer le signal et calculer les coefficients de la SFD:

```python
from scipy.fft import fft      

n=np.arange(64)               #  vecteur [0,1...,63]  
vn = np.sin(2*np.pi/16*n)
Vk=fft(vn)
```

Puis on affiche $v[n]$, les coefficients de la SFD $V[k]$, ainsi que leur partie rélle et imaginaire, et leur module:

```python
fig,ax = plt.subplots(4,1,figsize=(10,10))
ax[0].stem(vn)                     
ax[1].stem(np.real(Vk))
ax[2].stem(np.imag(Vk))
ax[3].stem(np.abs(Vk))
```
```{glue} fig_fft_sin_match
:doc: notebooks.ipynb
```

% UGLY HACK, WAITING FOR BUGFIX !!!!!!!!!!!!!https://jupyterbook.org/en/stable/explain/migration.html?highlight=glue#limitations-for-figures-and-math
![fig_fft_sin_match](fig/temp/fig_fft_sin_match.png)

**Comparaison avec l'analyse théorique** en {prf:ref}`sfd-sinus`:

* les suites $v[n]$ et $V[k]$ sont de même longueur $N=64$.
* partie réelle: elle est bien nulle. 
* partie imaginaire: 
   * on voit bien une seule raie négative pour $k=4$
   * $\mathcal{Im}(X[k])$ est bien impaire.
   * toutes les autres composantes sont bien nulles.


(cf {cite}`oppenheim_2010`, Exemple 10.5)
````

```{danger}
Le cas ci-dessus est un cas **idéal** qu'on trouvera rarement. Le plus souvent:

* on aura du mal à faire coincider la période du signal échantillonné avec $N$.
* il y aura du bruit de mesure.
* $N$ ne sera pas assez grand pour avoir une bonne résolution fréquentielle.
* etc...
```


````{prf:example}
**Sinus périodique sans fréquence de base commune**: 

Supposons qu'on effectue toutes les étapes précédentes et qu'on obtienne
le signal $x[n]=\sin(\frac{2\pi}{14}n)$ pris sur une fenêtre $N=64$.

Le signal $x[n]$ est 14-périodique mais sa période **ne correspond pas aux périodes des fonctions de base**
pour un signal de longueur $N=64$, $\phi_{k}[n]  = e^{j\frac{2\pi}{64}kn}$.

Pour voir comment cela impacte les coefficients, on utilise le code Python suivant:

```python
from scipy.fft import fft      

n=np.arange(64)               #  vecteur [0,1...,63]  
vn = np.sin(2*np.pi/14*n)
Vk=fft(vn)
```

Puis on affiche $v[n]$, les coefficients de la SFD $V[k]$, ainsi que leur partie rélle et imaginaire, et leur module:

```python
fig,ax = plt.subplots(4,1,figsize=(10,10))
ax[0].stem(vn)                     
ax[1].stem(np.real(Vk))
ax[2].stem(np.imag(Vk))
ax[3].stem(np.abs(Vk))
```
```{glue} fig_fft_sin_nomatch
:doc: notebooks.ipynb
```

% UGLY HACK, WAITING FOR BUGFIX !!!!!!!!!!!!!https://jupyterbook.org/en/stable/explain/migration.html?highlight=glue#limitations-for-figures-and-math
![fig_fft_sin_match](fig/temp/fig_fft_sin_nomatch.png)

**Comparaison avec l'analyse théorique** en {prf:ref}`sfd-sinus`:

* partie réelle: elle n'est **pas nulle**. 
* partie imaginaire: 
   * on voit **plusieurs** raies non-nulles aux alentours de $k=4$

```{important}
L'information utile a été **étalée** sur plusieurs fréquences proches, on a donc **perdu en précision**.
```

**Comment améliorer ce résultat ??**

* augmenter la résolution fréquentielle en augmentant $N$.
* caler l'enregistrement pour que $N$ coincide exactement avec une ou plusieurs périodes complètes du signal.
* changer la méthode de fenêtrage.


(cf {cite}`oppenheim_2010`, Exemple 10.4)
````


Bonus
-----

* la généralisation de la DSF pour des signaux non-périodiques à temps continu s'appelle la transformée de Fourier. Elle 
ne sera pas traitée en cours mais si le sujet vous intéresse, reportez-vous au chapitre {ref}`CM_Fourier_continu`.

* la généralisation de Série de Fourier pour un temps discret mais non périodique s'appelle DTFT (Discrete-Time
Fourier Transform), elle n'est pas au programme. 
Opp10 8.3 THE FOURIER TRANSFORM OF PERIODIC SIGNALS
    Example 8.6 Relationship Between the Fourier Series Coefﬁcients and the Fourier Transform of One Period

* la restriction de la DTFT à des fréquences discrètes s'appelle la DFT, elle n'est pas au programme.  
Opp10 8.5 FOURIER REPRESENTATION OF FINITE-DURATIONSEQUENCES: THE DFT



Exos TP
-----

Opp10 Exa 10.4 somme de 2 cos, $N=64$

idem avec  $N=128$

reconstruction willsky p.219

distorsion harmoniq + parseval: application plus facile avec Parseval discret

matlab+harmonic distorsion+fft: https://www.youtube.com/watch?v=Wtx8kyirG6U, https://fr.mathworks.com/videos/analyzing-power-quality-and-harmonic-distortion-93410.html

https://python-control.readthedocs.io/ ??

ajouter: matlab oppenheim companion [www](https://media.pearsoncmg.com/ph/esm/ecs_oppenheim_dtsp_3)
[lab0 2016](https://inst.eecs.berkeley.edu/~ee123/sp16/labs.html) [labs 2023](https://sites.google.com/berkeley.edu/ee123-sp23)

