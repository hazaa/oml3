Convolution dans le domaine fréquentiel
-----

Soient deux fonctions dont on donne les transformées de Fourier: $X_1(f)$ et $X_2(f)$.

![exo_launay1](fig/exo_launay_dirac1.svg)

1. Calculer graphiquement le produit de convolution $X_{12}(f)=X_1 \star X_2(f)$ entre $X_1(f)$ et $X_2(f)$.
1. Donnez l’expression de $X_2(t)$.  Donnez l’expression de $X_{12}(f)$ en fonction de $X_1(f)$ et de $F_e$ uniquement. Nous retrouvons une propriété essentielle du produit de convolution avec un Dirac.
1. En déduire le produit de convolution $X_{13}(f)$ entre $X_1(f)$ et $X_3(f)$, $X_3(f)$ étant donnée ci-dessous.
     ![exo_launay2](fig/exo_launay_dirac2.svg)
1. Que se passe-t-il si on poursuit avec $3F_e, \ldots$ ? Comparer avec l'expression du peigne de Dirac.


source: [Frederic Launay](https://www.lias-lab.fr/perso/fredericlaunay/Cours/T1)
