# TD1 Révisions


1 Laplace: fonction de transfert
-----

Soit un système obéissant à l'équation différentielle suivante, où
$x(t)$ est l'entrée et $y(t)$ la sortie. $a,b,c$ sont des constantes.
On suppose $y(0)=0$ et $\dot{y}(0)=0$.

$$
\begin{eqnarray}
b \frac{d^2y(t)}{dt^2} + a \frac{dy(t)}{dt} + y(t) & =&c x(t)
\end{eqnarray}
$$

Calculer la TL de l'équation différentielle et la fonction de transfert 
du système.


2 Laplace: réponse impulsionnelle.
-----

Soit le système intégrateur suivant:

$$
\begin{eqnarray}
y(t) &=& \int_0^t x(\theta)d\theta
\end{eqnarray}
$$

1. Calculer sa TL.
1. Calculer sa réponse impulsionnelle.


3 Laplace: rampe
-----

1. Calculer la TL d'une rampe $x(t)=t$. On la notera $X(p)$.
1. Soit un système du premier ordre, de fonction de transfert $H(p)$ ci-dessous. 
$
\begin{eqnarray}
H(p)=\frac{S(p)}{E(p)}  &=&  \frac{1}{1+\tau p} 
\end{eqnarray}
$
Pour calculer sa réponse à une rampe, on remplace $E(p)$ par la TL de la rampe, puis on calcule la décomposition en éléments simples de $S(p)$.
$
\begin{eqnarray}
S(p)=\frac{a}{p} + \frac{b}{1+\tau p} + \frac{c}{p^2} 
\end{eqnarray}
$
1. Pour finir le calcul de la réponse à une rampe, on calcule la TL inverse de $S(p)$, notée $s(t)$.


4 Séries numériques
-----

Vérifier que, pour $q \neq 1$:

$$
\begin{eqnarray}
\sum_0^{N} q^n &=& \frac{1-q^{N+1}}{1-q}
\end{eqnarray}
$$

5 Séries entières
-----

A partir de $\frac{1}{1-x}=\sum_0^{+\infty} x^n$, valable sur $]-1;1[$, calculer:

* $\frac{1}{1+u}$  (conseil: par changement de variable).
* $\frac{1}{1+u^2}$

6 Séries entières
-----

A partir de $\frac{1}{1-x}=\sum_0^{+\infty} x^n$, valable sur $]-1;1[$, calculer
par décomposition en éléments simples:

$$
\begin{eqnarray}
f(x) &=& \frac{x}{(x+1)(2-x)}
\end{eqnarray}
$$

On parle alors de développement en série entière de la fonction $f$.




Bonus
-----

redressement simple/double alternance: 
[Cottet](univ.scholarvox.com.ezproxy.u-pec.fr/reader/docid/88902771/page/42)

