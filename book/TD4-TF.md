# TD4 Transformée de Fourier

```{danger}
ATTENTION: ces énoncés ne seront pas utilisés, car la série de Fourier en temps continu n'est plus vue en cours.
```

1 TF: fonctions de base
-----

Calculer à la main à partir de la définition les TF des fonctions suivantes:

1. fonction porte définie ci-dessus.
1. $u(t-t_0)$, où $u(t)$ est une fonction quelconque et $t_0$ une constante.
1. $u(at)$ où $u(t)$ est une fonction quelconque et $a$ une constante strictement positive.


2 TF d'un Dirac
-----

Soit  $\delta_\varepsilon(t)$ la fonction définie par:

$$
\begin{equation*}
\delta_\varepsilon(t)=
\left\{
\begin{array}{rl}
\frac{1}{\varepsilon} & \text{si } t \in [0,\varepsilon],\\
0 & \text{sinon}.
\end{array} \right.
\end{equation*}
$$

1. Cette fonction respecte-t-elle $\int \delta_\varepsilon(t) dt=1$ ?
1. Calculer sa TF.
1. Faire tendre $\varepsilon$ vers 0.
1. Retrouve-t-on le même résultat que dans le cours avec la fonction ci-dessous ?

$$
\begin{equation*}
\delta_\varepsilon(t)=
\left\{
\begin{array}{rl}
\frac{1}{\varepsilon} & \text{si } t \in [-\frac{\varepsilon}{2},\frac{\varepsilon}{2}],\\
0 & \text{sinon}.
\end{array} \right.
\end{equation*}
$$


3 TF inverse
-----

1. Calculer la TF inverse de $U(f-f_0)$ où $f_0$ est une constante. 



4 TF: Shannon, domaine temporel
-----

Soit un signal $x(t)=sin(2 \pi f t)$ avec $f=50 ~\text{Hz}$. Répondre aux questions ci-dessous
pour $f_e=200~\text{Hz}$ puis $f_e=100~\text{Hz}$ puis $f_e=50~\text{Hz}$. 

1. Représentez $x(t)$, et $x_e (t)$ le signal échantillonné à la fréquence $f_e$.
1. Combien y-a-t-il d’échantillons par période de $x(t)$ ?
1. A partir de $x_e(t)$, est-il possible de reconstituer $x(t)$ (peut-on 
faire passer une courbe sinusoïdale unique entre les points échantillonnés) ?
1. Que vaut $f_{max}$ pour $x(t)$ ? Comparer la réponse à la question précédente (oui/non) avec la condition de Shannon-Nyquist.

source: [Frederic Launay](https://www.lias-lab.fr/perso/fredericlaunay/Cours/T1)

5 TF: Shannon, domaine fréquentiel
-----

Le but est ici de comprendre pourquoi le signal audio est échantillonné à $44,1~\text{kHz}$ avant
numérisation et stockage sur un CD audio.

Un signal audio réel (musique par exemple) a un spectre qui occupe les fréquences de
$20~\text{Hz}$ à $20~\text{kHz}$.

![exo_launay_freq](fig/spectre_voice.svg)


1. Représentez le spectre de ce signal échantilloné à $f_e=44,1~\text{kHz}$.
1. Représentez le spectre de ce signal échantilloné à $35~\text{kHz}$. Comment s’appelle le phénomène observé ?
1. En déduire une condition sur $f_e$ pour pouvoir reconstituer le signal : retrouvez le théorème de Shannon.
1. Est-ce que la fréquence utilisée dans les CD respecte bien ce théorème ?


source: [Frederic Launay](https://www.lias-lab.fr/perso/fredericlaunay/Cours/T1)


TF bonus
-----

TF d'un échelon unité


TF: bonus
-----

cf quéré: exos supplémentaires: p.134 (parabole, exponentielle,...)

