# TP3 Développement en série de Fourier

## DSF

## symbolic convolution 

xcas: f star g =  ifourier(fg)

## Symbolic Fourier with matlab

https://fr.mathworks.com/help/symbolic/sym.fourier.html
https://github.com/MathWorks-Teaching-Resources/Fourier-Analysis
https://fr.mathworks.com/matlabcentral/fileexchange/106725-fourier-analysis
https://disalw3.epfl.ch/teaching/signals_instruments_systems/ay_2021-22/exercises/lab03/SIS_21-22_lab03_assignment.pdf

## Symbolic Fourier with xcas

https://www-fourier.ujf-grenoble.fr/~parisse/giac/doc/en/cascmd_en/cascmd_en280.html#ssec%3Acfourier

## Symbolic Fourier with Sage
??

https://fr.mathworks.com/matlabcentral/fileexchange/106725-fourier-analysis

OK: dirac_delta, heaviside(t-1)-heaviside(t+1), e^(-t)*heaviside(t)
```
t,s = var('t,s')
f(t) = dirac_delta(t)
integrate(x(t) * e^(-i*2*pi*f*t),t,-oo,oo)
```

FAIL: cos, 1/2*(e^(i*2*pi*f*t) + e^(-i*2*pi*f*t))

```
t,f,f0 = var('t,f,f0')
assume(f0>0)
x(t) = sin(2*pi*f0*t)
integrate(x(t) * e^(-i*2*pi*f*t),t,-oo,oo)

x(t) = 1/2*(e^(i*2*pi*f0*t) + e^(-i*2*pi*f0*t))
integrate(x(t) * e^(-i*2*pi*f*t),t,-oo,oo, algorithm='giac')
```

UNCERTAIN:
x(t) = cos(2*pi*f*t)*(heaviside(t+1)-heaviside(t-1))
-1/16*I*(8*I*pi*f - e^(4*I*pi*f) - e^(-4*I*pi*f) + 2*I*sin(4*pi*f))/(pi*f) + 1/16*I*(-8*I*pi*f - e^(4*I*pi*f) - e^(-4*I*pi*f) - 2*I*sin(4*pi*f))/(pi*f)



```
t,f = var('t,f')
assume(f>0)
x = function('x')(t)
integrate(x.diff(t) * e^(-i*2*pi*f*t),t,-oo,oo)

```
