# TD5 Tranformee en Z

0 Réponse d'un filtre dans le domaine temporel
-------

Soit le fitre :

$$
\begin{eqnarray}
y(n) &=& \frac{1}{2}( x(n) + x(n-1) )
\end{eqnarray}
$$

Calculer dans le domaine temporel:

1. la réponse impulsionnelle.
1. la réponse à un échelon.

1 Calcul de transformée en Z
-------

Pour chacune des suites ci-dessous, calculer sa TZ.

1. Soit $x_k=[1,2,3,4,0,0,0,\ldots]$. 
1. Soit $x_k=a^k$ pour $k\in\mathbb{N}$.

2 Equation aux diﬀérences > transmittance en Z
-----

Soit le filtre décrit par l’équation aux diﬀérences suivante:

$$
\begin{eqnarray}
y(n) &=& 0,25 x(n) + 0,5 x(n-1) + 0,25 x(n-2)
\end{eqnarray}
$$

Rappel: l'équation aux différences est nécessaire pour l'implémentation du filtre.

1. Le fitre est-il non-récursif ou récursif ?
1. Calculer sa transmittance en $z$.


3 Transmittance en Z > équation aux diﬀérences
-----

Pour chaque transmittance en Z ci-dessous, écrire l'équation aux diﬀérences correspondante.

1. $H(z)=1 +  z^{-1}$
1. $H(z)= \frac{ 1 }{1 -z^{-1}}$
1. $H(z)=b_0 + b_1 z^{-1}+b_2 z^{-2}$
1. $H(z)= \frac{ b_0 + b_1 z^{-1}}{1+ a_1 z^{-1}+a_2 z^{-2}}$


Rappel: la transmittance en Z est nécessaire pour étudier des propriétés du filtre.


4 Equation aux diﬀérences > transmittance en Z
-----

Soit le filtre décrit par l’équation aux diﬀérences suivante:

$$
\begin{eqnarray}
y(n) &=&  x(n) + \alpha y(n-1) + \beta y(n-2)
\end{eqnarray}
$$

1. Le fitre est-il non-récursif ou récursif ?
1. Calculer sa transmittance en $z$.



5 Réponse en fréquence
-----

Soit le filtre dont l'équation de récurrence est:

$$
\begin{eqnarray}
y_k &=&  x_k + 2 x_{k-1} + x_{k-2}
\end{eqnarray}
$$

1. Le fitre est-il non-récursif ou récursif ?
1. Calculer sa transmittance en $z$.
1. Calculer et tracer sa réponse en fréquence, c'est-à-dire le module en fonction de $\omega$ pour $\omega \in [0; \pi]$. 
1. Quel est le type du filtre (passe bas, passe-haut, ...) ?


6 De continu vers discret: l'équivalence à la dérivation 
-----

Soit le filtre passe-bas de fonction de transfert: 

$$
\begin{eqnarray}
H(p) & = & \frac{1}{1+\tau .p}
\end{eqnarray}
$$

Pour les applications numériques on prendra $\tau=1 \text{ms}$, $T_e=100 \mu s$ .

Pour rappel on donne sa réponse impulsionnelle:

$$
\begin{eqnarray}
h(t) & = & \frac{1}{\tau} e^{-\frac{t}{\tau}} u(t)
\end{eqnarray}
$$

où $u(t)$ est l'échelon unité.

Et sa réponse indicielle:

$$
\begin{eqnarray}
s_{ind} & = & (1- e^{-\frac{t}{\tau}}) u(t)
\end{eqnarray}
$$

1. Trouver sa transmittance en $z$, $H(z)$ par la méthode d'équivalence à la dérivation (on remplace $p$ par $\frac{1}{T_e}(1-z^{-1})$). On doit trouver une expression du type:
$
\begin{eqnarray}
H(z) & = & \frac{b_0}{1-a_1 z^{-1}}
\end{eqnarray}
$ 
1. En déduire l'équation de récurrence du filtre en injectant $b_0$ et $a_1$ dans l'expression générale (voir cours, définition des filtres récursifs):
$
\begin{eqnarray}
y_k &=& \sum_{i=0}^M b_i x_{k-i} - \sum_{i=1}^N a_j y_{k-j}
\end{eqnarray}
$
1. Calculer à la main la réponse  $\{y_k\}_{k\geq 0}$ à un Dirac $\{x_k\}k\geq 0 = [1,0,0,\ldots]$. Ecrire le résultat dans un tableau à 3 colonnes ($k,~x_k,~y_k$).
1. Tracer les deux graphiques $x_k$ en fonction de $k$ et $y_k$ en fonction de $k$.
1. Comparer cette réponse impulsionnelle avec celle du cas continu, rappelée au début de l'énoncé.
1. Répéter les questions 3,4 pour une réponse indicielle, c'est-à-dire $\{x_k\}_{k\geq 0} = [1,1,1,\ldots]$.
1. Comparer cette réponse indicielle avec celle du cas continu, rappelée au début de l'énoncé.
1. Réponse fréquentielle: remplacer $z$ par $e^{j\omega}$ et calculer $\vert H(e^{j\omega}) \vert$.
1. S'agit-il bien d'un filtre passe-bas ?



7 Filtre: moyenne glissante
-----

Le filtre de la moyenne glissante est non récursif, il consiste à moyenner les $m$ derniers échantillons:

$$
\begin{eqnarray}
y_k &=& \frac{1}{m} \sum_{i=0}^{m-1}  x_{k-i}
\end{eqnarray}
$$

Soit $\{x_k\}_{k\geq 0}$ la suite de ses entrées et $\{y_k\}_{k\geq 0}$ la suite de ses sorties.
Pour les applications numériques on prendra $m=4$.

1. Calculer à la main la réponse  $\{y_k\}_{k\geq 0}$ à un Dirac $\{x_k\}_{k\geq 0} = [1,0,0,\ldots]$. Ecrire le résultat dans un tableau à 3 colonnes ($k,~x_k,~y_k$).
1. Tracer les deux graphiques $x_k$ en fonction de $k$ et $y_k$ en fonction de $k$.
1. Répéter les questions précédentes pour un créneau de longueur 10.

