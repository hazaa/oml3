# DS OML3 

* Les documents et téléphones sont interdits.
* L'usage des calculatrices collège est autorisé.
* Merci de rendre l'énoncé à la fin.
* Durée: 1h30


1 DSF (5 points)
-----

Soit $f(t)$ un signal triangle 2-périodique pair valant $2t-1$ sur $[0 , 1 ]$.

1. Calculez les coefficients de la décomposition en série de Fourier (DSF).
1. Écrivez la DSF grâce au théorème de Dirichlet.
1. Calculez sa valeur efficace dans le domaine temporel. 
1. Calculez sa valeur efficace dans le domaine fréquentiel à l'aide de la Formule de Parseval.

NB: vous pourrez utiliser la formule $\sum_{n=0}^{+\infty} \frac{1}{(2k+1)^2}=\frac{\pi^2}{8}$


2 Convolution (3 points)
-----

Calculez la convolution $f \star g(t)$ pour $f(t)=\Pi_\tau(t)$, et $g(t)=\delta(t)$

$$
\begin{equation*}
\Pi_\tau(t)=
\left\{
\begin{array}{rl}
1 & \text{si } t \in [-\frac{\tau}{2},\frac{\tau}{2}],\\
0 & \text{sinon}.
\end{array} \right.
\end{equation*}
$$


3 TF (4 points)
-----

Calculer la TF de la fonction $t \rightarrow \Pi_\tau(t) \cos(2 \pi f t)$, avec $\tau=1$, et $\Pi_\tau(t)$ défini ci-dessus.

4 TF et échantillonnage (3 points)
-----

On veut échantillonner des signaux analogiques issus de mesures. On représente 
leur spectre ci-dessous.

|signal a| signal b|
| ---      | ---      | 
|![voice_wide](fig/spectre_wide_bandwith.svg)|![voice](fig/spectre_voice.svg) | 



Pour chaque signal répondre aux questions:

1. Le spectre est-il borné par une fréquence $f_{max}$ ?
1. Peut-on reconstruire le signal échantillonné sans perte ?
1. Si oui quelle est la fréquence d'échantillonnage minimale ?


5 TZ (5 points)
-----

Soit le filtre à temps discret défini par l'équation de récurrence suivante:

$$
\begin{eqnarray}
y_k &=& x_k - x_{k-1}
\end{eqnarray}
$$

1. Ce filtre est-il récursif ou non ?
1. Calculez $a_0, a_1, \ldots$ et $b_0, b_1, \ldots$. et en déduire $H(z)$ .
1. Calculer la réponse en fréquence $\vert H(e^{j \omega}) \vert $. Tracer le graphe de cette réponse en fonction de $\omega$, pour $\omega \in [0; \pi]$.
1. Quel est le type du filtre (passe bas, passe-haut, ...) ?

Rappel:

* $ \sin^2(a)=\frac{1-\cos(2a)}{2}$
* $\cos^2(a) + \sin^2(a)=1 $
* $\sin(2a)=2\sin(a)\cos(a)$
