TD3 SFD
=====

1 Suites exponentielle complexes
---------

Soient les suites exponentielles complexes:

$$
\begin{eqnarray}
x[n] &=& e^{j \frac{2 \pi}{16} n } \\
y[n] &=& e^{j 6 n } \\
\end{eqnarray}
$$

1. Dans le cours, quelle est la condition pour qu'une exponentielle complexe discrète soit périodique ?
1. $x[n]$ est-elle périodique ?
1. idem avec $y[n]$


2 Somme de suite exponentielle complexe 
---------

Soit la somme de la suite exponentielle complexe ci-dessous, où on a pris $N=8$, $k$ entier:

$$
\begin{eqnarray}
x[n] &=& e^{j \frac{2 \pi}{8} kn } \\
S_k &=& \sum_{n = 0}^7 x[n]
\end{eqnarray}
$$

1. $x[n]$ est-elle périodique ? 
1. Calculer $S_{k}$ pour $k=1$.

3 SFD du cosinus
---------

Soit $x[n]=\cos( \omega_0 n)$. On pose $\omega_0=\frac{2\pi}{N}$ avec $N=8$ un entier. 


1. Le signal est-il périodique ?
1. Calculer $X[k]$ les coefficients de la Série de Fourier Discrète (SFD) pour $k=0,1,2,3,4$.
1. En déduire $X[k]$ pour $k=0,-1,-2,-3$ (parité de $\mathcal{Re}(X[k])$ pour $x[n]$ réel).
1. En déduire $X[k]$ pour $k=5,6,7$ (périodicité de $X[k]$).


4 TF: Shannon, domaine fréquentiel
-----

**Attention** exceptionnellement, on travaille en fréquence continue dans cet exercice.

Le but est ici de comprendre pourquoi le signal audio est échantillonné à $44,1~\text{kHz}$ avant
numérisation et stockage sur un CD audio.

Un signal audio réel (musique par exemple) a un spectre qui occupe les fréquences de
$20~\text{Hz}$ à $20~\text{kHz}$.

![exo_launay_freq](fig/spectre_voice.svg)


1. Rappeler la condition sur $f_e$ pour pouvoir reconstituer le signal après éhantillonnage (Shannon-Nyquist).
1. On échantillonne ce signal à $f_e=44,1~\text{kHz}$. Calculer $f_e$. Peut-on échantillonner puis reconstruire sans perte ?
1. Idem pour $f_e=35~\text{kHz}$.
1. Est-ce que la fréquence utilisée dans les CD respecte bien la condition ?


5 TF: Shannon, domaine temporel
-----

**Attention** exceptionnellement, on travaille en temps continu dans cet exercice.

Soit un signal $x(t)=sin(2 \pi f t)$ avec $f=50 ~\text{Hz}$. Répondre aux questions ci-dessous
pour $f_e=200~\text{Hz}$ puis $f_e=100~\text{Hz}$ puis $f_e=50~\text{Hz}$. 

1. Représentez $x(t)$, et $x_e (t)$ le signal échantillonné à la fréquence $f_e$.
1. Combien y-a-t-il d’échantillons par période de $x(t)$ ?
1. A partir de $x_e(t)$, est-il possible de reconstituer $x(t)$ (peut-on 
faire passer une courbe sinusoïdale unique entre les points échantillonnés) ?
1. Que vaut $f_{max}$ pour $x(t)$ ? Comparer la réponse à la question précédente (oui/non) avec la condition de Shannon-Nyquist.

source: [Frederic Launay](https://www.lias-lab.fr/perso/fredericlaunay/Cours/T1)





